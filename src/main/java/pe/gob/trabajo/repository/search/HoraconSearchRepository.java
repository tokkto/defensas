package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Horacon;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Horacon entity.
 */
public interface HoraconSearchRepository extends ElasticsearchRepository<Horacon, Long> {
}
