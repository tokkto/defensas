package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Tipproveid;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Tipproveid entity.
 */
public interface TipproveidSearchRepository extends ElasticsearchRepository<Tipproveid, Long> {
}
