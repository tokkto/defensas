package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Resolucrd;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Resolucrd entity.
 */
public interface ResolucrdSearchRepository extends ElasticsearchRepository<Resolucrd, Long> {
}
