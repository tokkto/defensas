package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Concilia;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Concilia entity.
 */
public interface ConciliaSearchRepository extends ElasticsearchRepository<Concilia, Long> {
}
