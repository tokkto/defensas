package pe.gob.trabajo.repository.search;

import pe.gob.trabajo.domain.Multaconci;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data Elasticsearch repository for the Multaconci entity.
 */
public interface MultaconciSearchRepository extends ElasticsearchRepository<Multaconci, Long> {
}
