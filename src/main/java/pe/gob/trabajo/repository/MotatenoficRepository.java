package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Motatenofic;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Motatenofic entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MotatenoficRepository extends JpaRepository<Motatenofic, Long> {

    // @Query("select M from Motatenofic M " +
    // "where M.oficina.id = ?1 " +
    // "and M.nFlgactivo = 1 ")
    // List<Motatenofic> findMotAteOfi(Long ifofic);

    // @Query("select M.id, M.pasegl.id,M.motatenofic.id, M.vObsmotpas from Motivpase M " +
    // "where M.pasegl.id = ?1 and M.motatenofic.oficina.id = ?2 " +
    // "and M.nFlgactivo = 1 ")

    @Query("select new map(M as Motateno, MP.id as idmotpase, MP.vObsmotpas as observacion, MP.nUsuareg as nUsuareg, MP.nSedereg as nSedereg, MP.tFecreg as tFecreg) " +
    "from Motatenofic M left join Motivpase MP on M.id = MP.motatenofic.id and MP.pasegl.id = ?1 " + 
    "where M.nFlgactivo = 1  and M.oficina.id = 2 order by M.id")
    List<Motatenofic> findMotAteOfi(Long id);
}
