package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Pernatural;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Pernatural entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PernaturalRepository extends JpaRepository<Pernatural, Long> {

    @Query("select P from Pernatural P " +
    "where P.tipdocident.id = ?1 and P.vNumdoc = ?2 " +
    "and P.nFlgactivo = 1 ")
     List<Pernatural>findPerNatNDoc(Long tip_doc, String nro_doc);


}
