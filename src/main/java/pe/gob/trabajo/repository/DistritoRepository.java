package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Distrito;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Distrito entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DistritoRepository extends JpaRepository<Distrito, String> {

    @Query("select distrito from Distrito distrito where distrito.vCoddep=?1 and distrito.vCodpro=?2 and distrito.vFlgact='1' order by distrito.vCoddis")
    List<Distrito> findFilterDistritos(String id, String idprov);
}
