package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Notifica;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Notifica entity.
 */
@SuppressWarnings("unused")
@Repository
public interface NotificaRepository extends JpaRepository<Notifica, Long> {

}
