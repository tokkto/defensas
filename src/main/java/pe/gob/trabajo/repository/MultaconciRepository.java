package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Multaconci;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Multaconci entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MultaconciRepository extends JpaRepository<Multaconci, Long> {

}
