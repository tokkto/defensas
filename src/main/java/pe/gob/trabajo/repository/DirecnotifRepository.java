package pe.gob.trabajo.repository;

import java.util.List;
import pe.gob.trabajo.domain.Direcnotif;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Direcnotif entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DirecnotifRepository extends JpaRepository<Direcnotif, Long> {
    
    @Query("select D from Direcnotif D " +
    "where D.notifica.id = ?1 " +
    "and D.nFlgactivo = 1 order by D.notifica.id")
    List<Direcnotif>findDirecnotifParam(Long  idnotifica);
    
    /* @Query("select new map( " +
        "(select dep.vDesdep from Departamento dep where dep.vCoddep=dirpernat.nCoddepto) as dpto "+
        ", (select provin.vDespro from Provincia provin where provin.vCodpro=dirpernat.nCodprov and provin.vCoddep=dirpernat.nCoddepto) as prov " +
        ", (select distri.vDesdis from Distrito distri where distri.vCoddis=dirpernat.nCoddist and distri.vCodpro=dirpernat.nCodprov and distri.vCoddep=dirpernat.nCoddepto) as dist" +
        ", dirpernat as direc " +
        ")" +
        "from Dirpernat dirpernat inner join Trabajador trabajador on dirpernat.pernatural.id=trabajador.pernatural.id  " +
        "where trabajador.id=?1 and dirpernat.nFlgactivo = true order by dirpernat.nFlgnotifi desc ")
    List<Dirpernat> findListDireccionesTrabajadorById(Long id);

    */
}
