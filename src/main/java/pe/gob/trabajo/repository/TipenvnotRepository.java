package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipenvnot;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipenvnot entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipenvnotRepository extends JpaRepository<Tipenvnot, Long> {

}
