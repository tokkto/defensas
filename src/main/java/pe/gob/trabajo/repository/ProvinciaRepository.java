package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Provincia;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Provincia entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ProvinciaRepository extends JpaRepository<Provincia, String> {

    @Query("select provincia from Provincia provincia where provincia.vCoddep=?1 and provincia.vFlgact='1' order by provincia.vCodpro")
    List<Provincia> findFilterProvincias(String id);

}
