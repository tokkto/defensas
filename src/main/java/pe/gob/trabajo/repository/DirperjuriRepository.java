package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Dirperjuri;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Dirperjuri entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DirperjuriRepository extends JpaRepository<Dirperjuri, Long> {

    @Query("select D from Dirperjuri D " +
    "Inner Join Departamento Dep on D.nCoddepto = Dep.vCoddep " +
    "Inner Join Provincia Prov on D.nCodprov = Prov.vCodpro and D.nCoddepto = Prov.vCoddep " +
    "Inner Join Distrito Dist on D.nCoddist = Dist.vCoddis and D.nCodprov = Dist.vCodpro and D.nCoddepto = Dist.vCoddep " +
    "where D.perjuridica.id = ?1 " +
    "and D.nFlgactivo = 1 ")
    List<Dirperjuri>findDirPerJur(Long id_perjur);

    @Query("select new map( " +
        "dirperjuri as direc "+
        ", (select dep.vDesdep from Departamento dep where dep.vCoddep=dirperjuri.nCoddepto) as dpto "+
        ", (select provin.vDespro from Provincia provin where provin.vCodpro=dirperjuri.nCodprov and provin.vCoddep=dirperjuri.nCoddepto) as prov " +
        ", (select distri.vDesdis from Distrito distri where distri.vCoddis=dirperjuri.nCoddist and distri.vCodpro=dirperjuri.nCodprov and distri.vCoddep=dirperjuri.nCoddepto) as dist" +
        ")"+
        "from Dirperjuri dirperjuri inner join Empleador empleador on dirperjuri.perjuridica.id=empleador.perjuridica.id  " +
        "where empleador.perjuridica.id=?1 and dirperjuri.nFlgactivo = true")
     List<Dirperjuri> findListDireccionesEmpleadorById(Long id);
   
}
