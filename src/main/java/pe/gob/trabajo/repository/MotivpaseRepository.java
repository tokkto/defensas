package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Motivpase;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Motivpase entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MotivpaseRepository extends JpaRepository<Motivpase, Long> {

    @Query("select M.id, M.pasegl.id,M.motatenofic.id, M.vObsmotpas from Motivpase M " +
    "where M.pasegl.id = ?1 and M.motatenofic.oficina.id = ?2 " +
    "and M.nFlgactivo = 1 ")
    List<Motivpase> findMotPaseOfic(Long id_pase, Long id_ofic);

}
