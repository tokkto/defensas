package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Resolucrd;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Resolucrd entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResolucrdRepository extends JpaRepository<Resolucrd, Long> {

}
