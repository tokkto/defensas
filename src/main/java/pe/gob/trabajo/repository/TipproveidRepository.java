package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Tipproveid;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Tipproveid entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TipproveidRepository extends JpaRepository<Tipproveid, Long> {
    @Query("select T from Tipproveid T " +
    "where T.nFlgactivo = 1 and T.tipdocexp.id = ?1")
    List<Tipproveid> ListarTipproveidActivosPorTDocExp(Long id);
}
