package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Concilia;
import pe.gob.trabajo.domain.Cancohorfec;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Concilia entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ConciliaRepository extends JpaRepository<Concilia, Long> {

    @Query("select C from Concilia C " +
    "where C.expediente.id = ?1 " +
    "and C.nFlgactivo = 1 ")
    List<Concilia> ListarAudienciasConciliacionExp(Long id_exp);

    //Lista las audiencias de conciliacion que no tienen asignado un conciliador por fecha
    @Query("select C from Concilia C " +
    "where to_date(cast(C.dFecconci as date)) = to_date(?1,'DD/MM/YYYY') and C.resulconci.id is null " +
    "and to_date(cast(C.dFecconci as date)) >= to_date(cast( sysdate as date)) " +
    "and C.nFlgactivo = 1 ")
    List<Concilia> ListarAudienciasConcSinAbog(String fecha);

    //Lista las audiencias de conciliacion que no tienen asignado un conciliador por fecha
    @Query("select C from Concilia C " +
    "where C.resulconci.id is null " + 
    "and to_date(cast(C.dFecconci as date)) >= to_date(cast( sysdate as date)) " +
    "and C.nFlgactivo = 1 ")
    List<Concilia> ListarAudienciasConcSinAbog();

    //Lista las audiencias de conciliacion que no tienen un resultado de conciliacion(Para asig Res Conc)
    @Query("select C from Concilia C " +
    "where C.resulconci.id is null " + 
    "and C.nFlgactivo = 1 ")
    List<Concilia> ListarAudienciasConcSinResul();

    @Query("select count(1) from Cancohorfec C where " + 
    "to_date(cast(C.dFecconci as date)) =  to_date(?1,'DD/MM/YYYY') and C.horacon.id = ?2 " + 
    "and C.nFlgactivo = 1 " + 
    "and C.nCantcon = (Select P.nCantcon from Perporhor P " + 
    "where to_date(?1,'DD/MM/YYYY') between  to_date(cast(P.dFecinicio as date)) " + 
    "and to_date(cast(P.dFecfin as date)) and P.nFlgactivo = 1 ) ")
    Integer ValidarCantConciPorHora(String fecha, Long hora);

    @Query("select C.nCantcon from Cancohorfec C where " + 
    "to_date(cast(C.dFecconci as date)) =  to_date(?1,'DD/MM/YYYY') and C.horacon.id = ?2 " + 
    "and C.nFlgactivo = 1 ")
    Integer CantConciPorHora(String fecha, Long hora);

    @Query("Select P.nCantcon from Perporhor P " + 
    "where to_date(?1,'DD/MM/YYYY') between  to_date(cast(P.dFecinicio as date)) " + 
    "and to_date(cast(P.dFecfin as date)) and P.nFlgactivo = 1 ")
    Integer CantConciPorHoraPeriodo(String fecha);

    @Query("select C.id from Cancohorfec C where " + 
    "to_date(cast(C.dFecconci as date)) =  to_date(?1,'DD/MM/YYYY') and C.horacon.id = ?2 " + 
    "and C.nFlgactivo = 1 ")
    Integer CantConciPorHoraId(String fecha, Long hora);
    
    @Query("select C from Cancohorfec C where " + 
    "to_date(cast(C.dFecconci as date)) =  to_date(?1,'DD/MM/YYYY') and C.horacon.id = ?2 " + 
    "and C.nFlgactivo = 1 ")
    Cancohorfec CantConciPorHoraObjeto(String fecha, Long hora);

    @Query("select C from Concilia C " +
    "where to_date(cast(C.dFecconci as date)) between to_date(?1,'DD/MM/YYYY')  and to_date(?2,'DD/MM/YYYY') " +
    "and C.nFlgactivo = 1 order by C.expediente.vNumexp")
    List<Concilia>findConciliaFechaParam(String fec_ini, String fec_fin);

    @Query("select C from Concilia C " +
    "where C.expediente.vNumexp like %?1% " +
    "and C.nFlgactivo = 1 order by C.expediente.vNumexp")
    List<Concilia>findConciliaNunExpParam(String num_exp);

    @Query("select C from Concilia C " +
    "where C.abogado.id = ?1 " +
    "and C.nFlgactivo = 1 order by C.expediente.vNumexp")
    List<Concilia>findConciliaAbogadoParam(Long  idabogado);

    //Maxima fecha de registro por expediente(Busqueda expediente emitido)
    @Query("select C from Concilia C " +
    "where (C.expediente.id, C.tFecreg) in " +
    "(select Con.expediente.id, max(Con.tFecreg) " +
    "from Concilia Con where Con.expediente.vNumexp like %?1% and " + 
    "Con.nFlgactivo = 1 group by Con.expediente.id ) " +
    "order by C.expediente.vNumexp")    
    List<Concilia>findConciliaNunExpMaxFecParam(String num_exp);

    //Maxima fecha de registro por fecha de expediente(Busqueda expediente emitido)
    @Query("select C from Concilia C " +
    "where (C.expediente.id, C.tFecreg) in " +
    "(select Con.expediente.id, max(Con.tFecreg) " +
    "from Concilia Con where to_date(cast(Con.expediente.dFecregexp as date)) between to_date(?1,'DD/MM/YYYY')  and to_date(?2,'DD/MM/YYYY') and " + 
    "Con.nFlgactivo = 1 group by Con.expediente.id ) " +
    "order by C.expediente.dFecregexp")    
    List<Concilia>findConciliaFechaMaxFecParam(String fec_ini, String fec_fin);

    //Maxima fecha de registro por expediente(Busqueda expediente para multar)
    @Query("select C from Concilia C " +
    "where (C.expediente.id, C.tFecreg) in " +
    "(select Con.expediente.id, max(Con.tFecreg) " +
    "from Concilia Con where Con.expediente.vNumexp like %?1% and " + 
    "Con.resulconci.tipresconc.id = 2 and " + 
    "Con.nFlgactivo = 1 group by Con.expediente.id ) " +
    "order by C.expediente.vNumexp")    
    List<Concilia>findConciliaParaMultarNroExpParam(String num_exp);

    @Query("select C from Concilia C " +
    "where (C.expediente.id, C.tFecreg) in " +
    "(select Con.expediente.id, max(Con.tFecreg) " +
    "from Concilia Con where to_date(cast(Con.expediente.dFecregexp as date)) between to_date(?1,'DD/MM/YYYY')  and to_date(?2,'DD/MM/YYYY') and " + 
    "Con.resulconci.tipresconc.id = 2 and " + 
    "Con.nFlgactivo = 1 group by Con.expediente.id ) " +
    "order by C.expediente.vNumexp")    
    List<Concilia>findConciliaParaMultarFechaParam(String fec_ini, String fec_fin);

}
