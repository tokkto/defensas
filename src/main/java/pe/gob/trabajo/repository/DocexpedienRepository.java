package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Docexpedien;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Docexpedien entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DocexpedienRepository extends JpaRepository<Docexpedien, Long> {

    @Query("select D from Docexpedien D " +
    "where D.expediente.id = ?1 " +
    "and D.nFlgactivo = 1 order by D.tFecreg")
    List<Docexpedien>findDocExpId(Long  idabogado);
}
