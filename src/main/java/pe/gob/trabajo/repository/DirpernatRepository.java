package pe.gob.trabajo.repository;

import pe.gob.trabajo.domain.Dirpernat;
import org.springframework.stereotype.Repository;
import java.util.List;
import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Dirpernat entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DirpernatRepository extends JpaRepository<Dirpernat, Long> {

    @Query("select D from Dirpernat D " +
    "where D.pernatural.id = ?1 " +
    "and D.nFlgactivo = 1 ")
     List<Dirpernat>findDirPerNat(Long id_pernat);

     @Query("select new map( " +
        "dirpernat as direc "+
        ", (select dep.vDesdep from Departamento dep where dep.vCoddep=dirpernat.nCoddepto) as dpto "+
        ", (select provin.vDespro from Provincia provin where provin.vCodpro=dirpernat.nCodprov and provin.vCoddep=dirpernat.nCoddepto) as prov " +
        ", (select distri.vDesdis from Distrito distri where distri.vCoddis=dirpernat.nCoddist and distri.vCodpro=dirpernat.nCodprov and distri.vCoddep=dirpernat.nCoddepto) as dist" +
        ")"+
        "from Dirpernat dirpernat inner join Trabajador trabajador on dirpernat.pernatural.id=trabajador.pernatural.id  " +
        "where trabajador.pernatural.id=?1 and dirpernat.nFlgactivo = true")
    List<Dirpernat> findListDireccionesTrabajadorById(Long id);

    @Query("select new map( " +
        "dirpernat as direc "+
        ", (select dep.vDesdep from Departamento dep where dep.vCoddep=dirpernat.nCoddepto) as dpto "+
        ", (select provin.vDespro from Provincia provin where provin.vCodpro=dirpernat.nCodprov and provin.vCoddep=dirpernat.nCoddepto) as prov " +
        ", (select distri.vDesdis from Distrito distri where distri.vCoddis=dirpernat.nCoddist and distri.vCodpro=dirpernat.nCodprov and distri.vCoddep=dirpernat.nCoddepto) as dist" +
        ")"+
        "from Dirpernat dirpernat inner join Empleador empleador on dirpernat.pernatural.id=empleador.pernatural.id  " +
        "where empleador.pernatural.id=?1 and dirpernat.nFlgactivo = true")
     List<Dirpernat> findListDireccionesEmpleadorById(Long id);
}


