package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Horacon.
 */
@Entity
@Table(name = "COTBC_HORACON")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "horacon")
public class Horacon implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codhora", nullable = false)
    private Long id;

    @NotNull
    @Size(max = 20)
    @Column(name = "v_descrip", length = 20, nullable = false)
    private String vDescrip;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @OneToMany(mappedBy = "horacon")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Concilia> concilias = new HashSet<>();

    @OneToMany(mappedBy = "horacon")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Cancohorfec> cancohorfecs = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvDescrip() {
        return vDescrip;
    }

    public Horacon vDescrip(String vDescrip) {
        this.vDescrip = vDescrip;
        return this;
    }

    public void setvDescrip(String vDescrip) {
        this.vDescrip = vDescrip;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Horacon nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Horacon tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Horacon nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Horacon nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Horacon nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Horacon tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Horacon nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Set<Concilia> getConcilias() {
        return concilias;
    }

    public Horacon concilias(Set<Concilia> concilias) {
        this.concilias = concilias;
        return this;
    }

    public Horacon addConcilia(Concilia concilia) {
        this.concilias.add(concilia);
        concilia.setHoracon(this);
        return this;
    }

    public Horacon removeConcilia(Concilia concilia) {
        this.concilias.remove(concilia);
        concilia.setHoracon(null);
        return this;
    }

    public void setConcilias(Set<Concilia> concilias) {
        this.concilias = concilias;
    }

    public Set<Cancohorfec> getCancohorfec() {
        return cancohorfecs;
    }

    public Horacon cancohorfecs(Set<Cancohorfec> cancohorfecs) {
        this.cancohorfecs = cancohorfecs;
        return this;
    }

    public Horacon addConcilia(Cancohorfec cancohorfec) {
        this.cancohorfecs.add(cancohorfec);
        cancohorfec.setHoracon(this);
        return this;
    }

    public Horacon removeCancohorfec(Cancohorfec cancohorfec) {
        this.cancohorfecs.remove(cancohorfec);
        cancohorfec.setHoracon(null);
        return this;
    }

    public void setCancohorfecs(Set<Cancohorfec> cancohorfecs) {
        this.cancohorfecs = cancohorfecs;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Horacon horacon = (Horacon) o;
        if (horacon.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), horacon.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Horacon{" +
            "id=" + getId() +
            ", vDescrip='" + getvDescrip() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
