package pe.gob.trabajo.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

/**
 * A Docexpedien.
 */
@Entity
@Table(name = "DLMVC_DOCEXPEDIEN")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "docexpedien")
public class Docexpedien implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_coddocexp", nullable = false)
    private Long id;

    @Size(max = 20)
    @Column(name = "v_numoficio", length = 20)
    private String vNumoficio;

    @Column(name = "d_fechadoc")
    private LocalDate dFechadoc;

    @Column(name = "n_folios")
    private Integer nFolios;

    @Size(max = 20)
    @Column(name = "v_numresord", length = 20)
    private String vNumresord;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    @JoinColumn(name = "n_codexp")
    private Expediente expediente;

    @ManyToOne
    @JoinColumn(name = "n_coddtipro")
    private Dettipprov dettipprov;

    @ManyToOne
    @JoinColumn(name = "n_codtdocex")
    private Tipdocexp tipdocexp;

    @ManyToOne
    @JoinColumn(name = "n_codtiprov")
    private Tipproveid tipproveid;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvNumoficio() {
        return vNumoficio;
    }

    public Docexpedien vNumoficio(String vNumoficio) {
        this.vNumoficio = vNumoficio;
        return this;
    }

    public void setvNumoficio(String vNumoficio) {
        this.vNumoficio = vNumoficio;
    }

    public LocalDate getdFechadoc() {
        return dFechadoc;
    }

    public Docexpedien dFechadoc(LocalDate dFechadoc) {
        this.dFechadoc = dFechadoc;
        return this;
    }

    public void setdFechadoc(LocalDate dFechadoc) {
        this.dFechadoc = dFechadoc;
    }

    public Integer getnFolios() {
        return nFolios;
    }

    public Docexpedien nFolios(Integer nFolios) {
        this.nFolios = nFolios;
        return this;
    }

    public void setnFolios(Integer nFolios) {
        this.nFolios = nFolios;
    }

    public String getvNumresord() {
        return vNumresord;
    }

    public Docexpedien vNumresord(String vNumresord) {
        this.vNumresord = vNumresord;
        return this;
    }

    public void setvNumresord(String vNumresord) {
        this.vNumresord = vNumresord;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Docexpedien nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Docexpedien tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Docexpedien nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Docexpedien nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Docexpedien nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Docexpedien tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Docexpedien nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public Docexpedien expediente(Expediente expediente) {
        this.expediente = expediente;
        return this;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Dettipprov getDettipprov() {
        return dettipprov;
    }

    public Docexpedien dettipprov(Dettipprov dettipprov) {
        this.dettipprov = dettipprov;
        return this;
    }

    public void setDettipprov(Dettipprov dettipprov) {
        this.dettipprov = dettipprov;
    }

    public Tipdocexp getTipdocexp() {
        return tipdocexp;
    }

    public Docexpedien tipdocexp(Tipdocexp tipdocexp) {
        this.tipdocexp = tipdocexp;
        return this;
    }

    public void setTipdocexp(Tipdocexp tipdocexp) {
        this.tipdocexp = tipdocexp;
    }

    public Tipproveid getTipproveid() {
        return tipproveid;
    }

    public Docexpedien tipproveid(Tipproveid tipproveid) {
        this.tipproveid = tipproveid;
        return this;
    }

    public void setTipproveid(Tipproveid tipproveid) {
        this.tipproveid = tipproveid;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Docexpedien docexpedien = (Docexpedien) o;
        if (docexpedien.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), docexpedien.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Docexpedien{" +
            "id=" + getId() +
            ", vNumoficio='" + getvNumoficio() + "'" +
            ", dFechadoc='" + getdFechadoc() + "'" +
            ", nFolios='" + getnFolios() + "'" +
            ", vNumresord='" + getvNumresord() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
