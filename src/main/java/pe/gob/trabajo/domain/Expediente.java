package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Expediente.
 */
@Entity
@Table(name = "COMVC_EXPEDIENTE")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "expediente")
public class Expediente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codexp", nullable = false)
    private Long id;

    @Size(max = 20)
    @Column(name = "v_numexp", length = 20, nullable = false)
    private String vNumexp;

    @NotNull
    @Column(name = "n_anioexp", nullable = false)
    private Integer nAnioexp;

    @Column(name = "d_fecregexp")
    private LocalDate dFecregexp;

    @Size(max = 250)
    @Column(name = "v_nomemplea", length = 250)
    private String vNomemplea;

    @Size(max = 250)
    @Column(name = "v_nomtrabaj", length = 250)
    private String vNomtrabaj;

    @Column(name = "n_flgexpobs")
    private Boolean nFlgexpobs;

    @Size(max = 50)
    @Column(name = "v_regmespar", length = 50)
    private String vRegmespar;

    @Column(name = "d_fecmespar")
    private LocalDate dFecmespar;

    @Size(max = 500)
    @Column(name = "v_observac", length = 500)
    private String vObservac;

    @Column(name = "n_flgarchiv")
    private Boolean nFlgarchiv;

    @Column(name = "d_fecarchiv")
    private LocalDate dFecArchiv;

    @Size(max = 500)
    @Column(name = "v_obsarchiv", length = 500)
    private String vObsarchiv;

    @Column(name = "n_codusuarc")
    private Integer nCodusuarc;

    @Size(max = 50)
    @Column(name = "v_numinfarc", length = 50)
    private String vNuminfarc;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    @JoinColumn(name = "N_CODEMPLEA")
    private Empleador empleador;

    @ManyToOne
    @JoinColumn(name = "N_CODTIPPER")
    private Tippersona tippersona;

    @ManyToOne
    @JoinColumn(name = "n_codtrab")
    private Trabajador trabajador;

    @OneToOne
    @JoinColumn(unique = true, name = "N_CODPASE")
    private Pasegl pasegl;

    @OneToMany(mappedBy = "expediente")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Concilia> concilias = new HashSet<>();

    @OneToMany(mappedBy = "expediente")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Docexpedien> docexpediens = new HashSet<>();

    @OneToMany(mappedBy = "expediente")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Resolucrd> resolucrds = new HashSet<>();

    @OneToMany(mappedBy = "expediente")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Notifica> notificas = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "N_CODESTEXP")
    private Estexpedien estexpedien;

    @ManyToOne
    @JoinColumn(name = "N_CODRESOL")
    private Resolutor resolutor;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvNumexp() {
        return vNumexp;
    }

    public Expediente vNumexp(String vNumexp) {
        this.vNumexp = vNumexp;
        return this;
    }

    public void setvNumexp(String vNumexp) {
        this.vNumexp = vNumexp;
    }

    public Integer getnAnioexp() {
        return nAnioexp;
    }

    public Expediente nAnioexp(Integer nAnioexp) {
        this.nAnioexp = nAnioexp;
        return this;
    }

    public void setnAnioexp(Integer nAnioexp) {
        this.nAnioexp = nAnioexp;
    }

    public LocalDate getdFecregexp() {
        return dFecregexp;
    }

    public Expediente dFecregexp(LocalDate dFecregexp) {
        this.dFecregexp = dFecregexp;
        return this;
    }

    public void setdFecregexp(LocalDate dFecregexp) {
        this.dFecregexp = dFecregexp;
    }

    public String getvNomemplea() {
        return vNomemplea;
    }

    public Expediente vNomemplea(String vNomemplea) {
        this.vNomemplea = vNomemplea;
        return this;
    }

    public void setvNomemplea(String vNomemplea) {
        this.vNomemplea = vNomemplea;
    }

    public String getvNomtrabaj() {
        return vNomtrabaj;
    }

    public Expediente vNomtrabaj(String vNomtrabaj) {
        this.vNomtrabaj = vNomtrabaj;
        return this;
    }

    public void setvNomtrabaj(String vNomtrabaj) {
        this.vNomtrabaj = vNomtrabaj;
    }

    public Boolean isnFlgexpobs() {
        return nFlgexpobs;
    }

    public Expediente nFlgexpobs(Boolean nFlgexpobs) {
        this.nFlgexpobs = nFlgexpobs;
        return this;
    }

    public void setnFlgexpobs(Boolean nFlgexpobs) {
        this.nFlgexpobs = nFlgexpobs;
    }

    public String getvRegmespar() {
        return vRegmespar;
    }

    public Expediente vRegmespar(String vRegmespar) {
        this.vRegmespar = vRegmespar;
        return this;
    }

    public void setvRegmespar(String vRegmespar) {
        this.vRegmespar = vRegmespar;
    }

    public LocalDate getdFecmespar() {
        return dFecmespar;
    }

    public Expediente dFecmespar(LocalDate dFecmespar) {
        this.dFecmespar = dFecmespar;
        return this;
    }

    public void setdFecmespar(LocalDate dFecmespar) {
        this.dFecmespar = dFecmespar;
    }

    public String getvObservac() {
        return vObservac;
    }

    public Expediente vObservac(String vObservac) {
        this.vObservac = vObservac;
        return this;
    }

    public void setvObservac(String vObservac) {
        this.vObservac = vObservac;
    }

    public Boolean isnFlgarchiv() {
        return nFlgarchiv;
    }

    public Expediente nFlgarchiv(Boolean nFlgarchiv) {
        this.nFlgarchiv = nFlgarchiv;
        return this;
    }

    public void setnFlgarchiv(Boolean nFlgarchiv) {
        this.nFlgarchiv = nFlgarchiv;
    }

    public LocalDate getdFecArchiv() {
        return dFecArchiv;
    }

    public Expediente dFecArchiv(LocalDate dFecArchiv) {
        this.dFecArchiv = dFecArchiv;
        return this;
    }

    public void setdFecArchiv(LocalDate dFecArchiv) {
        this.dFecArchiv = dFecArchiv;
    }

    public String getvObsarchiv() {
        return vObsarchiv;
    }

    public Expediente vObsarchiv(String vObsarchiv) {
        this.vObsarchiv = vObsarchiv;
        return this;
    }

    public void setvObsarchiv(String vObsarchiv) {
        this.vObsarchiv = vObsarchiv;
    }

    public Integer getnCodusuarc() {
        return nCodusuarc;
    }

    public Expediente nCodusuarc(Integer nCodusuarc) {
        this.nCodusuarc = nCodusuarc;
        return this;
    }

    public void setnCodusuarc(Integer nCodusuarc) {
        this.nCodusuarc = nCodusuarc;
    }

    public String getvNuminfarc() {
        return vNuminfarc;
    }

    public Expediente vNuminfarc(String vNuminfarc) {
        this.vNuminfarc = vNuminfarc;
        return this;
    }

    public void setvNuminfarc(String vNuminfarc) {
        this.vNuminfarc = vNuminfarc;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Expediente nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Expediente tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Expediente nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Expediente nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Expediente nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Expediente tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Expediente nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Empleador getEmpleador() {
        return empleador;
    }

    public Expediente empleador(Empleador empleador) {
        this.empleador = empleador;
        return this;
    }

    public void setEmpleador(Empleador empleador) {
        this.empleador = empleador;
    }

    public Tippersona getTippersona() {
        return tippersona;
    }

    public Expediente tippersona(Tippersona tippersona) {
        this.tippersona = tippersona;
        return this;
    }

    public void setTippersona(Tippersona tippersona) {
        this.tippersona = tippersona;
    }

    public Trabajador getTrabajador() {
        return trabajador;
    }

    public Expediente trabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
        return this;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }

    public Pasegl getPasegl() {
        return pasegl;
    }

    public Expediente pasegl(Pasegl pasegl) {
        this.pasegl = pasegl;
        return this;
    }

    public void setPasegl(Pasegl pasegl) {
        this.pasegl = pasegl;
    }

    public Set<Concilia> getConcilias() {
        return concilias;
    }

    public Expediente concilias(Set<Concilia> concilias) {
        this.concilias = concilias;
        return this;
    }

    public Expediente addConcilia(Concilia concilia) {
        this.concilias.add(concilia);
        concilia.setExpediente(this);
        return this;
    }

    public Expediente removeConcilia(Concilia concilia) {
        this.concilias.remove(concilia);
        concilia.setExpediente(null);
        return this;
    }

    public void setConcilias(Set<Concilia> concilias) {
        this.concilias = concilias;
    }

    public Set<Docexpedien> getDocexpediens() {
        return docexpediens;
    }

    public Expediente docexpediens(Set<Docexpedien> docexpediens) {
        this.docexpediens = docexpediens;
        return this;
    }

    public Expediente addDocexpedien(Docexpedien docexpedien) {
        this.docexpediens.add(docexpedien);
        docexpedien.setExpediente(this);
        return this;
    }

    public Expediente removeDocexpedien(Docexpedien docexpedien) {
        this.docexpediens.remove(docexpedien);
        docexpedien.setExpediente(null);
        return this;
    }

    public void setDocexpediens(Set<Docexpedien> docexpediens) {
        this.docexpediens = docexpediens;
    }

    public Set<Resolucrd> getResolucrds() {
        return resolucrds;
    }

    public Expediente resolucrds(Set<Resolucrd> resolucrds) {
        this.resolucrds = resolucrds;
        return this;
    }

    public Expediente addResolucrd(Resolucrd resolucrd) {
        this.resolucrds.add(resolucrd);
        resolucrd.setExpediente(this);
        return this;
    }

    public Expediente removeResolucrd(Resolucrd resolucrd) {
        this.resolucrds.remove(resolucrd);
        resolucrd.setExpediente(null);
        return this;
    }

    public void setResolucrds(Set<Resolucrd> resolucrds) {
        this.resolucrds = resolucrds;
    }

    public Set<Notifica> getNotificas() {
        return notificas;
    }

    public Expediente notificas(Set<Notifica> notificas) {
        this.notificas = notificas;
        return this;
    }

    public Expediente addNotifica(Notifica notifica) {
        this.notificas.add(notifica);
        notifica.setExpediente(this);
        return this;
    }

    public Expediente removeNotifica(Notifica notifica) {
        this.notificas.remove(notifica);
        notifica.setExpediente(null);
        return this;
    }

    public void setNotificas(Set<Notifica> notificas) {
        this.notificas = notificas;
    }

    public Estexpedien getEstexpedien() {
        return estexpedien;
    }

    public Expediente estexpedien(Estexpedien estexpedien) {
        this.estexpedien = estexpedien;
        return this;
    }

    public void setEstexpedien(Estexpedien estexpedien) {
        this.estexpedien = estexpedien;
    }

    public Resolutor getResolutor() {
        return resolutor;
    }

    public Expediente resolutor(Resolutor resolutor) {
        this.resolutor = resolutor;
        return this;
    }

    public void setResolutor(Resolutor resolutor) {
        this.resolutor = resolutor;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Expediente expediente = (Expediente) o;
        if (expediente.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), expediente.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Expediente{" +
            "id=" + getId() +
            ", vNumexp='" + getvNumexp() + "'" +
            ", nAnioexp='" + getnAnioexp() + "'" +
            ", dFecregexp='" + getdFecregexp() + "'" +
            ", vNomemplea='" + getvNomemplea() + "'" +
            ", vNomtrabaj='" + getvNomtrabaj() + "'" +
            ", nFlgexpobs='" + isnFlgexpobs() + "'" +
            ", vRegmespar='" + getvRegmespar() + "'" +
            ", dFecmespar='" + getdFecmespar() + "'" +
            ", vObservac='" + getvObservac() + "'" +
            ", nFlgarchiv='" + isnFlgarchiv() + "'" +
            ", dFecArchiv='" + getdFecArchiv() + "'" +
            ", vObsarchiv='" + getvObsarchiv() + "'" +
            ", nCodusuarc='" + getnCodusuarc() + "'" +
            ", vNuminfarc='" + getvNuminfarc() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
