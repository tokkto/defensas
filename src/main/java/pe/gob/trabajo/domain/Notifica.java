package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Notifica.
 */
@Entity
@Table(name = "DLMVC_NOTIFICA")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "notifica")
public class Notifica implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codnot", nullable = false)
    private Long id;

    @Column(name = "n_numfolios")
    private Integer nNumfolios;

    @Size(max = 20)
    @Column(name = "v_hojaenvio", length = 20)
    private String vHojaenvio;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    @JoinColumn(name = "n_codexp")
    private Expediente expediente;

    @OneToMany(mappedBy = "notifica")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Direcnotif> direcnotifs = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "n_codtenvno")
    private Tipenvnot tipenvnot;

    @ManyToOne
    @JoinColumn(name = "n_codtipnot")
    private Tipnotif tipnotif;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getnNumfolios() {
        return nNumfolios;
    }

    public Notifica nNumfolios(Integer nNumfolios) {
        this.nNumfolios = nNumfolios;
        return this;
    }

    public void setnNumfolios(Integer nNumfolios) {
        this.nNumfolios = nNumfolios;
    }

    public String getvHojaenvio() {
        return vHojaenvio;
    }

    public Notifica vHojaenvio(String vHojaenvio) {
        this.vHojaenvio = vHojaenvio;
        return this;
    }

    public void setvHojaenvio(String vHojaenvio) {
        this.vHojaenvio = vHojaenvio;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Notifica nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Notifica tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Notifica nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Notifica nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Notifica nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Notifica tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Notifica nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public Notifica expediente(Expediente expediente) {
        this.expediente = expediente;
        return this;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Set<Direcnotif> getDirecnotifs() {
        return direcnotifs;
    }

    public Notifica direcnotifs(Set<Direcnotif> direcnotifs) {
        this.direcnotifs = direcnotifs;
        return this;
    }

    public Notifica addDirecnotif(Direcnotif direcnotif) {
        this.direcnotifs.add(direcnotif);
        direcnotif.setNotifica(this);
        return this;
    }

    public Notifica removeDirecnotif(Direcnotif direcnotif) {
        this.direcnotifs.remove(direcnotif);
        direcnotif.setNotifica(null);
        return this;
    }

    public void setDirecnotifs(Set<Direcnotif> direcnotifs) {
        this.direcnotifs = direcnotifs;
    }

    public Tipenvnot getTipenvnot() {
        return tipenvnot;
    }

    public Notifica tipenvnot(Tipenvnot tipenvnot) {
        this.tipenvnot = tipenvnot;
        return this;
    }

    public void setTipenvnot(Tipenvnot tipenvnot) {
        this.tipenvnot = tipenvnot;
    }

    public Tipnotif getTipnotif() {
        return tipnotif;
    }

    public Notifica tipnotif(Tipnotif tipnotif) {
        this.tipnotif = tipnotif;
        return this;
    }

    public void setTipnotif(Tipnotif tipnotif) {
        this.tipnotif = tipnotif;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Notifica notifica = (Notifica) o;
        if (notifica.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), notifica.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Notifica{" +
            "id=" + getId() +
            ", nNumfolios='" + getnNumfolios() + "'" +
            ", vHojaenvio='" + getvHojaenvio() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
