package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Tipdocexp.
 */
@Entity
@Table(name = "DLTBC_TIPDOCEXP")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "tipdocexp")
public class Tipdocexp implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codtdocex", nullable = false)
    private Long id;

    @NotNull
    @Size(max = 100)
    @Column(name = "v_descrip", length = 100, nullable = false)
    private String vDescrip;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @OneToMany(mappedBy = "tipdocexp")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Tipproveid> tipproveids = new HashSet<>();

    @OneToMany(mappedBy = "tipdocexp")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Docexpedien> docexpediens = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvDescrip() {
        return vDescrip;
    }

    public Tipdocexp vDescrip(String vDescrip) {
        this.vDescrip = vDescrip;
        return this;
    }

    public void setvDescrip(String vDescrip) {
        this.vDescrip = vDescrip;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Tipdocexp nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Tipdocexp tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Tipdocexp nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Tipdocexp nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Tipdocexp nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Tipdocexp tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Tipdocexp nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Set<Tipproveid> getTipproveids() {
        return tipproveids;
    }

    public Tipdocexp tipproveids(Set<Tipproveid> tipproveids) {
        this.tipproveids = tipproveids;
        return this;
    }

    public Tipdocexp addTipproveid(Tipproveid tipproveid) {
        this.tipproveids.add(tipproveid);
        tipproveid.setTipdocexp(this);
        return this;
    }

    public Tipdocexp removeTipproveid(Tipproveid tipproveid) {
        this.tipproveids.remove(tipproveid);
        tipproveid.setTipdocexp(null);
        return this;
    }

    public void setTipproveids(Set<Tipproveid> tipproveids) {
        this.tipproveids = tipproveids;
    }

    public Set<Docexpedien> getDocexpediens() {
        return docexpediens;
    }

    public Tipdocexp docexpediens(Set<Docexpedien> docexpediens) {
        this.docexpediens = docexpediens;
        return this;
    }

    public Tipdocexp addDocexpedien(Docexpedien docexpedien) {
        this.docexpediens.add(docexpedien);
        docexpedien.setTipdocexp(this);
        return this;
    }

    public Tipdocexp removeDocexpedien(Docexpedien docexpedien) {
        this.docexpediens.remove(docexpedien);
        docexpedien.setTipdocexp(null);
        return this;
    }

    public void setDocexpediens(Set<Docexpedien> docexpediens) {
        this.docexpediens = docexpediens;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tipdocexp tipdocexp = (Tipdocexp) o;
        if (tipdocexp.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tipdocexp.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Tipdocexp{" +
            "id=" + getId() +
            ", vDescrip='" + getvDescrip() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
