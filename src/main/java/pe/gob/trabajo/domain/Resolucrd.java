package pe.gob.trabajo.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.data.elasticsearch.annotations.Document;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Resolucrd.
 */
@Entity
@Table(name = "DLMVC_RESOLUCRD")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Document(indexName = "resolucrd")
public class Resolucrd implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "n_codresosd", nullable = false)
    private Long id;

    @Size(max = 20)
    @Column(name = "v_numresosd", length = 20)
    private String vNumresosd;

    @Column(name = "d_fecresosd")
    private LocalDate dFecresosd;

    @Size(max = 250)
    @Column(name = "v_nomemplea", length = 250)
    private String vNomemplea;

    @Size(max = 250)
    @Column(name = "v_nomtrabaj", length = 250)
    private String vNomtrabaj;

    @Size(max = 250)
    @Column(name = "v_direccion", length = 250)
    private String vDireccion;

    @Size(max = 250)
    @Column(name = "v_telefono", length = 250)
    private String vTelefono;

    @Column(name = "d_fecconcil")
    private LocalDate dFecconcil;

    @Size(max = 20)
    @Column(name = "v_horconcil", length = 20)
    private String vHorconcil;

    @Column(name = "d_fechanoti")
    private LocalDate dFechanoti;

    @Size(max = 20)
    @Column(name = "v_numnotifi", length = 20)
    private String vNumnotifi;

    @Column(name = "f_monmulta")
    private Float fMonmulta;

    @NotNull
    @Column(name = "n_usuareg", nullable = false)
    private Integer nUsuareg;

    @NotNull
    @Column(name = "t_fecreg", nullable = false)
    private Instant tFecreg;

    @NotNull
    @Column(name = "n_flgactivo", nullable = false)
    private Boolean nFlgactivo;

    @NotNull
    @Column(name = "n_sedereg", nullable = false)
    private Integer nSedereg;

    @Column(name = "n_usuaupd")
    private Integer nUsuaupd;

    @Column(name = "t_fecupd")
    private Instant tFecupd;

    @Column(name = "n_sedeupd")
    private Integer nSedeupd;

    @ManyToOne
    @JoinColumn(name = "n_codtipper")
    private Tippersona tippersona;

    @ManyToOne
    @JoinColumn(name = "n_codexp")
    private Expediente expediente;

    @OneToMany(mappedBy = "resolucrd")
    @JsonIgnore
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Multaconci> multaconcis = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getvNumresosd() {
        return vNumresosd;
    }

    public Resolucrd vNumresosd(String vNumresosd) {
        this.vNumresosd = vNumresosd;
        return this;
    }

    public void setvNumresosd(String vNumresosd) {
        this.vNumresosd = vNumresosd;
    }

    public LocalDate getdFecresosd() {
        return dFecresosd;
    }

    public Resolucrd dFecresosd(LocalDate dFecresosd) {
        this.dFecresosd = dFecresosd;
        return this;
    }

    public void setdFecresosd(LocalDate dFecresosd) {
        this.dFecresosd = dFecresosd;
    }

    public String getvNomemplea() {
        return vNomemplea;
    }

    public Resolucrd vNomemplea(String vNomemplea) {
        this.vNomemplea = vNomemplea;
        return this;
    }

    public void setvNomemplea(String vNomemplea) {
        this.vNomemplea = vNomemplea;
    }

    public String getvNomtrabaj() {
        return vNomtrabaj;
    }

    public Resolucrd vNomtrabaj(String vNomtrabaj) {
        this.vNomtrabaj = vNomtrabaj;
        return this;
    }

    public void setvNomtrabaj(String vNomtrabaj) {
        this.vNomtrabaj = vNomtrabaj;
    }

    public String getvDireccion() {
        return vDireccion;
    }

    public Resolucrd vDireccion(String vDireccion) {
        this.vDireccion = vDireccion;
        return this;
    }

    public void setvDireccion(String vDireccion) {
        this.vDireccion = vDireccion;
    }

    public String getvTelefono() {
        return vTelefono;
    }

    public Resolucrd vTelefono(String vTelefono) {
        this.vTelefono = vTelefono;
        return this;
    }

    public void setvTelefono(String vTelefono) {
        this.vTelefono = vTelefono;
    }

    public LocalDate getdFecconcil() {
        return dFecconcil;
    }

    public Resolucrd dFecconcil(LocalDate dFecconcil) {
        this.dFecconcil = dFecconcil;
        return this;
    }

    public void setdFecconcil(LocalDate dFecconcil) {
        this.dFecconcil = dFecconcil;
    }

    public String getvHorconcil() {
        return vHorconcil;
    }

    public Resolucrd vHorconcil(String vHorconcil) {
        this.vHorconcil = vHorconcil;
        return this;
    }

    public void setvHorconcil(String vHorconcil) {
        this.vHorconcil = vHorconcil;
    }

    public LocalDate getdFechanoti() {
        return dFechanoti;
    }

    public Resolucrd dFechanoti(LocalDate dFechanoti) {
        this.dFechanoti = dFechanoti;
        return this;
    }

    public void setdFechanoti(LocalDate dFechanoti) {
        this.dFechanoti = dFechanoti;
    }

    public String getvNumnotifi() {
        return vNumnotifi;
    }

    public Resolucrd vNumnotifi(String vNumnotifi) {
        this.vNumnotifi = vNumnotifi;
        return this;
    }

    public void setvNumnotifi(String vNumnotifi) {
        this.vNumnotifi = vNumnotifi;
    }

    public Float getfMonmulta() {
        return fMonmulta;
    }

    public Resolucrd fMonmulta(Float fMonmulta) {
        this.fMonmulta = fMonmulta;
        return this;
    }

    public void setfMonmulta(Float fMonmulta) {
        this.fMonmulta = fMonmulta;
    }

    public Integer getnUsuareg() {
        return nUsuareg;
    }

    public Resolucrd nUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
        return this;
    }

    public void setnUsuareg(Integer nUsuareg) {
        this.nUsuareg = nUsuareg;
    }

    public Instant gettFecreg() {
        return tFecreg;
    }

    public Resolucrd tFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
        return this;
    }

    public void settFecreg(Instant tFecreg) {
        this.tFecreg = tFecreg;
    }

    public Boolean isnFlgactivo() {
        return nFlgactivo;
    }

    public Resolucrd nFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
        return this;
    }

    public void setnFlgactivo(Boolean nFlgactivo) {
        this.nFlgactivo = nFlgactivo;
    }

    public Integer getnSedereg() {
        return nSedereg;
    }

    public Resolucrd nSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
        return this;
    }

    public void setnSedereg(Integer nSedereg) {
        this.nSedereg = nSedereg;
    }

    public Integer getnUsuaupd() {
        return nUsuaupd;
    }

    public Resolucrd nUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
        return this;
    }

    public void setnUsuaupd(Integer nUsuaupd) {
        this.nUsuaupd = nUsuaupd;
    }

    public Instant gettFecupd() {
        return tFecupd;
    }

    public Resolucrd tFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
        return this;
    }

    public void settFecupd(Instant tFecupd) {
        this.tFecupd = tFecupd;
    }

    public Integer getnSedeupd() {
        return nSedeupd;
    }

    public Resolucrd nSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
        return this;
    }

    public void setnSedeupd(Integer nSedeupd) {
        this.nSedeupd = nSedeupd;
    }

    public Tippersona getTippersona() {
        return tippersona;
    }

    public Resolucrd tippersona(Tippersona tippersona) {
        this.tippersona = tippersona;
        return this;
    }

    public void setTippersona(Tippersona tippersona) {
        this.tippersona = tippersona;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public Resolucrd expediente(Expediente expediente) {
        this.expediente = expediente;
        return this;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Set<Multaconci> getMultaconcis() {
        return multaconcis;
    }

    public Resolucrd multaconcis(Set<Multaconci> multaconcis) {
        this.multaconcis = multaconcis;
        return this;
    }

    public Resolucrd addMultaconci(Multaconci multaconci) {
        this.multaconcis.add(multaconci);
        multaconci.setResolucrd(this);
        return this;
    }

    public Resolucrd removeMultaconci(Multaconci multaconci) {
        this.multaconcis.remove(multaconci);
        multaconci.setResolucrd(null);
        return this;
    }

    public void setMultaconcis(Set<Multaconci> multaconcis) {
        this.multaconcis = multaconcis;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Resolucrd resolucrd = (Resolucrd) o;
        if (resolucrd.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), resolucrd.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Resolucrd{" +
            "id=" + getId() +
            ", vNumresosd='" + getvNumresosd() + "'" +
            ", dFecresosd='" + getdFecresosd() + "'" +
            ", vNomemplea='" + getvNomemplea() + "'" +
            ", vNomtrabaj='" + getvNomtrabaj() + "'" +
            ", vDireccion='" + getvDireccion() + "'" +
            ", vTelefono='" + getvTelefono() + "'" +
            ", dFecconcil='" + getdFecconcil() + "'" +
            ", vHorconcil='" + getvHorconcil() + "'" +
            ", dFechanoti='" + getdFechanoti() + "'" +
            ", vNumnotifi='" + getvNumnotifi() + "'" +
            ", fMonmulta='" + getfMonmulta() + "'" +
            ", nUsuareg='" + getnUsuareg() + "'" +
            ", tFecreg='" + gettFecreg() + "'" +
            ", nFlgactivo='" + isnFlgactivo() + "'" +
            ", nSedereg='" + getnSedereg() + "'" +
            ", nUsuaupd='" + getnUsuaupd() + "'" +
            ", tFecupd='" + gettFecupd() + "'" +
            ", nSedeupd='" + getnSedeupd() + "'" +
            "}";
    }
}
