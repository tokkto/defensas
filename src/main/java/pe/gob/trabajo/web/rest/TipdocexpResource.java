package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Tipdocexp;

import pe.gob.trabajo.repository.TipdocexpRepository;
import pe.gob.trabajo.repository.search.TipdocexpSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Tipdocexp.
 */
@RestController
@RequestMapping("/api")
public class TipdocexpResource {

    private final Logger log = LoggerFactory.getLogger(TipdocexpResource.class);

    private static final String ENTITY_NAME = "tipdocexp";

    private final TipdocexpRepository tipdocexpRepository;

    private final TipdocexpSearchRepository tipdocexpSearchRepository;

    public TipdocexpResource(TipdocexpRepository tipdocexpRepository, TipdocexpSearchRepository tipdocexpSearchRepository) {
        this.tipdocexpRepository = tipdocexpRepository;
        this.tipdocexpSearchRepository = tipdocexpSearchRepository;
    }

    /**
     * POST  /tipdocexps : Create a new tipdocexp.
     *
     * @param tipdocexp the tipdocexp to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipdocexp, or with status 400 (Bad Request) if the tipdocexp has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipdocexps")
    @Timed
    public ResponseEntity<Tipdocexp> createTipdocexp(@Valid @RequestBody Tipdocexp tipdocexp) throws URISyntaxException {
        log.debug("REST request to save Tipdocexp : {}", tipdocexp);
        if (tipdocexp.getId() != null) {
            throw new BadRequestAlertException("A new tipdocexp cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tipdocexp result = tipdocexpRepository.save(tipdocexp);
        tipdocexpSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tipdocexps/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipdocexps : Updates an existing tipdocexp.
     *
     * @param tipdocexp the tipdocexp to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipdocexp,
     * or with status 400 (Bad Request) if the tipdocexp is not valid,
     * or with status 500 (Internal Server Error) if the tipdocexp couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipdocexps")
    @Timed
    public ResponseEntity<Tipdocexp> updateTipdocexp(@Valid @RequestBody Tipdocexp tipdocexp) throws URISyntaxException {
        log.debug("REST request to update Tipdocexp : {}", tipdocexp);
        if (tipdocexp.getId() == null) {
            return createTipdocexp(tipdocexp);
        }
        Tipdocexp result = tipdocexpRepository.save(tipdocexp);
        tipdocexpSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipdocexp.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipdocexps : get all the tipdocexps.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipdocexps in body
     */
    @GetMapping("/tipdocexps")
    @Timed
    public List<Tipdocexp> getAllTipdocexps() {
        log.debug("REST request to get all Tipdocexps Activos");
        return tipdocexpRepository.ListarTipdocExpActivos();
        }

    /**
     * GET  /tipdocexps/:id : get the "id" tipdocexp.
     *
     * @param id the id of the tipdocexp to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipdocexp, or with status 404 (Not Found)
     */
    @GetMapping("/tipdocexps/{id}")
    @Timed
    public ResponseEntity<Tipdocexp> getTipdocexp(@PathVariable Long id) {
        log.debug("REST request to get Tipdocexp : {}", id);
        Tipdocexp tipdocexp = tipdocexpRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipdocexp));
    }

    /**
     * DELETE  /tipdocexps/:id : delete the "id" tipdocexp.
     *
     * @param id the id of the tipdocexp to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipdocexps/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipdocexp(@PathVariable Long id) {
        log.debug("REST request to delete Tipdocexp : {}", id);
        tipdocexpRepository.delete(id);
        tipdocexpSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tipdocexps?query=:query : search for the tipdocexp corresponding
     * to the query.
     *
     * @param query the query of the tipdocexp search
     * @return the result of the search
     */
    @GetMapping("/_search/tipdocexps")
    @Timed
    public List<Tipdocexp> searchTipdocexps(@RequestParam String query) {
        log.debug("REST request to search Tipdocexps for query {}", query);
        return StreamSupport
            .stream(tipdocexpSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
