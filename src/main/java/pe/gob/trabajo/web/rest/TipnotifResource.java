package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Tipnotif;

import pe.gob.trabajo.repository.TipnotifRepository;
import pe.gob.trabajo.repository.search.TipnotifSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Tipnotif.
 */
@RestController
@RequestMapping("/api")
public class TipnotifResource {

    private final Logger log = LoggerFactory.getLogger(TipnotifResource.class);

    private static final String ENTITY_NAME = "tipnotif";

    private final TipnotifRepository tipnotifRepository;

    private final TipnotifSearchRepository tipnotifSearchRepository;

    public TipnotifResource(TipnotifRepository tipnotifRepository, TipnotifSearchRepository tipnotifSearchRepository) {
        this.tipnotifRepository = tipnotifRepository;
        this.tipnotifSearchRepository = tipnotifSearchRepository;
    }

    /**
     * POST  /tipnotifs : Create a new tipnotif.
     *
     * @param tipnotif the tipnotif to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipnotif, or with status 400 (Bad Request) if the tipnotif has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipnotifs")
    @Timed
    public ResponseEntity<Tipnotif> createTipnotif(@Valid @RequestBody Tipnotif tipnotif) throws URISyntaxException {
        log.debug("REST request to save Tipnotif : {}", tipnotif);
        if (tipnotif.getId() != null) {
            throw new BadRequestAlertException("A new tipnotif cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tipnotif result = tipnotifRepository.save(tipnotif);
        tipnotifSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tipnotifs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipnotifs : Updates an existing tipnotif.
     *
     * @param tipnotif the tipnotif to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipnotif,
     * or with status 400 (Bad Request) if the tipnotif is not valid,
     * or with status 500 (Internal Server Error) if the tipnotif couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipnotifs")
    @Timed
    public ResponseEntity<Tipnotif> updateTipnotif(@Valid @RequestBody Tipnotif tipnotif) throws URISyntaxException {
        log.debug("REST request to update Tipnotif : {}", tipnotif);
        if (tipnotif.getId() == null) {
            return createTipnotif(tipnotif);
        }
        Tipnotif result = tipnotifRepository.save(tipnotif);
        tipnotifSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipnotif.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipnotifs : get all the tipnotifs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipnotifs in body
     */
    @GetMapping("/tipnotifs")
    @Timed
    public List<Tipnotif> getAllTipnotifs() {
        log.debug("REST request to get all Tipnotifs");
        return tipnotifRepository.findAll();
        }

    /**
     * GET  /tipnotifs/:id : get the "id" tipnotif.
     *
     * @param id the id of the tipnotif to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipnotif, or with status 404 (Not Found)
     */
    @GetMapping("/tipnotifs/{id}")
    @Timed
    public ResponseEntity<Tipnotif> getTipnotif(@PathVariable Long id) {
        log.debug("REST request to get Tipnotif : {}", id);
        Tipnotif tipnotif = tipnotifRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipnotif));
    }

    /**
     * DELETE  /tipnotifs/:id : delete the "id" tipnotif.
     *
     * @param id the id of the tipnotif to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipnotifs/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipnotif(@PathVariable Long id) {
        log.debug("REST request to delete Tipnotif : {}", id);
        tipnotifRepository.delete(id);
        tipnotifSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tipnotifs?query=:query : search for the tipnotif corresponding
     * to the query.
     *
     * @param query the query of the tipnotif search
     * @return the result of the search
     */
    @GetMapping("/_search/tipnotifs")
    @Timed
    public List<Tipnotif> searchTipnotifs(@RequestParam String query) {
        log.debug("REST request to search Tipnotifs for query {}", query);
        return StreamSupport
            .stream(tipnotifSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
