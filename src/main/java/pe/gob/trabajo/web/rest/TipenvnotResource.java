package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Tipenvnot;

import pe.gob.trabajo.repository.TipenvnotRepository;
import pe.gob.trabajo.repository.search.TipenvnotSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Tipenvnot.
 */
@RestController
@RequestMapping("/api")
public class TipenvnotResource {

    private final Logger log = LoggerFactory.getLogger(TipenvnotResource.class);

    private static final String ENTITY_NAME = "tipenvnot";

    private final TipenvnotRepository tipenvnotRepository;

    private final TipenvnotSearchRepository tipenvnotSearchRepository;

    public TipenvnotResource(TipenvnotRepository tipenvnotRepository, TipenvnotSearchRepository tipenvnotSearchRepository) {
        this.tipenvnotRepository = tipenvnotRepository;
        this.tipenvnotSearchRepository = tipenvnotSearchRepository;
    }

    /**
     * POST  /tipenvnots : Create a new tipenvnot.
     *
     * @param tipenvnot the tipenvnot to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipenvnot, or with status 400 (Bad Request) if the tipenvnot has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipenvnots")
    @Timed
    public ResponseEntity<Tipenvnot> createTipenvnot(@Valid @RequestBody Tipenvnot tipenvnot) throws URISyntaxException {
        log.debug("REST request to save Tipenvnot : {}", tipenvnot);
        if (tipenvnot.getId() != null) {
            throw new BadRequestAlertException("A new tipenvnot cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tipenvnot result = tipenvnotRepository.save(tipenvnot);
        tipenvnotSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tipenvnots/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipenvnots : Updates an existing tipenvnot.
     *
     * @param tipenvnot the tipenvnot to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipenvnot,
     * or with status 400 (Bad Request) if the tipenvnot is not valid,
     * or with status 500 (Internal Server Error) if the tipenvnot couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipenvnots")
    @Timed
    public ResponseEntity<Tipenvnot> updateTipenvnot(@Valid @RequestBody Tipenvnot tipenvnot) throws URISyntaxException {
        log.debug("REST request to update Tipenvnot : {}", tipenvnot);
        if (tipenvnot.getId() == null) {
            return createTipenvnot(tipenvnot);
        }
        Tipenvnot result = tipenvnotRepository.save(tipenvnot);
        tipenvnotSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipenvnot.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipenvnots : get all the tipenvnots.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipenvnots in body
     */
    @GetMapping("/tipenvnots")
    @Timed
    public List<Tipenvnot> getAllTipenvnots() {
        log.debug("REST request to get all Tipenvnots");
        return tipenvnotRepository.findAll();
        }

    /**
     * GET  /tipenvnots/:id : get the "id" tipenvnot.
     *
     * @param id the id of the tipenvnot to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipenvnot, or with status 404 (Not Found)
     */
    @GetMapping("/tipenvnots/{id}")
    @Timed
    public ResponseEntity<Tipenvnot> getTipenvnot(@PathVariable Long id) {
        log.debug("REST request to get Tipenvnot : {}", id);
        Tipenvnot tipenvnot = tipenvnotRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipenvnot));
    }

    /**
     * DELETE  /tipenvnots/:id : delete the "id" tipenvnot.
     *
     * @param id the id of the tipenvnot to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipenvnots/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipenvnot(@PathVariable Long id) {
        log.debug("REST request to delete Tipenvnot : {}", id);
        tipenvnotRepository.delete(id);
        tipenvnotSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tipenvnots?query=:query : search for the tipenvnot corresponding
     * to the query.
     *
     * @param query the query of the tipenvnot search
     * @return the result of the search
     */
    @GetMapping("/_search/tipenvnots")
    @Timed
    public List<Tipenvnot> searchTipenvnots(@RequestParam String query) {
        log.debug("REST request to search Tipenvnots for query {}", query);
        return StreamSupport
            .stream(tipenvnotSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
