package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Resolutor;

import pe.gob.trabajo.repository.ResolutorRepository;
import pe.gob.trabajo.repository.search.ResolutorSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Resolutor.
 */
@RestController
@RequestMapping("/api")
public class ResolutorResource {

    private final Logger log = LoggerFactory.getLogger(ResolutorResource.class);

    private static final String ENTITY_NAME = "resolutor";

    private final ResolutorRepository resolutorRepository;

    private final ResolutorSearchRepository resolutorSearchRepository;

    public ResolutorResource(ResolutorRepository resolutorRepository, ResolutorSearchRepository resolutorSearchRepository) {
        this.resolutorRepository = resolutorRepository;
        this.resolutorSearchRepository = resolutorSearchRepository;
    }

    /**
     * POST  /resolutors : Create a new resolutor.
     *
     * @param resolutor the resolutor to create
     * @return the ResponseEntity with status 201 (Created) and with body the new resolutor, or with status 400 (Bad Request) if the resolutor has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/resolutors")
    @Timed
    public ResponseEntity<Resolutor> createResolutor(@Valid @RequestBody Resolutor resolutor) throws URISyntaxException {
        log.debug("REST request to save Resolutor : {}", resolutor);
        if (resolutor.getId() != null) {
            throw new BadRequestAlertException("A new resolutor cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Resolutor result = resolutorRepository.save(resolutor);
        resolutorSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/resolutors/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /resolutors : Updates an existing resolutor.
     *
     * @param resolutor the resolutor to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated resolutor,
     * or with status 400 (Bad Request) if the resolutor is not valid,
     * or with status 500 (Internal Server Error) if the resolutor couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/resolutors")
    @Timed
    public ResponseEntity<Resolutor> updateResolutor(@Valid @RequestBody Resolutor resolutor) throws URISyntaxException {
        log.debug("REST request to update Resolutor : {}", resolutor);
        if (resolutor.getId() == null) {
            return createResolutor(resolutor);
        }
        Resolutor result = resolutorRepository.save(resolutor);
        resolutorSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, resolutor.getId().toString()))
            .body(result);
    }

    /**
     * GET  /resolutors : get all the resolutors.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of resolutors in body
     */
    @GetMapping("/resolutors")
    @Timed
    public List<Resolutor> getAllResolutors() {
        log.debug("REST request to get all Resolutors");
        return resolutorRepository.findAll();
        }

    /**
     * GET  /resolutors/:id : get the "id" resolutor.
     *
     * @param id the id of the resolutor to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the resolutor, or with status 404 (Not Found)
     */
    @GetMapping("/resolutors/{id}")
    @Timed
    public ResponseEntity<Resolutor> getResolutor(@PathVariable Long id) {
        log.debug("REST request to get Resolutor : {}", id);
        Resolutor resolutor = resolutorRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(resolutor));
    }

    /**
     * DELETE  /resolutors/:id : delete the "id" resolutor.
     *
     * @param id the id of the resolutor to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/resolutors/{id}")
    @Timed
    public ResponseEntity<Void> deleteResolutor(@PathVariable Long id) {
        log.debug("REST request to delete Resolutor : {}", id);
        resolutorRepository.delete(id);
        resolutorSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/resolutors?query=:query : search for the resolutor corresponding
     * to the query.
     *
     * @param query the query of the resolutor search
     * @return the result of the search
     */
    @GetMapping("/_search/resolutors")
    @Timed
    public List<Resolutor> searchResolutors(@RequestParam String query) {
        log.debug("REST request to search Resolutors for query {}", query);
        return StreamSupport
            .stream(resolutorSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
