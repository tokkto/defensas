package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Resulconci;

import pe.gob.trabajo.repository.ResulconciRepository;
import pe.gob.trabajo.repository.search.ResulconciSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Resulconci.
 */
@RestController
@RequestMapping("/api")
public class ResulconciResource {

    private final Logger log = LoggerFactory.getLogger(ResulconciResource.class);

    private static final String ENTITY_NAME = "resulconci";

    private final ResulconciRepository resulconciRepository;

    private final ResulconciSearchRepository resulconciSearchRepository;

    public ResulconciResource(ResulconciRepository resulconciRepository, ResulconciSearchRepository resulconciSearchRepository) {
        this.resulconciRepository = resulconciRepository;
        this.resulconciSearchRepository = resulconciSearchRepository;
    }

    /**
     * POST  /resulconcis : Create a new resulconci.
     *
     * @param resulconci the resulconci to create
     * @return the ResponseEntity with status 201 (Created) and with body the new resulconci, or with status 400 (Bad Request) if the resulconci has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/resulconcis")
    @Timed
    public ResponseEntity<Resulconci> createResulconci(@Valid @RequestBody Resulconci resulconci) throws URISyntaxException {
        log.debug("REST request to save Resulconci : {}", resulconci);
        if (resulconci.getId() != null) {
            throw new BadRequestAlertException("A new resulconci cannot already have an ID", ENTITY_NAME, "idexists");
        }
        resulconci.tFecreg(Instant.now());
        Resulconci result = resulconciRepository.save(resulconci);
        resulconciSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/resulconcis/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /resulconcis : Updates an existing resulconci.
     *
     * @param resulconci the resulconci to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated resulconci,
     * or with status 400 (Bad Request) if the resulconci is not valid,
     * or with status 500 (Internal Server Error) if the resulconci couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/resulconcis")
    @Timed
    public ResponseEntity<Resulconci> updateResulconci(@Valid @RequestBody Resulconci resulconci) throws URISyntaxException {
        log.debug("REST request to update Resulconci : {}", resulconci);
        if (resulconci.getId() == null) {
            return createResulconci(resulconci);
        }
        resulconci.tFecupd(Instant.now());
        Resulconci result = resulconciRepository.save(resulconci);
        resulconciSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, resulconci.getId().toString()))
            .body(result);
    }

    /**
     * GET  /resulconcis : get all the resulconcis.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of resulconcis in body
     */
    @GetMapping("/resulconcis")
    @Timed
    public List<Resulconci> getAllResulconcis() {
        log.debug("REST request to get all Resulconcis");
        return resulconciRepository.findAll();
        }

    /**
     * GET  /resulconcis/:id : get the "id" resulconci.
     *
     * @param id the id of the resulconci to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the resulconci, or with status 404 (Not Found)
     */
    @GetMapping("/resulconcis/{id}")
    @Timed
    public ResponseEntity<Resulconci> getResulconci(@PathVariable Long id) {
        log.debug("REST request to get Resulconci : {}", id);
        Resulconci resulconci = resulconciRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(resulconci));
    }

    /**
     *GET  /resulconcis/tot.
     *
    */
    @GetMapping("/resulconcis/tot")
    @Timed
    public List<Resulconci> getResulConTot() {
        return resulconciRepository.findResConcTot();
    }


    /**
     *GET  /resulconcis/tot/{desc}.
     *
    */
    @GetMapping("/resulconcis/tot/{desc}")
    @Timed
    public List<Resulconci> getResulConDesc(@PathVariable String desc) {
        return resulconciRepository.findResConcDesc(desc.toUpperCase());
    }

    /**
     * DELETE  /resulconcis/:id : delete the "id" resulconci.
     *
     * @param id the id of the resulconci to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/resulconcis/{id}")
    @Timed
    public ResponseEntity<Void> deleteResulconci(@PathVariable Long id) {
        log.debug("REST request to delete Resulconci : {}", id);
        resulconciRepository.delete(id);
        resulconciSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/resulconcis?query=:query : search for the resulconci corresponding
     * to the query.
     *
     * @param query the query of the resulconci search
     * @return the result of the search
     */
    @GetMapping("/_search/resulconcis")
    @Timed
    public List<Resulconci> searchResulconcis(@RequestParam String query) {
        log.debug("REST request to search Resulconcis for query {}", query);
        return StreamSupport
            .stream(resulconciSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
