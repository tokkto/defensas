package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Pasegl;

import pe.gob.trabajo.repository.PaseglRepository;
import pe.gob.trabajo.repository.search.PaseglSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Pasegl.
 */
@RestController
@RequestMapping("/api")
public class PaseglResource {

    private final Logger log = LoggerFactory.getLogger(PaseglResource.class);

    private static final String ENTITY_NAME = "pasegl";

    private final PaseglRepository paseglRepository;

    private final PaseglSearchRepository paseglSearchRepository;

    public PaseglResource(PaseglRepository paseglRepository, PaseglSearchRepository paseglSearchRepository) {
        this.paseglRepository = paseglRepository;
        this.paseglSearchRepository = paseglSearchRepository;
    }

    /**
     * POST  /pasegls : Create a new pasegl.
     *
     * @param pasegl the pasegl to create
     * @return the ResponseEntity with status 201 (Created) and with body the new pasegl, or with status 400 (Bad Request) if the pasegl has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/pasegls")
    @Timed
    public ResponseEntity<Pasegl> createPasegl(@Valid @RequestBody Pasegl pasegl) throws URISyntaxException {
        log.debug("REST request to save Pasegl : {}", pasegl);
        if (pasegl.getId() != null) {
            throw new BadRequestAlertException("A new pasegl cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Pasegl result = paseglRepository.save(pasegl);
        paseglSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/pasegls/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /pasegls : Updates an existing pasegl.
     *
     * @param pasegl the pasegl to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated pasegl,
     * or with status 400 (Bad Request) if the pasegl is not valid,
     * or with status 500 (Internal Server Error) if the pasegl couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/pasegls")
    @Timed
    public ResponseEntity<Pasegl> updatePasegl(@Valid @RequestBody Pasegl pasegl) throws URISyntaxException {
        log.debug("REST request to update Pasegl : {}", pasegl);
        if (pasegl.getId() == null) {
            return createPasegl(pasegl);
        }
        Pasegl result = paseglRepository.save(pasegl);
        paseglSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, pasegl.getId().toString()))
            .body(result);
    }

    /**
     * GET  /pasegls : get all the pasegls.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of pasegls in body
     */
    @GetMapping("/pasegls")
    @Timed
    public List<Pasegl> getAllPasegls() {
        log.debug("REST request to get all Pasegls");
        return paseglRepository.findAll();
        }

    /**
     * GET  /pasegls/:id : get the "id" pasegl.
     *
     * @param id the id of the pasegl to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the pasegl, or with status 404 (Not Found)
     */
    @GetMapping("/pasegls/{id}")
    @Timed
    public ResponseEntity<Pasegl> getPasegl(@PathVariable Long id) {
        log.debug("REST request to get Pasegl : {}", id);
        Pasegl pasegl = paseglRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(pasegl));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/pase/param")
    @Timed
    public List<Pasegl> getPaseParam(@RequestParam(value = "tip_doc") Optional<Long> tip_doc,
                                     @RequestParam(value = "nro_doc") Optional<String> nro_doc,
                                     @RequestParam(value = "fec_ini") Optional<String> fec_ini,
                                     @RequestParam(value = "fec_fin") Optional<String> fec_fin) {

        if(fec_ini.isPresent() && fec_fin.isPresent()) {
            //return paseglRepository.findPaseFechaParam(instant1, instant2); 
            return paseglRepository.findPaseFechaParam(fec_ini.get(), fec_fin.get()); 
        }else{
            return paseglRepository.findPaseDocumentoParam(tip_doc.get(), nro_doc.get());         
        }
       
    }

    @GetMapping("/pase/id/{id}")
    @Timed
    public Pasegl getPaseId(@PathVariable Long id) {
        log.debug("REST request to get Pasegl : {}", id);
        return paseglRepository.findPaseId(id);
    }


    /**
     * DELETE  /pasegls/:id : delete the "id" pasegl.
     *
     * @param id the id of the pasegl to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/pasegls/{id}")
    @Timed
    public ResponseEntity<Void> deletePasegl(@PathVariable Long id) {
        log.debug("REST request to delete Pasegl : {}", id);
        paseglRepository.delete(id);
        paseglSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/pasegls?query=:query : search for the pasegl corresponding
     * to the query.
     *
     * @param query the query of the pasegl search
     * @return the result of the search
     */
    @GetMapping("/_search/pasegls")
    @Timed
    public List<Pasegl> searchPasegls(@RequestParam String query) {
        log.debug("REST request to search Pasegls for query {}", query);
        return StreamSupport
            .stream(paseglSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
