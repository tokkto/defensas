package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Concilia;
import pe.gob.trabajo.domain.Cancohorfec;

import pe.gob.trabajo.repository.ConciliaRepository;
import pe.gob.trabajo.repository.CancohorfecRepository;
import pe.gob.trabajo.repository.search.ConciliaSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Concilia.
 */
@RestController
@RequestMapping("/api")
public class ConciliaResource {

    private final Logger log = LoggerFactory.getLogger(ConciliaResource.class);

    private static final String ENTITY_NAME = "concilia";

    private final ConciliaRepository conciliaRepository;

    private final ConciliaSearchRepository conciliaSearchRepository;

    private final CancohorfecRepository cancohorfecRepository;

    public ConciliaResource(ConciliaRepository conciliaRepository, ConciliaSearchRepository conciliaSearchRepository, CancohorfecRepository cancohorfecRepository) {
        this.conciliaRepository = conciliaRepository;
        this.conciliaSearchRepository = conciliaSearchRepository;
        this.cancohorfecRepository = cancohorfecRepository;
    }

    /**
     * POST  /concilias : Create a new concilia.
     *
     * @param concilia the concilia to create
     * @return the ResponseEntity with status 201 (Created) and with body the new concilia, or with status 400 (Bad Request) if the concilia has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/concilias")
    @Timed
    public ResponseEntity<Concilia> createConcilia(@Valid @RequestBody Concilia concilia) throws URISyntaxException {
        log.debug("REST request to save Concilia : {}", concilia);
        
        DateTimeFormatter formatters = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        String fechacociliacion = concilia.getdFecconci().format(formatters);
        Integer validar =  conciliaRepository.ValidarCantConciPorHora(fechacociliacion, concilia.getHoracon().getId());

        if(validar == 0){
            if (concilia.getId() != null) {
                throw new BadRequestAlertException("A new concilia cannot already have an ID", ENTITY_NAME, "idexists");
            }
            
            Integer cantidadPeriodo = conciliaRepository.CantConciPorHoraPeriodo(fechacociliacion);
            Integer cantidadhorasfecha = (conciliaRepository.CantConciPorHora(fechacociliacion, concilia.getHoracon().getId()) == null) ? 0 : conciliaRepository.CantConciPorHora(fechacociliacion, concilia.getHoracon().getId());
            
            if(cantidadhorasfecha < cantidadPeriodo){
                Cancohorfec cancohorfec = new Cancohorfec();
                cancohorfec.setdFecconci(concilia.getdFecconci());
                cancohorfec.setHoracon(concilia.getHoracon());
                cancohorfec.setnFlgactivo(true);

                if(cantidadhorasfecha == 0) {
                    cancohorfec.setnUsuareg(1);
                    cancohorfec.settFecreg(Instant.now());
                    cancohorfec.setnSedereg(1);
                    cancohorfec.setnCantcon(1);
                    cancohorfecRepository.save(cancohorfec);
                } else {
                    cancohorfec = conciliaRepository.CantConciPorHoraObjeto(fechacociliacion, concilia.getHoracon().getId());
                    cancohorfec.setnUsuaupd(1);
                    cancohorfec.setnCantcon(cantidadhorasfecha + 1);
                    cancohorfec.settFecupd(Instant.now());
                    cancohorfecRepository.save(cancohorfec);
                }
            }
            
            concilia.settFecreg(Instant.now());
            concilia.settFecupd(null);
            Concilia result = conciliaRepository.save(concilia);
            conciliaSearchRepository.save(result);
            return ResponseEntity.created(new URI("/api/concilias/" + result.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
                .body(result);
        }else{
            throw new BadRequestAlertException("Supero el número de conciliaciones para la hora seleccionada", ENTITY_NAME, "idexists");
        }
        
    }

    /**
     * PUT  /concilias : Updates an existing concilia.
     *
     * @param concilia the concilia to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated concilia,
     * or with status 400 (Bad Request) if the concilia is not valid,
     * or with status 500 (Internal Server Error) if the concilia couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/concilias")
    @Timed
    public ResponseEntity<Concilia> updateConcilia(@Valid @RequestBody Concilia concilia) throws URISyntaxException {
        log.debug("REST request to update Concilia : {}", concilia);
        if (concilia.getId() == null) {
            return createConcilia(concilia);
        }
        concilia.tFecupd(Instant.now());
        Concilia result = conciliaRepository.save(concilia);
        conciliaSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, concilia.getId().toString()))
            .body(result);
    }

    /**
     * GET  /concilias : get all the concilias.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of concilias in body
     */
    @GetMapping("/concilias")
    @Timed
    public List<Concilia> getAllConcilias() {
        log.debug("REST request to get all Concilias");
        return conciliaRepository.findAll();
        }

    /**
     * GET  /concilias/:id : get the "id" concilia.
     *
     * @param id the id of the concilia to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the concilia, or with status 404 (Not Found)
     */
    @GetMapping("/concilias/{id}")
    @Timed
    public ResponseEntity<Concilia> getConcilia(@PathVariable Long id) {
        log.debug("REST request to get Concilia : {}", id);
        Concilia concilia = conciliaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(concilia));
    }

    @GetMapping("/concilias/exp/{id}")
    @Timed
    public List<Concilia> getAudienciaConciExpe(@PathVariable Long id) {
        log.debug("REST request to get Pasegl : {}", id);
        return conciliaRepository.ListarAudienciasConciliacionExp(id);
    }
    

    //Listar Audiencias de Conciliacion sin Abogado y por fecha
    @GetMapping("/concilias/fecha/{fecha}")
    @Timed
    public List<Concilia> LiataAudConcSinAbodFecha(@PathVariable String fecha) {
        log.debug("REST request to get Pasegl : {}", fecha);
        return conciliaRepository.ListarAudienciasConcSinAbog(fecha);
    }
    //0 : Ingresa Correctamente los datos
    //1 : Envia mensaje de error 
    @GetMapping("/concilias/validacion/{fecha}/{id_hora}")
    @Timed
    public Integer ValidarCantConciPorHora(@PathVariable String fecha, @PathVariable Long id_hora ) {
        log.debug("REST request to get Pasegl : {}", fecha);
        return conciliaRepository.ValidarCantConciPorHora(fecha, id_hora);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/concilias/param")
    @Timed
    public List<Concilia> getConciliaParam(@RequestParam(value = "nro_exp") Optional<String> nro_exp,
                                     @RequestParam(value = "fec_ini") Optional<String> fec_ini,
                                     @RequestParam(value = "fec_fin") Optional<String> fec_fin,
                                     @RequestParam(value = "id_abog") Optional<Long> id_abog) {

        if(fec_ini.isPresent() && fec_fin.isPresent()) {
            return conciliaRepository.findConciliaFechaParam(fec_ini.get(), fec_fin.get()); 
        }else if(id_abog.isPresent()) {
            return conciliaRepository.findConciliaAbogadoParam(id_abog.get()); 
        }else{
            return conciliaRepository.findConciliaNunExpParam(nro_exp.get());         
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/concilias/expediente/param")
    @Timed
    public List<Concilia> getConciliaExpedienteParam(@RequestParam(value = "nro_exp") Optional<String> nro_exp,
                                     @RequestParam(value = "fec_ini") Optional<String> fec_ini,
                                     @RequestParam(value = "fec_fin") Optional<String> fec_fin) {

        if(fec_ini.isPresent() && fec_fin.isPresent()) {
            return conciliaRepository.findConciliaFechaMaxFecParam(fec_ini.get(), fec_fin.get()); 
        }else{
            return conciliaRepository.findConciliaNunExpMaxFecParam(nro_exp.get());         
        }
    }

    @RequestMapping(method = RequestMethod.GET, value = "/concilias/expediente/paramultar/param")
    @Timed
    public List<Concilia> getConciliaExpedienteParaMultarParam(@RequestParam(value = "nro_exp") Optional<String> nro_exp,
                                     @RequestParam(value = "fec_ini") Optional<String> fec_ini,
                                     @RequestParam(value = "fec_fin") Optional<String> fec_fin) {

        if(fec_ini.isPresent() && fec_fin.isPresent()) {
            return conciliaRepository.findConciliaParaMultarFechaParam(fec_ini.get(), fec_fin.get()); 
        }else{
            return conciliaRepository.findConciliaParaMultarNroExpParam(nro_exp.get());         
        }
    }



    @GetMapping("/concilias/fecha")
    @Timed
    public List<Concilia> ListaAudConcSinAbodFecha() {
        log.debug("REST request solo fecha");
        return conciliaRepository.ListarAudienciasConcSinAbog();
    }

    @GetMapping("/concilias/resul")
    @Timed
    public List<Concilia> ListaAudConcSinResultado() {
        log.debug("REST request solo fecha");
        return conciliaRepository.ListarAudienciasConcSinResul();
    }

    /**
     * DELETE  /concilias/:id : delete the "id" concilia.
     *
     * @param id the id of the concilia to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/concilias/{id}")
    @Timed
    public ResponseEntity<Void> deleteConcilia(@PathVariable Long id) {
        log.debug("REST request to delete Concilia : {}", id);
        conciliaRepository.delete(id);
        conciliaSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/concilias?query=:query : search for the concilia corresponding
     * to the query.
     *
     * @param query the query of the concilia search
     * @return the result of the search
     */
    @GetMapping("/_search/concilias")
    @Timed
    public List<Concilia> searchConcilias(@RequestParam String query) {
        log.debug("REST request to search Concilias for query {}", query);
        return StreamSupport
            .stream(conciliaSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
