package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Tipresconc;

import pe.gob.trabajo.repository.TipresconcRepository;
import pe.gob.trabajo.repository.search.TipresconcSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Tipresconc.
 */
@RestController
@RequestMapping("/api")
public class TipresconcResource {

    private final Logger log = LoggerFactory.getLogger(TipresconcResource.class);

    private static final String ENTITY_NAME = "tipresconc";

    private final TipresconcRepository tipresconcRepository;

    private final TipresconcSearchRepository tipresconcSearchRepository;

    public TipresconcResource(TipresconcRepository tipresconcRepository, TipresconcSearchRepository tipresconcSearchRepository) {
        this.tipresconcRepository = tipresconcRepository;
        this.tipresconcSearchRepository = tipresconcSearchRepository;
    }

    /**
     * POST  /tipresconcs : Create a new tipresconc.
     *
     * @param tipresconc the tipresconc to create
     * @return the ResponseEntity with status 201 (Created) and with body the new tipresconc, or with status 400 (Bad Request) if the tipresconc has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/tipresconcs")
    @Timed
    public ResponseEntity<Tipresconc> createTipresconc(@Valid @RequestBody Tipresconc tipresconc) throws URISyntaxException {
        log.debug("REST request to save Tipresconc : {}", tipresconc);
        if (tipresconc.getId() != null) {
            throw new BadRequestAlertException("A new tipresconc cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Tipresconc result = tipresconcRepository.save(tipresconc);
        tipresconcSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/tipresconcs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /tipresconcs : Updates an existing tipresconc.
     *
     * @param tipresconc the tipresconc to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated tipresconc,
     * or with status 400 (Bad Request) if the tipresconc is not valid,
     * or with status 500 (Internal Server Error) if the tipresconc couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/tipresconcs")
    @Timed
    public ResponseEntity<Tipresconc> updateTipresconc(@Valid @RequestBody Tipresconc tipresconc) throws URISyntaxException {
        log.debug("REST request to update Tipresconc : {}", tipresconc);
        if (tipresconc.getId() == null) {
            return createTipresconc(tipresconc);
        }
        Tipresconc result = tipresconcRepository.save(tipresconc);
        tipresconcSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, tipresconc.getId().toString()))
            .body(result);
    }

    /**
     * GET  /tipresconcs : get all the tipresconcs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of tipresconcs in body
     */
    @GetMapping("/tipresconcs")
    @Timed
    public List<Tipresconc> getAllTipresconcs() {
        log.debug("REST request to get all Tipresconcs");
        return tipresconcRepository.findAll();
        }

    /**
     * GET  /tipresconcs/:id : get the "id" tipresconc.
     *
     * @param id the id of the tipresconc to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the tipresconc, or with status 404 (Not Found)
     */
    @GetMapping("/tipresconcs/{id}")
    @Timed
    public ResponseEntity<Tipresconc> getTipresconc(@PathVariable Long id) {
        log.debug("REST request to get Tipresconc : {}", id);
        Tipresconc tipresconc = tipresconcRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(tipresconc));
    }

    /**
     * DELETE  /tipresconcs/:id : delete the "id" tipresconc.
     *
     * @param id the id of the tipresconc to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/tipresconcs/{id}")
    @Timed
    public ResponseEntity<Void> deleteTipresconc(@PathVariable Long id) {
        log.debug("REST request to delete Tipresconc : {}", id);
        tipresconcRepository.delete(id);
        tipresconcSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/tipresconcs?query=:query : search for the tipresconc corresponding
     * to the query.
     *
     * @param query the query of the tipresconc search
     * @return the result of the search
     */
    @GetMapping("/_search/tipresconcs")
    @Timed
    public List<Tipresconc> searchTipresconcs(@RequestParam String query) {
        log.debug("REST request to search Tipresconcs for query {}", query);
        return StreamSupport
            .stream(tipresconcSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
