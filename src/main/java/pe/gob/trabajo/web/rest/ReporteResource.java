package pe.gob.trabajo.web.rest;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * REST controller for managing Oficina.
 */
@RestController
@RequestMapping("/api")
public class ReporteResource {

    private final Logger log = LoggerFactory.getLogger(ReporteResource.class);
    
    @Autowired
    DataSource dataSource;

    //private static final String ENTITY_NAME = "oficina";

    //private final OficinaRepository oficinaRepository;

    //private final OficinaSearchRepository oficinaSearchRepository;

    public ReporteResource() {
    }

    @GetMapping("/reporte/{nombre}/{parametro}")
   @ResponseBody
   public void getReport(HttpServletResponse response, @PathVariable String nombre, @PathVariable String parametro)
           throws JRException, IOException, SQLException {

       InputStream jasperStream = this.getClass().getResourceAsStream("/reportes/" + nombre + ".jasper");
       Map<String, Object> params = new HashMap<>();
       System.out.println("************");
       for (String var : parametro.split("\\^")) {
           System.out.println(var);
           if (var.split("\\|").length > 0) {
               System.out.println(var.split("\\|")[0]);
               System.out.println(var.split("\\|")[1]);
               params.put(var.split("\\|")[0], var.split("\\|")[1]);
           }
       }

       JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
       JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource.getConnection());

       response.setContentType("application/x-pdf");
       response.setHeader("Content-disposition", "inline; filename=helloWorldReport.pdf");

       final OutputStream outStream = response.getOutputStream();
       JasperExportManager.exportReportToPdfStream(jasperPrint, outStream);
   }


}
