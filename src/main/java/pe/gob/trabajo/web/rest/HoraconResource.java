package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Horacon;

import pe.gob.trabajo.repository.HoraconRepository;
import pe.gob.trabajo.repository.search.HoraconSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Horacon.
 */
@RestController
@RequestMapping("/api")
public class HoraconResource {

    private final Logger log = LoggerFactory.getLogger(HoraconResource.class);

    private static final String ENTITY_NAME = "horacon";

    private final HoraconRepository horaconRepository;

    private final HoraconSearchRepository horaconSearchRepository;

    public HoraconResource(HoraconRepository horaconRepository, HoraconSearchRepository horaconSearchRepository) {
        this.horaconRepository = horaconRepository;
        this.horaconSearchRepository = horaconSearchRepository;
    }

    /**
     * POST  /horacons : Create a new horacon.
     *
     * @param horacon the horacon to create
     * @return the ResponseEntity with status 201 (Created) and with body the new horacon, or with status 400 (Bad Request) if the horacon has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/horacons")
    @Timed
    public ResponseEntity<Horacon> createHoracon(@Valid @RequestBody Horacon horacon) throws URISyntaxException {
        log.debug("REST request to save Horacon : {}", horacon);
        if (horacon.getId() != null) {
            throw new BadRequestAlertException("A new horacon cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Horacon result = horaconRepository.save(horacon);
        horaconSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/horacons/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /horacons : Updates an existing horacon.
     *
     * @param horacon the horacon to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated horacon,
     * or with status 400 (Bad Request) if the horacon is not valid,
     * or with status 500 (Internal Server Error) if the horacon couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/horacons")
    @Timed
    public ResponseEntity<Horacon> updateHoracon(@Valid @RequestBody Horacon horacon) throws URISyntaxException {
        log.debug("REST request to update Horacon : {}", horacon);
        if (horacon.getId() == null) {
            return createHoracon(horacon);
        }
        Horacon result = horaconRepository.save(horacon);
        horaconSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, horacon.getId().toString()))
            .body(result);
    }

    /**
     * GET  /horacons : get all the horacons.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of horacons in body
     */
    @GetMapping("/horacons")
    @Timed
    public List<Horacon> getAllHoracons() {
        log.debug("REST request to get all Horacons");
        return horaconRepository.findAll();
        }

    @GetMapping("/horacons/activos")
    @Timed
    public List<Horacon> getAllHoraconsAct() {
        return horaconRepository.ListarHorasActivas();
    }

    @GetMapping("/horacons/fecha/{fecha}")
    @Timed
    public List<Horacon> getAllHoraconsFecha(@PathVariable String fecha) {
        log.debug("REST request to get Horacon : {}", fecha);
        return horaconRepository.ListarHorasActivasFecha(fecha);
    }

    /**
     * GET  /horacons/:id : get the "id" horacon.
     *
     * @param id the id of the horacon to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the horacon, or with status 404 (Not Found)
     */
    @GetMapping("/horacons/{id}")
    @Timed
    public ResponseEntity<Horacon> getHoracon(@PathVariable Long id) {
        log.debug("REST request to get Horacon : {}", id);
        Horacon horacon = horaconRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(horacon));
    }

    /**
     * DELETE  /horacons/:id : delete the "id" horacon.
     *
     * @param id the id of the horacon to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/horacons/{id}")
    @Timed
    public ResponseEntity<Void> deleteHoracon(@PathVariable Long id) {
        log.debug("REST request to delete Horacon : {}", id);
        horaconRepository.delete(id);
        horaconSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/horacons?query=:query : search for the horacon corresponding
     * to the query.
     *
     * @param query the query of the horacon search
     * @return the result of the search
     */
    @GetMapping("/_search/horacons")
    @Timed
    public List<Horacon> searchHoracons(@RequestParam String query) {
        log.debug("REST request to search Horacons for query {}", query);
        return StreamSupport
            .stream(horaconSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
