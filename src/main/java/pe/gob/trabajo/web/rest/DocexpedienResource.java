package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Docexpedien;

import pe.gob.trabajo.repository.DocexpedienRepository;
import pe.gob.trabajo.repository.search.DocexpedienSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Docexpedien.
 */
@RestController
@RequestMapping("/api")
public class DocexpedienResource {

    private final Logger log = LoggerFactory.getLogger(DocexpedienResource.class);

    private static final String ENTITY_NAME = "docexpedien";

    private final DocexpedienRepository docexpedienRepository;

    private final DocexpedienSearchRepository docexpedienSearchRepository;

    public DocexpedienResource(DocexpedienRepository docexpedienRepository, DocexpedienSearchRepository docexpedienSearchRepository) {
        this.docexpedienRepository = docexpedienRepository;
        this.docexpedienSearchRepository = docexpedienSearchRepository;
    }

    /**
     * POST  /docexpediens : Create a new docexpedien.
     *
     * @param docexpedien the docexpedien to create
     * @return the ResponseEntity with status 201 (Created) and with body the new docexpedien, or with status 400 (Bad Request) if the docexpedien has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/docexpediens")
    @Timed
    public ResponseEntity<Docexpedien> createDocexpedien(@Valid @RequestBody Docexpedien docexpedien) throws URISyntaxException {
        log.debug("REST request to save Docexpedien : {}", docexpedien);
        if (docexpedien.getId() != null) {
            throw new BadRequestAlertException("A new docexpedien cannot already have an ID", ENTITY_NAME, "idexists");
        } else{
            docexpedien.settFecreg(Instant.now());
            log.debug("REST La fecha de hoy es : {}", LocalDate.now());
        }
        Docexpedien result = docexpedienRepository.save(docexpedien);
        docexpedienSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/docexpediens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /docexpediens : Updates an existing docexpedien.
     *
     * @param docexpedien the docexpedien to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated docexpedien,
     * or with status 400 (Bad Request) if the docexpedien is not valid,
     * or with status 500 (Internal Server Error) if the docexpedien couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/docexpediens")
    @Timed
    public ResponseEntity<Docexpedien> updateDocexpedien(@Valid @RequestBody Docexpedien docexpedien) throws URISyntaxException {
        log.debug("REST request to update Docexpedien : {}", docexpedien);
        if (docexpedien.getId() == null) {
            return createDocexpedien(docexpedien);
        }
        Docexpedien result = docexpedienRepository.save(docexpedien);
        docexpedienSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, docexpedien.getId().toString()))
            .body(result);
    }

    /**
     * GET  /docexpediens : get all the docexpediens.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of docexpediens in body
     */
    @GetMapping("/docexpediens")
    @Timed
    public List<Docexpedien> getAllDocexpediens() {
        log.debug("REST request to get all Docexpediens");
        return docexpedienRepository.findAll();
        }

    @GetMapping("/docexpediens/exp/{id}")
    @Timed
    public List<Docexpedien> getAllDocexpediensPorExp(@PathVariable Long id) {
        log.debug("REST request to get all Docexpediens");
        return docexpedienRepository.findDocExpId(id);
    }

    /**
     * GET  /docexpediens/:id : get the "id" docexpedien.
     *
     * @param id the id of the docexpedien to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the docexpedien, or with status 404 (Not Found)
     */
    @GetMapping("/docexpediens/{id}")
    @Timed
    public ResponseEntity<Docexpedien> getDocexpedien(@PathVariable Long id) {
        log.debug("REST request to get Docexpedien : {}", id);
        Docexpedien docexpedien = docexpedienRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(docexpedien));
    }

    /**
     * DELETE  /docexpediens/:id : delete the "id" docexpedien.
     *
     * @param id the id of the docexpedien to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/docexpediens/{id}")
    @Timed
    public ResponseEntity<Void> deleteDocexpedien(@PathVariable Long id) {
        log.debug("REST request to delete Docexpedien : {}", id);
        docexpedienRepository.delete(id);
        docexpedienSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/docexpediens?query=:query : search for the docexpedien corresponding
     * to the query.
     *
     * @param query the query of the docexpedien search
     * @return the result of the search
     */
    @GetMapping("/_search/docexpediens")
    @Timed
    public List<Docexpedien> searchDocexpediens(@RequestParam String query) {
        log.debug("REST request to search Docexpediens for query {}", query);
        return StreamSupport
            .stream(docexpedienSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
