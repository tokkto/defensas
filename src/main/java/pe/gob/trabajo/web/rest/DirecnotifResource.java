package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Direcnotif;

import pe.gob.trabajo.repository.DirecnotifRepository;
import pe.gob.trabajo.repository.search.DirecnotifSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Direcnotif.
 */
@RestController
@RequestMapping("/api")
public class DirecnotifResource {

    private final Logger log = LoggerFactory.getLogger(DirecnotifResource.class);

    private static final String ENTITY_NAME = "direcnotif";

    private final DirecnotifRepository direcnotifRepository;

    private final DirecnotifSearchRepository direcnotifSearchRepository;

    public DirecnotifResource(DirecnotifRepository direcnotifRepository, DirecnotifSearchRepository direcnotifSearchRepository) {
        this.direcnotifRepository = direcnotifRepository;
        this.direcnotifSearchRepository = direcnotifSearchRepository;
    }

    /**
     * POST  /direcnotifs : Create a new direcnotif.
     *
     * @param direcnotif the direcnotif to create
     * @return the ResponseEntity with status 201 (Created) and with body the new direcnotif, or with status 400 (Bad Request) if the direcnotif has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/direcnotifs")
    @Timed
    public ResponseEntity<Direcnotif> createDirecnotif(@Valid @RequestBody Direcnotif direcnotif) throws URISyntaxException {
        log.debug("REST request to save Direcnotif : {}", direcnotif);
        if (direcnotif.getId() != null) {
            throw new BadRequestAlertException("A new direcnotif cannot already have an ID", ENTITY_NAME, "idexists");
        }
        direcnotif.tFecreg(Instant.now());
        Direcnotif result = direcnotifRepository.save(direcnotif);
        direcnotifSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/direcnotifs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /direcnotifs : Updates an existing direcnotif.
     *
     * @param direcnotif the direcnotif to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated direcnotif,
     * or with status 400 (Bad Request) if the direcnotif is not valid,
     * or with status 500 (Internal Server Error) if the direcnotif couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/direcnotifs")
    @Timed
    public ResponseEntity<Direcnotif> updateDirecnotif(@Valid @RequestBody Direcnotif direcnotif) throws URISyntaxException {
        log.debug("REST request to update Direcnotif : {}", direcnotif);
        if (direcnotif.getId() == null) {
            return createDirecnotif(direcnotif);
        }
        direcnotif.tFecupd(Instant.now());
        Direcnotif result = direcnotifRepository.save(direcnotif);
        direcnotifSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, direcnotif.getId().toString()))
            .body(result);
    }

    /**
     * GET  /direcnotifs : get all the direcnotifs.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of direcnotifs in body
     */
    @GetMapping("/direcnotifs")
    @Timed
    public List<Direcnotif> getAllDirecnotifs() {
        log.debug("REST request to get all Direcnotifs");
        return direcnotifRepository.findAll();
        }

    /**
     * GET  /direcnotifs/:id : get the "id" direcnotif.
     *
     * @param id the id of the direcnotif to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the direcnotif, or with status 404 (Not Found)
     */
    @GetMapping("/direcnotifs/{id}")
    @Timed
    public ResponseEntity<Direcnotif> getDirecnotif(@PathVariable Long id) {
        log.debug("REST request to get Direcnotif : {}", id);
        Direcnotif direcnotif = direcnotifRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(direcnotif));
    }
    
    @GetMapping("/direcnotifs/notifica/{idnotifica}")
    @Timed
    public List<Direcnotif> getDirecnotifByNotifica(@PathVariable Long idnotifica) {
        log.debug("REST request to get DirecnotifByNotifica : {}", idnotifica);
        return direcnotifRepository.findDirecnotifParam(idnotifica);
    }

    /**
     * DELETE  /direcnotifs/:id : delete the "id" direcnotif.
     *
     * @param id the id of the direcnotif to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/direcnotifs/{id}")
    @Timed
    public ResponseEntity<Void> deleteDirecnotif(@PathVariable Long id) {
        log.debug("REST request to delete Direcnotif : {}", id);
        direcnotifRepository.delete(id);
        direcnotifSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/direcnotifs?query=:query : search for the direcnotif corresponding
     * to the query.
     *
     * @param query the query of the direcnotif search
     * @return the result of the search
     */
    @GetMapping("/_search/direcnotifs")
    @Timed
    public List<Direcnotif> searchDirecnotifs(@RequestParam String query) {
        log.debug("REST request to search Direcnotifs for query {}", query);
        return StreamSupport
            .stream(direcnotifSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
