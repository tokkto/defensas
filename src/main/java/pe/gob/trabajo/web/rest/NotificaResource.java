package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Notifica;

import pe.gob.trabajo.repository.NotificaRepository;
import pe.gob.trabajo.repository.search.NotificaSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Notifica.
 */
@RestController
@RequestMapping("/api")
public class NotificaResource {

    private final Logger log = LoggerFactory.getLogger(NotificaResource.class);

    private static final String ENTITY_NAME = "notifica";

    private final NotificaRepository notificaRepository;

    private final NotificaSearchRepository notificaSearchRepository;

    public NotificaResource(NotificaRepository notificaRepository, NotificaSearchRepository notificaSearchRepository) {
        this.notificaRepository = notificaRepository;
        this.notificaSearchRepository = notificaSearchRepository;
    }

    /**
     * POST  /notificas : Create a new notifica.
     *
     * @param notifica the notifica to create
     * @return the ResponseEntity with status 201 (Created) and with body the new notifica, or with status 400 (Bad Request) if the notifica has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/notificas")
    @Timed
    public ResponseEntity<Notifica> createNotifica(@Valid @RequestBody Notifica notifica) throws URISyntaxException {
        log.debug("REST request to save Notifica : {}", notifica);
        if (notifica.getId() != null) {
            throw new BadRequestAlertException("A new notifica cannot already have an ID", ENTITY_NAME, "idexists");
        }
        notifica.tFecreg(Instant.now());
        Notifica result = notificaRepository.save(notifica);
        notificaSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/notificas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /notificas : Updates an existing notifica.
     *
     * @param notifica the notifica to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated notifica,
     * or with status 400 (Bad Request) if the notifica is not valid,
     * or with status 500 (Internal Server Error) if the notifica couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/notificas")
    @Timed
    public ResponseEntity<Notifica> updateNotifica(@Valid @RequestBody Notifica notifica) throws URISyntaxException {
        log.debug("REST request to update Notifica : {}", notifica);
        if (notifica.getId() == null) {
            return createNotifica(notifica);
        }
        notifica.tFecupd(Instant.now());
        Notifica result = notificaRepository.save(notifica);
        notificaSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, notifica.getId().toString()))
            .body(result);
    }

    /**
     * GET  /notificas : get all the notificas.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of notificas in body
     */
    @GetMapping("/notificas")
    @Timed
    public List<Notifica> getAllNotificas() {
        log.debug("REST request to get all Notificas");
        return notificaRepository.findAll();
        }

    /**
     * GET  /notificas/:id : get the "id" notifica.
     *
     * @param id the id of the notifica to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the notifica, or with status 404 (Not Found)
     */
    @GetMapping("/notificas/{id}")
    @Timed
    public ResponseEntity<Notifica> getNotifica(@PathVariable Long id) {
        log.debug("REST request to get Notifica : {}", id);
        Notifica notifica = notificaRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(notifica));
    }

    /**
     * DELETE  /notificas/:id : delete the "id" notifica.
     *
     * @param id the id of the notifica to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/notificas/{id}")
    @Timed
    public ResponseEntity<Void> deleteNotifica(@PathVariable Long id) {
        log.debug("REST request to delete Notifica : {}", id);
        notificaRepository.delete(id);
        notificaSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/notificas?query=:query : search for the notifica corresponding
     * to the query.
     *
     * @param query the query of the notifica search
     * @return the result of the search
     */
    @GetMapping("/_search/notificas")
    @Timed
    public List<Notifica> searchNotificas(@RequestParam String query) {
        log.debug("REST request to search Notificas for query {}", query);
        return StreamSupport
            .stream(notificaSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
