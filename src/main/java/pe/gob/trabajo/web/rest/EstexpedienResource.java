package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Estexpedien;

import pe.gob.trabajo.repository.EstexpedienRepository;
import pe.gob.trabajo.repository.search.EstexpedienSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Estexpedien.
 */
@RestController
@RequestMapping("/api")
public class EstexpedienResource {

    private final Logger log = LoggerFactory.getLogger(EstexpedienResource.class);

    private static final String ENTITY_NAME = "estexpedien";

    private final EstexpedienRepository estexpedienRepository;

    private final EstexpedienSearchRepository estexpedienSearchRepository;

    public EstexpedienResource(EstexpedienRepository estexpedienRepository, EstexpedienSearchRepository estexpedienSearchRepository) {
        this.estexpedienRepository = estexpedienRepository;
        this.estexpedienSearchRepository = estexpedienSearchRepository;
    }

    /**
     * POST  /estexpediens : Create a new estexpedien.
     *
     * @param estexpedien the estexpedien to create
     * @return the ResponseEntity with status 201 (Created) and with body the new estexpedien, or with status 400 (Bad Request) if the estexpedien has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/estexpediens")
    @Timed
    public ResponseEntity<Estexpedien> createEstexpedien(@Valid @RequestBody Estexpedien estexpedien) throws URISyntaxException {
        log.debug("REST request to save Estexpedien : {}", estexpedien);
        if (estexpedien.getId() != null) {
            throw new BadRequestAlertException("A new estexpedien cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Estexpedien result = estexpedienRepository.save(estexpedien);
        estexpedienSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/estexpediens/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /estexpediens : Updates an existing estexpedien.
     *
     * @param estexpedien the estexpedien to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated estexpedien,
     * or with status 400 (Bad Request) if the estexpedien is not valid,
     * or with status 500 (Internal Server Error) if the estexpedien couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/estexpediens")
    @Timed
    public ResponseEntity<Estexpedien> updateEstexpedien(@Valid @RequestBody Estexpedien estexpedien) throws URISyntaxException {
        log.debug("REST request to update Estexpedien : {}", estexpedien);
        if (estexpedien.getId() == null) {
            return createEstexpedien(estexpedien);
        }
        Estexpedien result = estexpedienRepository.save(estexpedien);
        estexpedienSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, estexpedien.getId().toString()))
            .body(result);
    }

    /**
     * GET  /estexpediens : get all the estexpediens.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of estexpediens in body
     */
    @GetMapping("/estexpediens")
    @Timed
    public List<Estexpedien> getAllEstexpediens() {
        log.debug("REST request to get all Estexpediens");
        return estexpedienRepository.findAll();
        }

    /**
     * GET  /estexpediens/:id : get the "id" estexpedien.
     *
     * @param id the id of the estexpedien to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the estexpedien, or with status 404 (Not Found)
     */
    @GetMapping("/estexpediens/{id}")
    @Timed
    public ResponseEntity<Estexpedien> getEstexpedien(@PathVariable Long id) {
        log.debug("REST request to get Estexpedien : {}", id);
        Estexpedien estexpedien = estexpedienRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(estexpedien));
    }

    /**
     * DELETE  /estexpediens/:id : delete the "id" estexpedien.
     *
     * @param id the id of the estexpedien to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/estexpediens/{id}")
    @Timed
    public ResponseEntity<Void> deleteEstexpedien(@PathVariable Long id) {
        log.debug("REST request to delete Estexpedien : {}", id);
        estexpedienRepository.delete(id);
        estexpedienSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/estexpediens?query=:query : search for the estexpedien corresponding
     * to the query.
     *
     * @param query the query of the estexpedien search
     * @return the result of the search
     */
    @GetMapping("/_search/estexpediens")
    @Timed
    public List<Estexpedien> searchEstexpediens(@RequestParam String query) {
        log.debug("REST request to search Estexpediens for query {}", query);
        return StreamSupport
            .stream(estexpedienSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
