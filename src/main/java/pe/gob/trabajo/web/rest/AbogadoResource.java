package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Abogado;

import pe.gob.trabajo.repository.AbogadoRepository;
import pe.gob.trabajo.repository.search.AbogadoSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Abogado.
 */
@RestController
@RequestMapping("/api")
public class AbogadoResource {

    private final Logger log = LoggerFactory.getLogger(AbogadoResource.class);

    private static final String ENTITY_NAME = "abogado";

    private final AbogadoRepository abogadoRepository;

    private final AbogadoSearchRepository abogadoSearchRepository;

    public AbogadoResource(AbogadoRepository abogadoRepository, AbogadoSearchRepository abogadoSearchRepository) {
        this.abogadoRepository = abogadoRepository;
        this.abogadoSearchRepository = abogadoSearchRepository;
    }

    /**
     * POST  /abogados : Create a new abogado.
     *
     * @param abogado the abogado to create
     * @return the ResponseEntity with status 201 (Created) and with body the new abogado, or with status 400 (Bad Request) if the abogado has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/abogados")
    @Timed
    public ResponseEntity<Abogado> createAbogado(@Valid @RequestBody Abogado abogado) throws URISyntaxException {
        log.debug("REST request to save Abogado : {}", abogado);
        if (abogado.getId() != null) {
            throw new BadRequestAlertException("A new abogado cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Abogado result = abogadoRepository.save(abogado);
        abogadoSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/abogados/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /abogados : Updates an existing abogado.
     *
     * @param abogado the abogado to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated abogado,
     * or with status 400 (Bad Request) if the abogado is not valid,
     * or with status 500 (Internal Server Error) if the abogado couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/abogados")
    @Timed
    public ResponseEntity<Abogado> updateAbogado(@Valid @RequestBody Abogado abogado) throws URISyntaxException {
        log.debug("REST request to update Abogado : {}", abogado);
        if (abogado.getId() == null) {
            return createAbogado(abogado);
        }
        Abogado result = abogadoRepository.save(abogado);
        abogadoSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, abogado.getId().toString()))
            .body(result);
    }

    /**
     * GET  /abogados : get all the abogados.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of abogados in body
     */
    @GetMapping("/abogados")
    @Timed
    public List<Abogado> getAllAbogados() {
        log.debug("REST request to get all Abogados");
        return abogadoRepository.ListarAbogadosActivos();
        }

    /**
     * GET  /abogados/:id : get the "id" abogado.
     *
     * @param id the id of the abogado to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the abogado, or with status 404 (Not Found)
     */
    @GetMapping("/abogados/{id}")
    @Timed
    public ResponseEntity<Abogado> getAbogado(@PathVariable Long id) {
        log.debug("REST request to get Abogado : {}", id);
        Abogado abogado = abogadoRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(abogado));
    }

    /**
     * DELETE  /abogados/:id : delete the "id" abogado.
     *
     * @param id the id of the abogado to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/abogados/{id}")
    @Timed
    public ResponseEntity<Void> deleteAbogado(@PathVariable Long id) {
        log.debug("REST request to delete Abogado : {}", id);
        abogadoRepository.delete(id);
        abogadoSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/abogados?query=:query : search for the abogado corresponding
     * to the query.
     *
     * @param query the query of the abogado search
     * @return the result of the search
     */
    @GetMapping("/_search/abogados")
    @Timed
    public List<Abogado> searchAbogados(@RequestParam String query) {
        log.debug("REST request to search Abogados for query {}", query);
        return StreamSupport
            .stream(abogadoSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
