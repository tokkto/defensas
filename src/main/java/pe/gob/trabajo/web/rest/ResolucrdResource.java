package pe.gob.trabajo.web.rest;

import com.codahale.metrics.annotation.Timed;
import pe.gob.trabajo.domain.Resolucrd;

import pe.gob.trabajo.repository.ResolucrdRepository;
import pe.gob.trabajo.repository.search.ResolucrdSearchRepository;
import pe.gob.trabajo.web.rest.errors.BadRequestAlertException;
import pe.gob.trabajo.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * REST controller for managing Resolucrd.
 */
@RestController
@RequestMapping("/api")
public class ResolucrdResource {

    private final Logger log = LoggerFactory.getLogger(ResolucrdResource.class);

    private static final String ENTITY_NAME = "resolucrd";

    private final ResolucrdRepository resolucrdRepository;

    private final ResolucrdSearchRepository resolucrdSearchRepository;

    public ResolucrdResource(ResolucrdRepository resolucrdRepository, ResolucrdSearchRepository resolucrdSearchRepository) {
        this.resolucrdRepository = resolucrdRepository;
        this.resolucrdSearchRepository = resolucrdSearchRepository;
    }

    /**
     * POST  /resolucrds : Create a new resolucrd.
     *
     * @param resolucrd the resolucrd to create
     * @return the ResponseEntity with status 201 (Created) and with body the new resolucrd, or with status 400 (Bad Request) if the resolucrd has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/resolucrds")
    @Timed
    public ResponseEntity<Resolucrd> createResolucrd(@Valid @RequestBody Resolucrd resolucrd) throws URISyntaxException {
        log.debug("REST request to save Resolucrd : {}", resolucrd);
        if (resolucrd.getId() != null) {
            throw new BadRequestAlertException("A new resolucrd cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Resolucrd result = resolucrdRepository.save(resolucrd);
        resolucrdSearchRepository.save(result);
        return ResponseEntity.created(new URI("/api/resolucrds/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /resolucrds : Updates an existing resolucrd.
     *
     * @param resolucrd the resolucrd to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated resolucrd,
     * or with status 400 (Bad Request) if the resolucrd is not valid,
     * or with status 500 (Internal Server Error) if the resolucrd couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/resolucrds")
    @Timed
    public ResponseEntity<Resolucrd> updateResolucrd(@Valid @RequestBody Resolucrd resolucrd) throws URISyntaxException {
        log.debug("REST request to update Resolucrd : {}", resolucrd);
        if (resolucrd.getId() == null) {
            return createResolucrd(resolucrd);
        }
        Resolucrd result = resolucrdRepository.save(resolucrd);
        resolucrdSearchRepository.save(result);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, resolucrd.getId().toString()))
            .body(result);
    }

    /**
     * GET  /resolucrds : get all the resolucrds.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of resolucrds in body
     */
    @GetMapping("/resolucrds")
    @Timed
    public List<Resolucrd> getAllResolucrds() {
        log.debug("REST request to get all Resolucrds");
        return resolucrdRepository.findAll();
        }

    /**
     * GET  /resolucrds/:id : get the "id" resolucrd.
     *
     * @param id the id of the resolucrd to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the resolucrd, or with status 404 (Not Found)
     */
    @GetMapping("/resolucrds/{id}")
    @Timed
    public ResponseEntity<Resolucrd> getResolucrd(@PathVariable Long id) {
        log.debug("REST request to get Resolucrd : {}", id);
        Resolucrd resolucrd = resolucrdRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(resolucrd));
    }

    /**
     * DELETE  /resolucrds/:id : delete the "id" resolucrd.
     *
     * @param id the id of the resolucrd to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/resolucrds/{id}")
    @Timed
    public ResponseEntity<Void> deleteResolucrd(@PathVariable Long id) {
        log.debug("REST request to delete Resolucrd : {}", id);
        resolucrdRepository.delete(id);
        resolucrdSearchRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

    /**
     * SEARCH  /_search/resolucrds?query=:query : search for the resolucrd corresponding
     * to the query.
     *
     * @param query the query of the resolucrd search
     * @return the result of the search
     */
    @GetMapping("/_search/resolucrds")
    @Timed
    public List<Resolucrd> searchResolucrds(@RequestParam String query) {
        log.debug("REST request to search Resolucrds for query {}", query);
        return StreamSupport
            .stream(resolucrdSearchRepository.search(queryStringQuery(query)).spliterator(), false)
            .collect(Collectors.toList());
    }

}
