package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Direcnotif;
import pe.gob.trabajo.repository.DirecnotifRepository;
import pe.gob.trabajo.repository.search.DirecnotifSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DirecnotifResource REST controller.
 *
 * @see DirecnotifResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class DirecnotifResourceIntTest {

    private static final String DEFAULT_V_DIRECCION = "AAAAAAAAAA";
    private static final String UPDATED_V_DIRECCION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_NFLGTRABAJ = false;
    private static final Boolean UPDATED_NFLGTRABAJ = true;

    private static final String DEFAULT_V_HOJAENVIO = "AAAAAAAAAA";
    private static final String UPDATED_V_HOJAENVIO = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private DirecnotifRepository direcnotifRepository;

    @Autowired
    private DirecnotifSearchRepository direcnotifSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDirecnotifMockMvc;

    private Direcnotif direcnotif;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DirecnotifResource direcnotifResource = new DirecnotifResource(direcnotifRepository, direcnotifSearchRepository);
        this.restDirecnotifMockMvc = MockMvcBuilders.standaloneSetup(direcnotifResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Direcnotif createEntity(EntityManager em) {
        Direcnotif direcnotif = new Direcnotif()
            .vDireccion(DEFAULT_V_DIRECCION)
            .nflgtrabaj(DEFAULT_NFLGTRABAJ)
            .vHojaenvio(DEFAULT_V_HOJAENVIO)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return direcnotif;
    }

    @Before
    public void initTest() {
        direcnotifSearchRepository.deleteAll();
        direcnotif = createEntity(em);
    }

    @Test
    @Transactional
    public void createDirecnotif() throws Exception {
        int databaseSizeBeforeCreate = direcnotifRepository.findAll().size();

        // Create the Direcnotif
        restDirecnotifMockMvc.perform(post("/api/direcnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(direcnotif)))
            .andExpect(status().isCreated());

        // Validate the Direcnotif in the database
        List<Direcnotif> direcnotifList = direcnotifRepository.findAll();
        assertThat(direcnotifList).hasSize(databaseSizeBeforeCreate + 1);
        Direcnotif testDirecnotif = direcnotifList.get(direcnotifList.size() - 1);
        assertThat(testDirecnotif.getvDireccion()).isEqualTo(DEFAULT_V_DIRECCION);
        assertThat(testDirecnotif.isNflgtrabaj()).isEqualTo(DEFAULT_NFLGTRABAJ);
        assertThat(testDirecnotif.getvHojaenvio()).isEqualTo(DEFAULT_V_HOJAENVIO);
        assertThat(testDirecnotif.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testDirecnotif.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testDirecnotif.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testDirecnotif.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testDirecnotif.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testDirecnotif.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testDirecnotif.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Direcnotif in Elasticsearch
        Direcnotif direcnotifEs = direcnotifSearchRepository.findOne(testDirecnotif.getId());
        assertThat(direcnotifEs).isEqualToComparingFieldByField(testDirecnotif);
    }

    @Test
    @Transactional
    public void createDirecnotifWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = direcnotifRepository.findAll().size();

        // Create the Direcnotif with an existing ID
        direcnotif.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDirecnotifMockMvc.perform(post("/api/direcnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(direcnotif)))
            .andExpect(status().isBadRequest());

        // Validate the Direcnotif in the database
        List<Direcnotif> direcnotifList = direcnotifRepository.findAll();
        assertThat(direcnotifList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = direcnotifRepository.findAll().size();
        // set the field null
        direcnotif.setnUsuareg(null);

        // Create the Direcnotif, which fails.

        restDirecnotifMockMvc.perform(post("/api/direcnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(direcnotif)))
            .andExpect(status().isBadRequest());

        List<Direcnotif> direcnotifList = direcnotifRepository.findAll();
        assertThat(direcnotifList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = direcnotifRepository.findAll().size();
        // set the field null
        direcnotif.settFecreg(null);

        // Create the Direcnotif, which fails.

        restDirecnotifMockMvc.perform(post("/api/direcnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(direcnotif)))
            .andExpect(status().isBadRequest());

        List<Direcnotif> direcnotifList = direcnotifRepository.findAll();
        assertThat(direcnotifList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = direcnotifRepository.findAll().size();
        // set the field null
        direcnotif.setnFlgactivo(null);

        // Create the Direcnotif, which fails.

        restDirecnotifMockMvc.perform(post("/api/direcnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(direcnotif)))
            .andExpect(status().isBadRequest());

        List<Direcnotif> direcnotifList = direcnotifRepository.findAll();
        assertThat(direcnotifList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = direcnotifRepository.findAll().size();
        // set the field null
        direcnotif.setnSedereg(null);

        // Create the Direcnotif, which fails.

        restDirecnotifMockMvc.perform(post("/api/direcnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(direcnotif)))
            .andExpect(status().isBadRequest());

        List<Direcnotif> direcnotifList = direcnotifRepository.findAll();
        assertThat(direcnotifList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDirecnotifs() throws Exception {
        // Initialize the database
        direcnotifRepository.saveAndFlush(direcnotif);

        // Get all the direcnotifList
        restDirecnotifMockMvc.perform(get("/api/direcnotifs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(direcnotif.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDireccion").value(hasItem(DEFAULT_V_DIRECCION.toString())))
            .andExpect(jsonPath("$.[*].nflgtrabaj").value(hasItem(DEFAULT_NFLGTRABAJ.booleanValue())))
            .andExpect(jsonPath("$.[*].vHojaenvio").value(hasItem(DEFAULT_V_HOJAENVIO.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getDirecnotif() throws Exception {
        // Initialize the database
        direcnotifRepository.saveAndFlush(direcnotif);

        // Get the direcnotif
        restDirecnotifMockMvc.perform(get("/api/direcnotifs/{id}", direcnotif.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(direcnotif.getId().intValue()))
            .andExpect(jsonPath("$.vDireccion").value(DEFAULT_V_DIRECCION.toString()))
            .andExpect(jsonPath("$.nflgtrabaj").value(DEFAULT_NFLGTRABAJ.booleanValue()))
            .andExpect(jsonPath("$.vHojaenvio").value(DEFAULT_V_HOJAENVIO.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingDirecnotif() throws Exception {
        // Get the direcnotif
        restDirecnotifMockMvc.perform(get("/api/direcnotifs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDirecnotif() throws Exception {
        // Initialize the database
        direcnotifRepository.saveAndFlush(direcnotif);
        direcnotifSearchRepository.save(direcnotif);
        int databaseSizeBeforeUpdate = direcnotifRepository.findAll().size();

        // Update the direcnotif
        Direcnotif updatedDirecnotif = direcnotifRepository.findOne(direcnotif.getId());
        updatedDirecnotif
            .vDireccion(UPDATED_V_DIRECCION)
            .nflgtrabaj(UPDATED_NFLGTRABAJ)
            .vHojaenvio(UPDATED_V_HOJAENVIO)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restDirecnotifMockMvc.perform(put("/api/direcnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDirecnotif)))
            .andExpect(status().isOk());

        // Validate the Direcnotif in the database
        List<Direcnotif> direcnotifList = direcnotifRepository.findAll();
        assertThat(direcnotifList).hasSize(databaseSizeBeforeUpdate);
        Direcnotif testDirecnotif = direcnotifList.get(direcnotifList.size() - 1);
        assertThat(testDirecnotif.getvDireccion()).isEqualTo(UPDATED_V_DIRECCION);
        assertThat(testDirecnotif.isNflgtrabaj()).isEqualTo(UPDATED_NFLGTRABAJ);
        assertThat(testDirecnotif.getvHojaenvio()).isEqualTo(UPDATED_V_HOJAENVIO);
        assertThat(testDirecnotif.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testDirecnotif.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testDirecnotif.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testDirecnotif.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testDirecnotif.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testDirecnotif.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testDirecnotif.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Direcnotif in Elasticsearch
        Direcnotif direcnotifEs = direcnotifSearchRepository.findOne(testDirecnotif.getId());
        assertThat(direcnotifEs).isEqualToComparingFieldByField(testDirecnotif);
    }

    @Test
    @Transactional
    public void updateNonExistingDirecnotif() throws Exception {
        int databaseSizeBeforeUpdate = direcnotifRepository.findAll().size();

        // Create the Direcnotif

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDirecnotifMockMvc.perform(put("/api/direcnotifs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(direcnotif)))
            .andExpect(status().isCreated());

        // Validate the Direcnotif in the database
        List<Direcnotif> direcnotifList = direcnotifRepository.findAll();
        assertThat(direcnotifList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDirecnotif() throws Exception {
        // Initialize the database
        direcnotifRepository.saveAndFlush(direcnotif);
        direcnotifSearchRepository.save(direcnotif);
        int databaseSizeBeforeDelete = direcnotifRepository.findAll().size();

        // Get the direcnotif
        restDirecnotifMockMvc.perform(delete("/api/direcnotifs/{id}", direcnotif.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean direcnotifExistsInEs = direcnotifSearchRepository.exists(direcnotif.getId());
        assertThat(direcnotifExistsInEs).isFalse();

        // Validate the database is empty
        List<Direcnotif> direcnotifList = direcnotifRepository.findAll();
        assertThat(direcnotifList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDirecnotif() throws Exception {
        // Initialize the database
        direcnotifRepository.saveAndFlush(direcnotif);
        direcnotifSearchRepository.save(direcnotif);

        // Search the direcnotif
        restDirecnotifMockMvc.perform(get("/api/_search/direcnotifs?query=id:" + direcnotif.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(direcnotif.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDireccion").value(hasItem(DEFAULT_V_DIRECCION.toString())))
            .andExpect(jsonPath("$.[*].nflgtrabaj").value(hasItem(DEFAULT_NFLGTRABAJ.booleanValue())))
            .andExpect(jsonPath("$.[*].vHojaenvio").value(hasItem(DEFAULT_V_HOJAENVIO.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Direcnotif.class);
        Direcnotif direcnotif1 = new Direcnotif();
        direcnotif1.setId(1L);
        Direcnotif direcnotif2 = new Direcnotif();
        direcnotif2.setId(direcnotif1.getId());
        assertThat(direcnotif1).isEqualTo(direcnotif2);
        direcnotif2.setId(2L);
        assertThat(direcnotif1).isNotEqualTo(direcnotif2);
        direcnotif1.setId(null);
        assertThat(direcnotif1).isNotEqualTo(direcnotif2);
    }
}
