package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Tipproveid;
import pe.gob.trabajo.repository.TipproveidRepository;
import pe.gob.trabajo.repository.search.TipproveidSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipproveidResource REST controller.
 *
 * @see TipproveidResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class TipproveidResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private TipproveidRepository tipproveidRepository;

    @Autowired
    private TipproveidSearchRepository tipproveidSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipproveidMockMvc;

    private Tipproveid tipproveid;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipproveidResource tipproveidResource = new TipproveidResource(tipproveidRepository, tipproveidSearchRepository);
        this.restTipproveidMockMvc = MockMvcBuilders.standaloneSetup(tipproveidResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tipproveid createEntity(EntityManager em) {
        Tipproveid tipproveid = new Tipproveid()
            .vDescrip(DEFAULT_V_DESCRIP)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return tipproveid;
    }

    @Before
    public void initTest() {
        tipproveidSearchRepository.deleteAll();
        tipproveid = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipproveid() throws Exception {
        int databaseSizeBeforeCreate = tipproveidRepository.findAll().size();

        // Create the Tipproveid
        restTipproveidMockMvc.perform(post("/api/tipproveids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipproveid)))
            .andExpect(status().isCreated());

        // Validate the Tipproveid in the database
        List<Tipproveid> tipproveidList = tipproveidRepository.findAll();
        assertThat(tipproveidList).hasSize(databaseSizeBeforeCreate + 1);
        Tipproveid testTipproveid = tipproveidList.get(tipproveidList.size() - 1);
        assertThat(testTipproveid.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testTipproveid.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testTipproveid.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testTipproveid.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testTipproveid.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testTipproveid.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testTipproveid.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testTipproveid.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Tipproveid in Elasticsearch
        Tipproveid tipproveidEs = tipproveidSearchRepository.findOne(testTipproveid.getId());
        assertThat(tipproveidEs).isEqualToComparingFieldByField(testTipproveid);
    }

    @Test
    @Transactional
    public void createTipproveidWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipproveidRepository.findAll().size();

        // Create the Tipproveid with an existing ID
        tipproveid.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipproveidMockMvc.perform(post("/api/tipproveids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipproveid)))
            .andExpect(status().isBadRequest());

        // Validate the Tipproveid in the database
        List<Tipproveid> tipproveidList = tipproveidRepository.findAll();
        assertThat(tipproveidList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipproveidRepository.findAll().size();
        // set the field null
        tipproveid.setvDescrip(null);

        // Create the Tipproveid, which fails.

        restTipproveidMockMvc.perform(post("/api/tipproveids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipproveid)))
            .andExpect(status().isBadRequest());

        List<Tipproveid> tipproveidList = tipproveidRepository.findAll();
        assertThat(tipproveidList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipproveidRepository.findAll().size();
        // set the field null
        tipproveid.setnUsuareg(null);

        // Create the Tipproveid, which fails.

        restTipproveidMockMvc.perform(post("/api/tipproveids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipproveid)))
            .andExpect(status().isBadRequest());

        List<Tipproveid> tipproveidList = tipproveidRepository.findAll();
        assertThat(tipproveidList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipproveidRepository.findAll().size();
        // set the field null
        tipproveid.settFecreg(null);

        // Create the Tipproveid, which fails.

        restTipproveidMockMvc.perform(post("/api/tipproveids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipproveid)))
            .andExpect(status().isBadRequest());

        List<Tipproveid> tipproveidList = tipproveidRepository.findAll();
        assertThat(tipproveidList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipproveidRepository.findAll().size();
        // set the field null
        tipproveid.setnFlgactivo(null);

        // Create the Tipproveid, which fails.

        restTipproveidMockMvc.perform(post("/api/tipproveids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipproveid)))
            .andExpect(status().isBadRequest());

        List<Tipproveid> tipproveidList = tipproveidRepository.findAll();
        assertThat(tipproveidList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipproveidRepository.findAll().size();
        // set the field null
        tipproveid.setnSedereg(null);

        // Create the Tipproveid, which fails.

        restTipproveidMockMvc.perform(post("/api/tipproveids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipproveid)))
            .andExpect(status().isBadRequest());

        List<Tipproveid> tipproveidList = tipproveidRepository.findAll();
        assertThat(tipproveidList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipproveids() throws Exception {
        // Initialize the database
        tipproveidRepository.saveAndFlush(tipproveid);

        // Get all the tipproveidList
        restTipproveidMockMvc.perform(get("/api/tipproveids?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipproveid.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getTipproveid() throws Exception {
        // Initialize the database
        tipproveidRepository.saveAndFlush(tipproveid);

        // Get the tipproveid
        restTipproveidMockMvc.perform(get("/api/tipproveids/{id}", tipproveid.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipproveid.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingTipproveid() throws Exception {
        // Get the tipproveid
        restTipproveidMockMvc.perform(get("/api/tipproveids/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipproveid() throws Exception {
        // Initialize the database
        tipproveidRepository.saveAndFlush(tipproveid);
        tipproveidSearchRepository.save(tipproveid);
        int databaseSizeBeforeUpdate = tipproveidRepository.findAll().size();

        // Update the tipproveid
        Tipproveid updatedTipproveid = tipproveidRepository.findOne(tipproveid.getId());
        updatedTipproveid
            .vDescrip(UPDATED_V_DESCRIP)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restTipproveidMockMvc.perform(put("/api/tipproveids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipproveid)))
            .andExpect(status().isOk());

        // Validate the Tipproveid in the database
        List<Tipproveid> tipproveidList = tipproveidRepository.findAll();
        assertThat(tipproveidList).hasSize(databaseSizeBeforeUpdate);
        Tipproveid testTipproveid = tipproveidList.get(tipproveidList.size() - 1);
        assertThat(testTipproveid.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testTipproveid.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testTipproveid.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testTipproveid.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testTipproveid.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testTipproveid.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testTipproveid.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testTipproveid.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Tipproveid in Elasticsearch
        Tipproveid tipproveidEs = tipproveidSearchRepository.findOne(testTipproveid.getId());
        assertThat(tipproveidEs).isEqualToComparingFieldByField(testTipproveid);
    }

    @Test
    @Transactional
    public void updateNonExistingTipproveid() throws Exception {
        int databaseSizeBeforeUpdate = tipproveidRepository.findAll().size();

        // Create the Tipproveid

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipproveidMockMvc.perform(put("/api/tipproveids")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipproveid)))
            .andExpect(status().isCreated());

        // Validate the Tipproveid in the database
        List<Tipproveid> tipproveidList = tipproveidRepository.findAll();
        assertThat(tipproveidList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipproveid() throws Exception {
        // Initialize the database
        tipproveidRepository.saveAndFlush(tipproveid);
        tipproveidSearchRepository.save(tipproveid);
        int databaseSizeBeforeDelete = tipproveidRepository.findAll().size();

        // Get the tipproveid
        restTipproveidMockMvc.perform(delete("/api/tipproveids/{id}", tipproveid.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tipproveidExistsInEs = tipproveidSearchRepository.exists(tipproveid.getId());
        assertThat(tipproveidExistsInEs).isFalse();

        // Validate the database is empty
        List<Tipproveid> tipproveidList = tipproveidRepository.findAll();
        assertThat(tipproveidList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTipproveid() throws Exception {
        // Initialize the database
        tipproveidRepository.saveAndFlush(tipproveid);
        tipproveidSearchRepository.save(tipproveid);

        // Search the tipproveid
        restTipproveidMockMvc.perform(get("/api/_search/tipproveids?query=id:" + tipproveid.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipproveid.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tipproveid.class);
        Tipproveid tipproveid1 = new Tipproveid();
        tipproveid1.setId(1L);
        Tipproveid tipproveid2 = new Tipproveid();
        tipproveid2.setId(tipproveid1.getId());
        assertThat(tipproveid1).isEqualTo(tipproveid2);
        tipproveid2.setId(2L);
        assertThat(tipproveid1).isNotEqualTo(tipproveid2);
        tipproveid1.setId(null);
        assertThat(tipproveid1).isNotEqualTo(tipproveid2);
    }
}
