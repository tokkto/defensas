package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Docexpedien;
import pe.gob.trabajo.repository.DocexpedienRepository;
import pe.gob.trabajo.repository.search.DocexpedienSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DocexpedienResource REST controller.
 *
 * @see DocexpedienResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class DocexpedienResourceIntTest {

    private static final String DEFAULT_V_NUMOFICIO = "AAAAAAAAAA";
    private static final String UPDATED_V_NUMOFICIO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECHADOC = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECHADOC = LocalDate.now(ZoneId.systemDefault());

    private static final Integer DEFAULT_N_FOLIOS = 1;
    private static final Integer UPDATED_N_FOLIOS = 2;

    private static final String DEFAULT_V_NUMRESORD = "AAAAAAAAAA";
    private static final String UPDATED_V_NUMRESORD = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private DocexpedienRepository docexpedienRepository;

    @Autowired
    private DocexpedienSearchRepository docexpedienSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDocexpedienMockMvc;

    private Docexpedien docexpedien;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DocexpedienResource docexpedienResource = new DocexpedienResource(docexpedienRepository, docexpedienSearchRepository);
        this.restDocexpedienMockMvc = MockMvcBuilders.standaloneSetup(docexpedienResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Docexpedien createEntity(EntityManager em) {
        Docexpedien docexpedien = new Docexpedien()
            .vNumoficio(DEFAULT_V_NUMOFICIO)
            .dFechadoc(DEFAULT_D_FECHADOC)
            .nFolios(DEFAULT_N_FOLIOS)
            .vNumresord(DEFAULT_V_NUMRESORD)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return docexpedien;
    }

    @Before
    public void initTest() {
        docexpedienSearchRepository.deleteAll();
        docexpedien = createEntity(em);
    }

    @Test
    @Transactional
    public void createDocexpedien() throws Exception {
        int databaseSizeBeforeCreate = docexpedienRepository.findAll().size();

        // Create the Docexpedien
        restDocexpedienMockMvc.perform(post("/api/docexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(docexpedien)))
            .andExpect(status().isCreated());

        // Validate the Docexpedien in the database
        List<Docexpedien> docexpedienList = docexpedienRepository.findAll();
        assertThat(docexpedienList).hasSize(databaseSizeBeforeCreate + 1);
        Docexpedien testDocexpedien = docexpedienList.get(docexpedienList.size() - 1);
        assertThat(testDocexpedien.getvNumoficio()).isEqualTo(DEFAULT_V_NUMOFICIO);
        assertThat(testDocexpedien.getdFechadoc()).isEqualTo(DEFAULT_D_FECHADOC);
        assertThat(testDocexpedien.getnFolios()).isEqualTo(DEFAULT_N_FOLIOS);
        assertThat(testDocexpedien.getvNumresord()).isEqualTo(DEFAULT_V_NUMRESORD);
        assertThat(testDocexpedien.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testDocexpedien.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testDocexpedien.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testDocexpedien.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testDocexpedien.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testDocexpedien.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testDocexpedien.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Docexpedien in Elasticsearch
        Docexpedien docexpedienEs = docexpedienSearchRepository.findOne(testDocexpedien.getId());
        assertThat(docexpedienEs).isEqualToComparingFieldByField(testDocexpedien);
    }

    @Test
    @Transactional
    public void createDocexpedienWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = docexpedienRepository.findAll().size();

        // Create the Docexpedien with an existing ID
        docexpedien.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDocexpedienMockMvc.perform(post("/api/docexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(docexpedien)))
            .andExpect(status().isBadRequest());

        // Validate the Docexpedien in the database
        List<Docexpedien> docexpedienList = docexpedienRepository.findAll();
        assertThat(docexpedienList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = docexpedienRepository.findAll().size();
        // set the field null
        docexpedien.setnUsuareg(null);

        // Create the Docexpedien, which fails.

        restDocexpedienMockMvc.perform(post("/api/docexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(docexpedien)))
            .andExpect(status().isBadRequest());

        List<Docexpedien> docexpedienList = docexpedienRepository.findAll();
        assertThat(docexpedienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = docexpedienRepository.findAll().size();
        // set the field null
        docexpedien.settFecreg(null);

        // Create the Docexpedien, which fails.

        restDocexpedienMockMvc.perform(post("/api/docexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(docexpedien)))
            .andExpect(status().isBadRequest());

        List<Docexpedien> docexpedienList = docexpedienRepository.findAll();
        assertThat(docexpedienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = docexpedienRepository.findAll().size();
        // set the field null
        docexpedien.setnFlgactivo(null);

        // Create the Docexpedien, which fails.

        restDocexpedienMockMvc.perform(post("/api/docexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(docexpedien)))
            .andExpect(status().isBadRequest());

        List<Docexpedien> docexpedienList = docexpedienRepository.findAll();
        assertThat(docexpedienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = docexpedienRepository.findAll().size();
        // set the field null
        docexpedien.setnSedereg(null);

        // Create the Docexpedien, which fails.

        restDocexpedienMockMvc.perform(post("/api/docexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(docexpedien)))
            .andExpect(status().isBadRequest());

        List<Docexpedien> docexpedienList = docexpedienRepository.findAll();
        assertThat(docexpedienList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDocexpediens() throws Exception {
        // Initialize the database
        docexpedienRepository.saveAndFlush(docexpedien);

        // Get all the docexpedienList
        restDocexpedienMockMvc.perform(get("/api/docexpediens?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(docexpedien.getId().intValue())))
            .andExpect(jsonPath("$.[*].vNumoficio").value(hasItem(DEFAULT_V_NUMOFICIO.toString())))
            .andExpect(jsonPath("$.[*].dFechadoc").value(hasItem(DEFAULT_D_FECHADOC.toString())))
            .andExpect(jsonPath("$.[*].nFolios").value(hasItem(DEFAULT_N_FOLIOS)))
            .andExpect(jsonPath("$.[*].vNumresord").value(hasItem(DEFAULT_V_NUMRESORD.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getDocexpedien() throws Exception {
        // Initialize the database
        docexpedienRepository.saveAndFlush(docexpedien);

        // Get the docexpedien
        restDocexpedienMockMvc.perform(get("/api/docexpediens/{id}", docexpedien.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(docexpedien.getId().intValue()))
            .andExpect(jsonPath("$.vNumoficio").value(DEFAULT_V_NUMOFICIO.toString()))
            .andExpect(jsonPath("$.dFechadoc").value(DEFAULT_D_FECHADOC.toString()))
            .andExpect(jsonPath("$.nFolios").value(DEFAULT_N_FOLIOS))
            .andExpect(jsonPath("$.vNumresord").value(DEFAULT_V_NUMRESORD.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingDocexpedien() throws Exception {
        // Get the docexpedien
        restDocexpedienMockMvc.perform(get("/api/docexpediens/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDocexpedien() throws Exception {
        // Initialize the database
        docexpedienRepository.saveAndFlush(docexpedien);
        docexpedienSearchRepository.save(docexpedien);
        int databaseSizeBeforeUpdate = docexpedienRepository.findAll().size();

        // Update the docexpedien
        Docexpedien updatedDocexpedien = docexpedienRepository.findOne(docexpedien.getId());
        updatedDocexpedien
            .vNumoficio(UPDATED_V_NUMOFICIO)
            .dFechadoc(UPDATED_D_FECHADOC)
            .nFolios(UPDATED_N_FOLIOS)
            .vNumresord(UPDATED_V_NUMRESORD)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restDocexpedienMockMvc.perform(put("/api/docexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDocexpedien)))
            .andExpect(status().isOk());

        // Validate the Docexpedien in the database
        List<Docexpedien> docexpedienList = docexpedienRepository.findAll();
        assertThat(docexpedienList).hasSize(databaseSizeBeforeUpdate);
        Docexpedien testDocexpedien = docexpedienList.get(docexpedienList.size() - 1);
        assertThat(testDocexpedien.getvNumoficio()).isEqualTo(UPDATED_V_NUMOFICIO);
        assertThat(testDocexpedien.getdFechadoc()).isEqualTo(UPDATED_D_FECHADOC);
        assertThat(testDocexpedien.getnFolios()).isEqualTo(UPDATED_N_FOLIOS);
        assertThat(testDocexpedien.getvNumresord()).isEqualTo(UPDATED_V_NUMRESORD);
        assertThat(testDocexpedien.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testDocexpedien.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testDocexpedien.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testDocexpedien.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testDocexpedien.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testDocexpedien.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testDocexpedien.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Docexpedien in Elasticsearch
        Docexpedien docexpedienEs = docexpedienSearchRepository.findOne(testDocexpedien.getId());
        assertThat(docexpedienEs).isEqualToComparingFieldByField(testDocexpedien);
    }

    @Test
    @Transactional
    public void updateNonExistingDocexpedien() throws Exception {
        int databaseSizeBeforeUpdate = docexpedienRepository.findAll().size();

        // Create the Docexpedien

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDocexpedienMockMvc.perform(put("/api/docexpediens")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(docexpedien)))
            .andExpect(status().isCreated());

        // Validate the Docexpedien in the database
        List<Docexpedien> docexpedienList = docexpedienRepository.findAll();
        assertThat(docexpedienList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDocexpedien() throws Exception {
        // Initialize the database
        docexpedienRepository.saveAndFlush(docexpedien);
        docexpedienSearchRepository.save(docexpedien);
        int databaseSizeBeforeDelete = docexpedienRepository.findAll().size();

        // Get the docexpedien
        restDocexpedienMockMvc.perform(delete("/api/docexpediens/{id}", docexpedien.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean docexpedienExistsInEs = docexpedienSearchRepository.exists(docexpedien.getId());
        assertThat(docexpedienExistsInEs).isFalse();

        // Validate the database is empty
        List<Docexpedien> docexpedienList = docexpedienRepository.findAll();
        assertThat(docexpedienList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchDocexpedien() throws Exception {
        // Initialize the database
        docexpedienRepository.saveAndFlush(docexpedien);
        docexpedienSearchRepository.save(docexpedien);

        // Search the docexpedien
        restDocexpedienMockMvc.perform(get("/api/_search/docexpediens?query=id:" + docexpedien.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(docexpedien.getId().intValue())))
            .andExpect(jsonPath("$.[*].vNumoficio").value(hasItem(DEFAULT_V_NUMOFICIO.toString())))
            .andExpect(jsonPath("$.[*].dFechadoc").value(hasItem(DEFAULT_D_FECHADOC.toString())))
            .andExpect(jsonPath("$.[*].nFolios").value(hasItem(DEFAULT_N_FOLIOS)))
            .andExpect(jsonPath("$.[*].vNumresord").value(hasItem(DEFAULT_V_NUMRESORD.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Docexpedien.class);
        Docexpedien docexpedien1 = new Docexpedien();
        docexpedien1.setId(1L);
        Docexpedien docexpedien2 = new Docexpedien();
        docexpedien2.setId(docexpedien1.getId());
        assertThat(docexpedien1).isEqualTo(docexpedien2);
        docexpedien2.setId(2L);
        assertThat(docexpedien1).isNotEqualTo(docexpedien2);
        docexpedien1.setId(null);
        assertThat(docexpedien1).isNotEqualTo(docexpedien2);
    }
}
