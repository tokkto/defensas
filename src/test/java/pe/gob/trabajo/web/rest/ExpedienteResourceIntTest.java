package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Expediente;
import pe.gob.trabajo.repository.ExpedienteRepository;
import pe.gob.trabajo.repository.search.ExpedienteSearchRepository;
import pe.gob.trabajo.repository.PaseglRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ExpedienteResource REST controller.
 *
 * @see ExpedienteResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class ExpedienteResourceIntTest {

    private static final String DEFAULT_V_NUMEXP = "AAAAAAAAAA";
    private static final String UPDATED_V_NUMEXP = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_ANIOEXP = 1;
    private static final Integer UPDATED_N_ANIOEXP = 2;

    private static final LocalDate DEFAULT_D_FECREGEXP = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECREGEXP = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_NOMEMPLEA = "AAAAAAAAAA";
    private static final String UPDATED_V_NOMEMPLEA = "BBBBBBBBBB";

    private static final String DEFAULT_V_NOMTRABAJ = "AAAAAAAAAA";
    private static final String UPDATED_V_NOMTRABAJ = "BBBBBBBBBB";

    private static final Boolean DEFAULT_N_FLGEXPOBS = false;
    private static final Boolean UPDATED_N_FLGEXPOBS = true;

    private static final String DEFAULT_V_REGMESPAR = "AAAAAAAAAA";
    private static final String UPDATED_V_REGMESPAR = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECMESPAR = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECMESPAR = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_OBSERVAC = "AAAAAAAAAA";
    private static final String UPDATED_V_OBSERVAC = "BBBBBBBBBB";

    private static final Boolean DEFAULT_N_FLGARCHIV = false;
    private static final Boolean UPDATED_N_FLGARCHIV = true;

    private static final LocalDate DEFAULT_D_FEC_ARCHIV = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FEC_ARCHIV = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_OBSARCHIV = "AAAAAAAAAA";
    private static final String UPDATED_V_OBSARCHIV = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_CODUSUARC = 1;
    private static final Integer UPDATED_N_CODUSUARC = 2;

    private static final String DEFAULT_V_NUMINFARC = "AAAAAAAAAA";
    private static final String UPDATED_V_NUMINFARC = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private ExpedienteRepository expedienteRepository;

    @Autowired
    private ExpedienteSearchRepository expedienteSearchRepository;
    
    @Autowired
    private PaseglRepository paseglRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restExpedienteMockMvc;

    private Expediente expediente;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ExpedienteResource expedienteResource = new ExpedienteResource(expedienteRepository, expedienteSearchRepository, paseglRepository);
        this.restExpedienteMockMvc = MockMvcBuilders.standaloneSetup(expedienteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Expediente createEntity(EntityManager em) {
        Expediente expediente = new Expediente()
            .vNumexp(DEFAULT_V_NUMEXP)
            .nAnioexp(DEFAULT_N_ANIOEXP)
            .dFecregexp(DEFAULT_D_FECREGEXP)
            .vNomemplea(DEFAULT_V_NOMEMPLEA)
            .vNomtrabaj(DEFAULT_V_NOMTRABAJ)
            .nFlgexpobs(DEFAULT_N_FLGEXPOBS)
            .vRegmespar(DEFAULT_V_REGMESPAR)
            .dFecmespar(DEFAULT_D_FECMESPAR)
            .vObservac(DEFAULT_V_OBSERVAC)
            .nFlgarchiv(DEFAULT_N_FLGARCHIV)
            .dFecArchiv(DEFAULT_D_FEC_ARCHIV)
            .vObsarchiv(DEFAULT_V_OBSARCHIV)
            .nCodusuarc(DEFAULT_N_CODUSUARC)
            .vNuminfarc(DEFAULT_V_NUMINFARC)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return expediente;
    }

    @Before
    public void initTest() {
        expedienteSearchRepository.deleteAll();
        expediente = createEntity(em);
    }

    @Test
    @Transactional
    public void createExpediente() throws Exception {
        int databaseSizeBeforeCreate = expedienteRepository.findAll().size();

        // Create the Expediente
        restExpedienteMockMvc.perform(post("/api/expedientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expediente)))
            .andExpect(status().isCreated());

        // Validate the Expediente in the database
        List<Expediente> expedienteList = expedienteRepository.findAll();
        assertThat(expedienteList).hasSize(databaseSizeBeforeCreate + 1);
        Expediente testExpediente = expedienteList.get(expedienteList.size() - 1);
        assertThat(testExpediente.getvNumexp()).isEqualTo(DEFAULT_V_NUMEXP);
        assertThat(testExpediente.getnAnioexp()).isEqualTo(DEFAULT_N_ANIOEXP);
        assertThat(testExpediente.getdFecregexp()).isEqualTo(DEFAULT_D_FECREGEXP);
        assertThat(testExpediente.getvNomemplea()).isEqualTo(DEFAULT_V_NOMEMPLEA);
        assertThat(testExpediente.getvNomtrabaj()).isEqualTo(DEFAULT_V_NOMTRABAJ);
        assertThat(testExpediente.isnFlgexpobs()).isEqualTo(DEFAULT_N_FLGEXPOBS);
        assertThat(testExpediente.getvRegmespar()).isEqualTo(DEFAULT_V_REGMESPAR);
        assertThat(testExpediente.getdFecmespar()).isEqualTo(DEFAULT_D_FECMESPAR);
        assertThat(testExpediente.getvObservac()).isEqualTo(DEFAULT_V_OBSERVAC);
        assertThat(testExpediente.isnFlgarchiv()).isEqualTo(DEFAULT_N_FLGARCHIV);
        assertThat(testExpediente.getdFecArchiv()).isEqualTo(DEFAULT_D_FEC_ARCHIV);
        assertThat(testExpediente.getvObsarchiv()).isEqualTo(DEFAULT_V_OBSARCHIV);
        assertThat(testExpediente.getnCodusuarc()).isEqualTo(DEFAULT_N_CODUSUARC);
        assertThat(testExpediente.getvNuminfarc()).isEqualTo(DEFAULT_V_NUMINFARC);
        assertThat(testExpediente.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testExpediente.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testExpediente.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testExpediente.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testExpediente.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testExpediente.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testExpediente.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Expediente in Elasticsearch
        Expediente expedienteEs = expedienteSearchRepository.findOne(testExpediente.getId());
        assertThat(expedienteEs).isEqualToComparingFieldByField(testExpediente);
    }

    @Test
    @Transactional
    public void createExpedienteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = expedienteRepository.findAll().size();

        // Create the Expediente with an existing ID
        expediente.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restExpedienteMockMvc.perform(post("/api/expedientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expediente)))
            .andExpect(status().isBadRequest());

        // Validate the Expediente in the database
        List<Expediente> expedienteList = expedienteRepository.findAll();
        assertThat(expedienteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvNumexpIsRequired() throws Exception {
        int databaseSizeBeforeTest = expedienteRepository.findAll().size();
        // set the field null
        expediente.setvNumexp(null);

        // Create the Expediente, which fails.

        restExpedienteMockMvc.perform(post("/api/expedientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expediente)))
            .andExpect(status().isBadRequest());

        List<Expediente> expedienteList = expedienteRepository.findAll();
        assertThat(expedienteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknAnioexpIsRequired() throws Exception {
        int databaseSizeBeforeTest = expedienteRepository.findAll().size();
        // set the field null
        expediente.setnAnioexp(null);

        // Create the Expediente, which fails.

        restExpedienteMockMvc.perform(post("/api/expedientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expediente)))
            .andExpect(status().isBadRequest());

        List<Expediente> expedienteList = expedienteRepository.findAll();
        assertThat(expedienteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = expedienteRepository.findAll().size();
        // set the field null
        expediente.setnUsuareg(null);

        // Create the Expediente, which fails.

        restExpedienteMockMvc.perform(post("/api/expedientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expediente)))
            .andExpect(status().isBadRequest());

        List<Expediente> expedienteList = expedienteRepository.findAll();
        assertThat(expedienteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = expedienteRepository.findAll().size();
        // set the field null
        expediente.settFecreg(null);

        // Create the Expediente, which fails.

        restExpedienteMockMvc.perform(post("/api/expedientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expediente)))
            .andExpect(status().isBadRequest());

        List<Expediente> expedienteList = expedienteRepository.findAll();
        assertThat(expedienteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = expedienteRepository.findAll().size();
        // set the field null
        expediente.setnFlgactivo(null);

        // Create the Expediente, which fails.

        restExpedienteMockMvc.perform(post("/api/expedientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expediente)))
            .andExpect(status().isBadRequest());

        List<Expediente> expedienteList = expedienteRepository.findAll();
        assertThat(expedienteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = expedienteRepository.findAll().size();
        // set the field null
        expediente.setnSedereg(null);

        // Create the Expediente, which fails.

        restExpedienteMockMvc.perform(post("/api/expedientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expediente)))
            .andExpect(status().isBadRequest());

        List<Expediente> expedienteList = expedienteRepository.findAll();
        assertThat(expedienteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllExpedientes() throws Exception {
        // Initialize the database
        expedienteRepository.saveAndFlush(expediente);

        // Get all the expedienteList
        restExpedienteMockMvc.perform(get("/api/expedientes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(expediente.getId().intValue())))
            .andExpect(jsonPath("$.[*].vNumexp").value(hasItem(DEFAULT_V_NUMEXP.toString())))
            .andExpect(jsonPath("$.[*].nAnioexp").value(hasItem(DEFAULT_N_ANIOEXP)))
            .andExpect(jsonPath("$.[*].dFecregexp").value(hasItem(DEFAULT_D_FECREGEXP.toString())))
            .andExpect(jsonPath("$.[*].vNomemplea").value(hasItem(DEFAULT_V_NOMEMPLEA.toString())))
            .andExpect(jsonPath("$.[*].vNomtrabaj").value(hasItem(DEFAULT_V_NOMTRABAJ.toString())))
            .andExpect(jsonPath("$.[*].nFlgexpobs").value(hasItem(DEFAULT_N_FLGEXPOBS.booleanValue())))
            .andExpect(jsonPath("$.[*].vRegmespar").value(hasItem(DEFAULT_V_REGMESPAR.toString())))
            .andExpect(jsonPath("$.[*].dFecmespar").value(hasItem(DEFAULT_D_FECMESPAR.toString())))
            .andExpect(jsonPath("$.[*].vObservac").value(hasItem(DEFAULT_V_OBSERVAC.toString())))
            .andExpect(jsonPath("$.[*].nFlgarchiv").value(hasItem(DEFAULT_N_FLGARCHIV.booleanValue())))
            .andExpect(jsonPath("$.[*].dFecArchiv").value(hasItem(DEFAULT_D_FEC_ARCHIV.toString())))
            .andExpect(jsonPath("$.[*].vObsarchiv").value(hasItem(DEFAULT_V_OBSARCHIV.toString())))
            .andExpect(jsonPath("$.[*].nCodusuarc").value(hasItem(DEFAULT_N_CODUSUARC)))
            .andExpect(jsonPath("$.[*].vNuminfarc").value(hasItem(DEFAULT_V_NUMINFARC.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getExpediente() throws Exception {
        // Initialize the database
        expedienteRepository.saveAndFlush(expediente);

        // Get the expediente
        restExpedienteMockMvc.perform(get("/api/expedientes/{id}", expediente.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(expediente.getId().intValue()))
            .andExpect(jsonPath("$.vNumexp").value(DEFAULT_V_NUMEXP.toString()))
            .andExpect(jsonPath("$.nAnioexp").value(DEFAULT_N_ANIOEXP))
            .andExpect(jsonPath("$.dFecregexp").value(DEFAULT_D_FECREGEXP.toString()))
            .andExpect(jsonPath("$.vNomemplea").value(DEFAULT_V_NOMEMPLEA.toString()))
            .andExpect(jsonPath("$.vNomtrabaj").value(DEFAULT_V_NOMTRABAJ.toString()))
            .andExpect(jsonPath("$.nFlgexpobs").value(DEFAULT_N_FLGEXPOBS.booleanValue()))
            .andExpect(jsonPath("$.vRegmespar").value(DEFAULT_V_REGMESPAR.toString()))
            .andExpect(jsonPath("$.dFecmespar").value(DEFAULT_D_FECMESPAR.toString()))
            .andExpect(jsonPath("$.vObservac").value(DEFAULT_V_OBSERVAC.toString()))
            .andExpect(jsonPath("$.nFlgarchiv").value(DEFAULT_N_FLGARCHIV.booleanValue()))
            .andExpect(jsonPath("$.dFecArchiv").value(DEFAULT_D_FEC_ARCHIV.toString()))
            .andExpect(jsonPath("$.vObsarchiv").value(DEFAULT_V_OBSARCHIV.toString()))
            .andExpect(jsonPath("$.nCodusuarc").value(DEFAULT_N_CODUSUARC))
            .andExpect(jsonPath("$.vNuminfarc").value(DEFAULT_V_NUMINFARC.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingExpediente() throws Exception {
        // Get the expediente
        restExpedienteMockMvc.perform(get("/api/expedientes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateExpediente() throws Exception {
        // Initialize the database
        expedienteRepository.saveAndFlush(expediente);
        expedienteSearchRepository.save(expediente);
        int databaseSizeBeforeUpdate = expedienteRepository.findAll().size();

        // Update the expediente
        Expediente updatedExpediente = expedienteRepository.findOne(expediente.getId());
        updatedExpediente
            .vNumexp(UPDATED_V_NUMEXP)
            .nAnioexp(UPDATED_N_ANIOEXP)
            .dFecregexp(UPDATED_D_FECREGEXP)
            .vNomemplea(UPDATED_V_NOMEMPLEA)
            .vNomtrabaj(UPDATED_V_NOMTRABAJ)
            .nFlgexpobs(UPDATED_N_FLGEXPOBS)
            .vRegmespar(UPDATED_V_REGMESPAR)
            .dFecmespar(UPDATED_D_FECMESPAR)
            .vObservac(UPDATED_V_OBSERVAC)
            .nFlgarchiv(UPDATED_N_FLGARCHIV)
            .dFecArchiv(UPDATED_D_FEC_ARCHIV)
            .vObsarchiv(UPDATED_V_OBSARCHIV)
            .nCodusuarc(UPDATED_N_CODUSUARC)
            .vNuminfarc(UPDATED_V_NUMINFARC)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restExpedienteMockMvc.perform(put("/api/expedientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedExpediente)))
            .andExpect(status().isOk());

        // Validate the Expediente in the database
        List<Expediente> expedienteList = expedienteRepository.findAll();
        assertThat(expedienteList).hasSize(databaseSizeBeforeUpdate);
        Expediente testExpediente = expedienteList.get(expedienteList.size() - 1);
        assertThat(testExpediente.getvNumexp()).isEqualTo(UPDATED_V_NUMEXP);
        assertThat(testExpediente.getnAnioexp()).isEqualTo(UPDATED_N_ANIOEXP);
        assertThat(testExpediente.getdFecregexp()).isEqualTo(UPDATED_D_FECREGEXP);
        assertThat(testExpediente.getvNomemplea()).isEqualTo(UPDATED_V_NOMEMPLEA);
        assertThat(testExpediente.getvNomtrabaj()).isEqualTo(UPDATED_V_NOMTRABAJ);
        assertThat(testExpediente.isnFlgexpobs()).isEqualTo(UPDATED_N_FLGEXPOBS);
        assertThat(testExpediente.getvRegmespar()).isEqualTo(UPDATED_V_REGMESPAR);
        assertThat(testExpediente.getdFecmespar()).isEqualTo(UPDATED_D_FECMESPAR);
        assertThat(testExpediente.getvObservac()).isEqualTo(UPDATED_V_OBSERVAC);
        assertThat(testExpediente.isnFlgarchiv()).isEqualTo(UPDATED_N_FLGARCHIV);
        assertThat(testExpediente.getdFecArchiv()).isEqualTo(UPDATED_D_FEC_ARCHIV);
        assertThat(testExpediente.getvObsarchiv()).isEqualTo(UPDATED_V_OBSARCHIV);
        assertThat(testExpediente.getnCodusuarc()).isEqualTo(UPDATED_N_CODUSUARC);
        assertThat(testExpediente.getvNuminfarc()).isEqualTo(UPDATED_V_NUMINFARC);
        assertThat(testExpediente.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testExpediente.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testExpediente.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testExpediente.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testExpediente.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testExpediente.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testExpediente.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Expediente in Elasticsearch
        Expediente expedienteEs = expedienteSearchRepository.findOne(testExpediente.getId());
        assertThat(expedienteEs).isEqualToComparingFieldByField(testExpediente);
    }

    @Test
    @Transactional
    public void updateNonExistingExpediente() throws Exception {
        int databaseSizeBeforeUpdate = expedienteRepository.findAll().size();

        // Create the Expediente

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restExpedienteMockMvc.perform(put("/api/expedientes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(expediente)))
            .andExpect(status().isCreated());

        // Validate the Expediente in the database
        List<Expediente> expedienteList = expedienteRepository.findAll();
        assertThat(expedienteList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteExpediente() throws Exception {
        // Initialize the database
        expedienteRepository.saveAndFlush(expediente);
        expedienteSearchRepository.save(expediente);
        int databaseSizeBeforeDelete = expedienteRepository.findAll().size();

        // Get the expediente
        restExpedienteMockMvc.perform(delete("/api/expedientes/{id}", expediente.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean expedienteExistsInEs = expedienteSearchRepository.exists(expediente.getId());
        assertThat(expedienteExistsInEs).isFalse();

        // Validate the database is empty
        List<Expediente> expedienteList = expedienteRepository.findAll();
        assertThat(expedienteList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchExpediente() throws Exception {
        // Initialize the database
        expedienteRepository.saveAndFlush(expediente);
        expedienteSearchRepository.save(expediente);

        // Search the expediente
        restExpedienteMockMvc.perform(get("/api/_search/expedientes?query=id:" + expediente.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(expediente.getId().intValue())))
            .andExpect(jsonPath("$.[*].vNumexp").value(hasItem(DEFAULT_V_NUMEXP.toString())))
            .andExpect(jsonPath("$.[*].nAnioexp").value(hasItem(DEFAULT_N_ANIOEXP)))
            .andExpect(jsonPath("$.[*].dFecregexp").value(hasItem(DEFAULT_D_FECREGEXP.toString())))
            .andExpect(jsonPath("$.[*].vNomemplea").value(hasItem(DEFAULT_V_NOMEMPLEA.toString())))
            .andExpect(jsonPath("$.[*].vNomtrabaj").value(hasItem(DEFAULT_V_NOMTRABAJ.toString())))
            .andExpect(jsonPath("$.[*].nFlgexpobs").value(hasItem(DEFAULT_N_FLGEXPOBS.booleanValue())))
            .andExpect(jsonPath("$.[*].vRegmespar").value(hasItem(DEFAULT_V_REGMESPAR.toString())))
            .andExpect(jsonPath("$.[*].dFecmespar").value(hasItem(DEFAULT_D_FECMESPAR.toString())))
            .andExpect(jsonPath("$.[*].vObservac").value(hasItem(DEFAULT_V_OBSERVAC.toString())))
            .andExpect(jsonPath("$.[*].nFlgarchiv").value(hasItem(DEFAULT_N_FLGARCHIV.booleanValue())))
            .andExpect(jsonPath("$.[*].dFecArchiv").value(hasItem(DEFAULT_D_FEC_ARCHIV.toString())))
            .andExpect(jsonPath("$.[*].vObsarchiv").value(hasItem(DEFAULT_V_OBSARCHIV.toString())))
            .andExpect(jsonPath("$.[*].nCodusuarc").value(hasItem(DEFAULT_N_CODUSUARC)))
            .andExpect(jsonPath("$.[*].vNuminfarc").value(hasItem(DEFAULT_V_NUMINFARC.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Expediente.class);
        Expediente expediente1 = new Expediente();
        expediente1.setId(1L);
        Expediente expediente2 = new Expediente();
        expediente2.setId(expediente1.getId());
        assertThat(expediente1).isEqualTo(expediente2);
        expediente2.setId(2L);
        assertThat(expediente1).isNotEqualTo(expediente2);
        expediente1.setId(null);
        assertThat(expediente1).isNotEqualTo(expediente2);
    }
}
