package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Notifica;
import pe.gob.trabajo.repository.NotificaRepository;
import pe.gob.trabajo.repository.search.NotificaSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the NotificaResource REST controller.
 *
 * @see NotificaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class NotificaResourceIntTest {

    private static final Integer DEFAULT_N_NUMFOLIOS = 1;
    private static final Integer UPDATED_N_NUMFOLIOS = 2;

    private static final String DEFAULT_V_HOJAENVIO = "AAAAAAAAAA";
    private static final String UPDATED_V_HOJAENVIO = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private NotificaRepository notificaRepository;

    @Autowired
    private NotificaSearchRepository notificaSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restNotificaMockMvc;

    private Notifica notifica;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final NotificaResource notificaResource = new NotificaResource(notificaRepository, notificaSearchRepository);
        this.restNotificaMockMvc = MockMvcBuilders.standaloneSetup(notificaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Notifica createEntity(EntityManager em) {
        Notifica notifica = new Notifica()
            .nNumfolios(DEFAULT_N_NUMFOLIOS)
            .vHojaenvio(DEFAULT_V_HOJAENVIO)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return notifica;
    }

    @Before
    public void initTest() {
        notificaSearchRepository.deleteAll();
        notifica = createEntity(em);
    }

    @Test
    @Transactional
    public void createNotifica() throws Exception {
        int databaseSizeBeforeCreate = notificaRepository.findAll().size();

        // Create the Notifica
        restNotificaMockMvc.perform(post("/api/notificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notifica)))
            .andExpect(status().isCreated());

        // Validate the Notifica in the database
        List<Notifica> notificaList = notificaRepository.findAll();
        assertThat(notificaList).hasSize(databaseSizeBeforeCreate + 1);
        Notifica testNotifica = notificaList.get(notificaList.size() - 1);
        assertThat(testNotifica.getnNumfolios()).isEqualTo(DEFAULT_N_NUMFOLIOS);
        assertThat(testNotifica.getvHojaenvio()).isEqualTo(DEFAULT_V_HOJAENVIO);
        assertThat(testNotifica.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testNotifica.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testNotifica.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testNotifica.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testNotifica.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testNotifica.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testNotifica.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Notifica in Elasticsearch
        Notifica notificaEs = notificaSearchRepository.findOne(testNotifica.getId());
        assertThat(notificaEs).isEqualToComparingFieldByField(testNotifica);
    }

    @Test
    @Transactional
    public void createNotificaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = notificaRepository.findAll().size();

        // Create the Notifica with an existing ID
        notifica.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restNotificaMockMvc.perform(post("/api/notificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notifica)))
            .andExpect(status().isBadRequest());

        // Validate the Notifica in the database
        List<Notifica> notificaList = notificaRepository.findAll();
        assertThat(notificaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificaRepository.findAll().size();
        // set the field null
        notifica.setnUsuareg(null);

        // Create the Notifica, which fails.

        restNotificaMockMvc.perform(post("/api/notificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notifica)))
            .andExpect(status().isBadRequest());

        List<Notifica> notificaList = notificaRepository.findAll();
        assertThat(notificaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificaRepository.findAll().size();
        // set the field null
        notifica.settFecreg(null);

        // Create the Notifica, which fails.

        restNotificaMockMvc.perform(post("/api/notificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notifica)))
            .andExpect(status().isBadRequest());

        List<Notifica> notificaList = notificaRepository.findAll();
        assertThat(notificaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificaRepository.findAll().size();
        // set the field null
        notifica.setnFlgactivo(null);

        // Create the Notifica, which fails.

        restNotificaMockMvc.perform(post("/api/notificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notifica)))
            .andExpect(status().isBadRequest());

        List<Notifica> notificaList = notificaRepository.findAll();
        assertThat(notificaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = notificaRepository.findAll().size();
        // set the field null
        notifica.setnSedereg(null);

        // Create the Notifica, which fails.

        restNotificaMockMvc.perform(post("/api/notificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notifica)))
            .andExpect(status().isBadRequest());

        List<Notifica> notificaList = notificaRepository.findAll();
        assertThat(notificaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllNotificas() throws Exception {
        // Initialize the database
        notificaRepository.saveAndFlush(notifica);

        // Get all the notificaList
        restNotificaMockMvc.perform(get("/api/notificas?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notifica.getId().intValue())))
            .andExpect(jsonPath("$.[*].nNumfolios").value(hasItem(DEFAULT_N_NUMFOLIOS)))
            .andExpect(jsonPath("$.[*].vHojaenvio").value(hasItem(DEFAULT_V_HOJAENVIO.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getNotifica() throws Exception {
        // Initialize the database
        notificaRepository.saveAndFlush(notifica);

        // Get the notifica
        restNotificaMockMvc.perform(get("/api/notificas/{id}", notifica.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(notifica.getId().intValue()))
            .andExpect(jsonPath("$.nNumfolios").value(DEFAULT_N_NUMFOLIOS))
            .andExpect(jsonPath("$.vHojaenvio").value(DEFAULT_V_HOJAENVIO.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingNotifica() throws Exception {
        // Get the notifica
        restNotificaMockMvc.perform(get("/api/notificas/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateNotifica() throws Exception {
        // Initialize the database
        notificaRepository.saveAndFlush(notifica);
        notificaSearchRepository.save(notifica);
        int databaseSizeBeforeUpdate = notificaRepository.findAll().size();

        // Update the notifica
        Notifica updatedNotifica = notificaRepository.findOne(notifica.getId());
        updatedNotifica
            .nNumfolios(UPDATED_N_NUMFOLIOS)
            .vHojaenvio(UPDATED_V_HOJAENVIO)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restNotificaMockMvc.perform(put("/api/notificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedNotifica)))
            .andExpect(status().isOk());

        // Validate the Notifica in the database
        List<Notifica> notificaList = notificaRepository.findAll();
        assertThat(notificaList).hasSize(databaseSizeBeforeUpdate);
        Notifica testNotifica = notificaList.get(notificaList.size() - 1);
        assertThat(testNotifica.getnNumfolios()).isEqualTo(UPDATED_N_NUMFOLIOS);
        assertThat(testNotifica.getvHojaenvio()).isEqualTo(UPDATED_V_HOJAENVIO);
        assertThat(testNotifica.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testNotifica.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testNotifica.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testNotifica.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testNotifica.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testNotifica.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testNotifica.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Notifica in Elasticsearch
        Notifica notificaEs = notificaSearchRepository.findOne(testNotifica.getId());
        assertThat(notificaEs).isEqualToComparingFieldByField(testNotifica);
    }

    @Test
    @Transactional
    public void updateNonExistingNotifica() throws Exception {
        int databaseSizeBeforeUpdate = notificaRepository.findAll().size();

        // Create the Notifica

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restNotificaMockMvc.perform(put("/api/notificas")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(notifica)))
            .andExpect(status().isCreated());

        // Validate the Notifica in the database
        List<Notifica> notificaList = notificaRepository.findAll();
        assertThat(notificaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteNotifica() throws Exception {
        // Initialize the database
        notificaRepository.saveAndFlush(notifica);
        notificaSearchRepository.save(notifica);
        int databaseSizeBeforeDelete = notificaRepository.findAll().size();

        // Get the notifica
        restNotificaMockMvc.perform(delete("/api/notificas/{id}", notifica.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean notificaExistsInEs = notificaSearchRepository.exists(notifica.getId());
        assertThat(notificaExistsInEs).isFalse();

        // Validate the database is empty
        List<Notifica> notificaList = notificaRepository.findAll();
        assertThat(notificaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchNotifica() throws Exception {
        // Initialize the database
        notificaRepository.saveAndFlush(notifica);
        notificaSearchRepository.save(notifica);

        // Search the notifica
        restNotificaMockMvc.perform(get("/api/_search/notificas?query=id:" + notifica.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(notifica.getId().intValue())))
            .andExpect(jsonPath("$.[*].nNumfolios").value(hasItem(DEFAULT_N_NUMFOLIOS)))
            .andExpect(jsonPath("$.[*].vHojaenvio").value(hasItem(DEFAULT_V_HOJAENVIO.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Notifica.class);
        Notifica notifica1 = new Notifica();
        notifica1.setId(1L);
        Notifica notifica2 = new Notifica();
        notifica2.setId(notifica1.getId());
        assertThat(notifica1).isEqualTo(notifica2);
        notifica2.setId(2L);
        assertThat(notifica1).isNotEqualTo(notifica2);
        notifica1.setId(null);
        assertThat(notifica1).isNotEqualTo(notifica2);
    }
}
