package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Concilia;
import pe.gob.trabajo.repository.CancohorfecRepository;
import pe.gob.trabajo.repository.ConciliaRepository;
import pe.gob.trabajo.repository.search.ConciliaSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ConciliaResource REST controller.
 *
 * @see ConciliaResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class ConciliaResourceIntTest {

    private static final LocalDate DEFAULT_D_FECCONCI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECCONCI = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_N_FLGAUDRES = false;
    private static final Boolean UPDATED_N_FLGAUDRES = true;

    private static final Boolean DEFAULT_N_FLGREPROG = false;
    private static final Boolean UPDATED_N_FLGREPROG = true;

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private ConciliaRepository conciliaRepository;

    @Autowired
    private ConciliaSearchRepository conciliaSearchRepository;
    
    @Autowired
    private CancohorfecRepository cancohorfecRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restConciliaMockMvc;

    private Concilia concilia;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ConciliaResource conciliaResource = new ConciliaResource(conciliaRepository, conciliaSearchRepository, cancohorfecRepository);
        this.restConciliaMockMvc = MockMvcBuilders.standaloneSetup(conciliaResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Concilia createEntity(EntityManager em) {
        Concilia concilia = new Concilia()
            .dFecconci(DEFAULT_D_FECCONCI)
            .nFlgaudres(DEFAULT_N_FLGAUDRES)
            .nFlgreprog(DEFAULT_N_FLGREPROG)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return concilia;
    }

    @Before
    public void initTest() {
        conciliaSearchRepository.deleteAll();
        concilia = createEntity(em);
    }

    @Test
    @Transactional
    public void createConcilia() throws Exception {
        int databaseSizeBeforeCreate = conciliaRepository.findAll().size();

        // Create the Concilia
        restConciliaMockMvc.perform(post("/api/concilias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(concilia)))
            .andExpect(status().isCreated());

        // Validate the Concilia in the database
        List<Concilia> conciliaList = conciliaRepository.findAll();
        assertThat(conciliaList).hasSize(databaseSizeBeforeCreate + 1);
        Concilia testConcilia = conciliaList.get(conciliaList.size() - 1);
        assertThat(testConcilia.getdFecconci()).isEqualTo(DEFAULT_D_FECCONCI);
        assertThat(testConcilia.isnFlgaudres()).isEqualTo(DEFAULT_N_FLGAUDRES);
        assertThat(testConcilia.isnFlgreprog()).isEqualTo(DEFAULT_N_FLGREPROG);
        assertThat(testConcilia.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testConcilia.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testConcilia.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testConcilia.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testConcilia.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testConcilia.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testConcilia.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Concilia in Elasticsearch
        Concilia conciliaEs = conciliaSearchRepository.findOne(testConcilia.getId());
        assertThat(conciliaEs).isEqualToComparingFieldByField(testConcilia);
    }

    @Test
    @Transactional
    public void createConciliaWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = conciliaRepository.findAll().size();

        // Create the Concilia with an existing ID
        concilia.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restConciliaMockMvc.perform(post("/api/concilias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(concilia)))
            .andExpect(status().isBadRequest());

        // Validate the Concilia in the database
        List<Concilia> conciliaList = conciliaRepository.findAll();
        assertThat(conciliaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = conciliaRepository.findAll().size();
        // set the field null
        concilia.setnUsuareg(null);

        // Create the Concilia, which fails.

        restConciliaMockMvc.perform(post("/api/concilias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(concilia)))
            .andExpect(status().isBadRequest());

        List<Concilia> conciliaList = conciliaRepository.findAll();
        assertThat(conciliaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = conciliaRepository.findAll().size();
        // set the field null
        concilia.settFecreg(null);

        // Create the Concilia, which fails.

        restConciliaMockMvc.perform(post("/api/concilias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(concilia)))
            .andExpect(status().isBadRequest());

        List<Concilia> conciliaList = conciliaRepository.findAll();
        assertThat(conciliaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = conciliaRepository.findAll().size();
        // set the field null
        concilia.setnFlgactivo(null);

        // Create the Concilia, which fails.

        restConciliaMockMvc.perform(post("/api/concilias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(concilia)))
            .andExpect(status().isBadRequest());

        List<Concilia> conciliaList = conciliaRepository.findAll();
        assertThat(conciliaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = conciliaRepository.findAll().size();
        // set the field null
        concilia.setnSedereg(null);

        // Create the Concilia, which fails.

        restConciliaMockMvc.perform(post("/api/concilias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(concilia)))
            .andExpect(status().isBadRequest());

        List<Concilia> conciliaList = conciliaRepository.findAll();
        assertThat(conciliaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllConcilias() throws Exception {
        // Initialize the database
        conciliaRepository.saveAndFlush(concilia);

        // Get all the conciliaList
        restConciliaMockMvc.perform(get("/api/concilias?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(concilia.getId().intValue())))
            .andExpect(jsonPath("$.[*].dFecconci").value(hasItem(DEFAULT_D_FECCONCI.toString())))
            .andExpect(jsonPath("$.[*].nFlgaudres").value(hasItem(DEFAULT_N_FLGAUDRES.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlgreprog").value(hasItem(DEFAULT_N_FLGREPROG.booleanValue())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getConcilia() throws Exception {
        // Initialize the database
        conciliaRepository.saveAndFlush(concilia);

        // Get the concilia
        restConciliaMockMvc.perform(get("/api/concilias/{id}", concilia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(concilia.getId().intValue()))
            .andExpect(jsonPath("$.dFecconci").value(DEFAULT_D_FECCONCI.toString()))
            .andExpect(jsonPath("$.nFlgaudres").value(DEFAULT_N_FLGAUDRES.booleanValue()))
            .andExpect(jsonPath("$.nFlgreprog").value(DEFAULT_N_FLGREPROG.booleanValue()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingConcilia() throws Exception {
        // Get the concilia
        restConciliaMockMvc.perform(get("/api/concilias/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateConcilia() throws Exception {
        // Initialize the database
        conciliaRepository.saveAndFlush(concilia);
        conciliaSearchRepository.save(concilia);
        int databaseSizeBeforeUpdate = conciliaRepository.findAll().size();

        // Update the concilia
        Concilia updatedConcilia = conciliaRepository.findOne(concilia.getId());
        updatedConcilia
            .dFecconci(UPDATED_D_FECCONCI)
            .nFlgaudres(UPDATED_N_FLGAUDRES)
            .nFlgreprog(UPDATED_N_FLGREPROG)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restConciliaMockMvc.perform(put("/api/concilias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedConcilia)))
            .andExpect(status().isOk());

        // Validate the Concilia in the database
        List<Concilia> conciliaList = conciliaRepository.findAll();
        assertThat(conciliaList).hasSize(databaseSizeBeforeUpdate);
        Concilia testConcilia = conciliaList.get(conciliaList.size() - 1);
        assertThat(testConcilia.getdFecconci()).isEqualTo(UPDATED_D_FECCONCI);
        assertThat(testConcilia.isnFlgaudres()).isEqualTo(UPDATED_N_FLGAUDRES);
        assertThat(testConcilia.isnFlgreprog()).isEqualTo(UPDATED_N_FLGREPROG);
        assertThat(testConcilia.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testConcilia.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testConcilia.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testConcilia.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testConcilia.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testConcilia.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testConcilia.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Concilia in Elasticsearch
        Concilia conciliaEs = conciliaSearchRepository.findOne(testConcilia.getId());
        assertThat(conciliaEs).isEqualToComparingFieldByField(testConcilia);
    }

    @Test
    @Transactional
    public void updateNonExistingConcilia() throws Exception {
        int databaseSizeBeforeUpdate = conciliaRepository.findAll().size();

        // Create the Concilia

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restConciliaMockMvc.perform(put("/api/concilias")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(concilia)))
            .andExpect(status().isCreated());

        // Validate the Concilia in the database
        List<Concilia> conciliaList = conciliaRepository.findAll();
        assertThat(conciliaList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteConcilia() throws Exception {
        // Initialize the database
        conciliaRepository.saveAndFlush(concilia);
        conciliaSearchRepository.save(concilia);
        int databaseSizeBeforeDelete = conciliaRepository.findAll().size();

        // Get the concilia
        restConciliaMockMvc.perform(delete("/api/concilias/{id}", concilia.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean conciliaExistsInEs = conciliaSearchRepository.exists(concilia.getId());
        assertThat(conciliaExistsInEs).isFalse();

        // Validate the database is empty
        List<Concilia> conciliaList = conciliaRepository.findAll();
        assertThat(conciliaList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchConcilia() throws Exception {
        // Initialize the database
        conciliaRepository.saveAndFlush(concilia);
        conciliaSearchRepository.save(concilia);

        // Search the concilia
        restConciliaMockMvc.perform(get("/api/_search/concilias?query=id:" + concilia.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(concilia.getId().intValue())))
            .andExpect(jsonPath("$.[*].dFecconci").value(hasItem(DEFAULT_D_FECCONCI.toString())))
            .andExpect(jsonPath("$.[*].nFlgaudres").value(hasItem(DEFAULT_N_FLGAUDRES.booleanValue())))
            .andExpect(jsonPath("$.[*].nFlgreprog").value(hasItem(DEFAULT_N_FLGREPROG.booleanValue())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Concilia.class);
        Concilia concilia1 = new Concilia();
        concilia1.setId(1L);
        Concilia concilia2 = new Concilia();
        concilia2.setId(concilia1.getId());
        assertThat(concilia1).isEqualTo(concilia2);
        concilia2.setId(2L);
        assertThat(concilia1).isNotEqualTo(concilia2);
        concilia1.setId(null);
        assertThat(concilia1).isNotEqualTo(concilia2);
    }
}
