package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Tipdocexp;
import pe.gob.trabajo.repository.TipdocexpRepository;
import pe.gob.trabajo.repository.search.TipdocexpSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipdocexpResource REST controller.
 *
 * @see TipdocexpResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class TipdocexpResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private TipdocexpRepository tipdocexpRepository;

    @Autowired
    private TipdocexpSearchRepository tipdocexpSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipdocexpMockMvc;

    private Tipdocexp tipdocexp;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipdocexpResource tipdocexpResource = new TipdocexpResource(tipdocexpRepository, tipdocexpSearchRepository);
        this.restTipdocexpMockMvc = MockMvcBuilders.standaloneSetup(tipdocexpResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tipdocexp createEntity(EntityManager em) {
        Tipdocexp tipdocexp = new Tipdocexp()
            .vDescrip(DEFAULT_V_DESCRIP)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return tipdocexp;
    }

    @Before
    public void initTest() {
        tipdocexpSearchRepository.deleteAll();
        tipdocexp = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipdocexp() throws Exception {
        int databaseSizeBeforeCreate = tipdocexpRepository.findAll().size();

        // Create the Tipdocexp
        restTipdocexpMockMvc.perform(post("/api/tipdocexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocexp)))
            .andExpect(status().isCreated());

        // Validate the Tipdocexp in the database
        List<Tipdocexp> tipdocexpList = tipdocexpRepository.findAll();
        assertThat(tipdocexpList).hasSize(databaseSizeBeforeCreate + 1);
        Tipdocexp testTipdocexp = tipdocexpList.get(tipdocexpList.size() - 1);
        assertThat(testTipdocexp.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testTipdocexp.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testTipdocexp.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testTipdocexp.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testTipdocexp.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testTipdocexp.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testTipdocexp.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testTipdocexp.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Tipdocexp in Elasticsearch
        Tipdocexp tipdocexpEs = tipdocexpSearchRepository.findOne(testTipdocexp.getId());
        assertThat(tipdocexpEs).isEqualToComparingFieldByField(testTipdocexp);
    }

    @Test
    @Transactional
    public void createTipdocexpWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipdocexpRepository.findAll().size();

        // Create the Tipdocexp with an existing ID
        tipdocexp.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipdocexpMockMvc.perform(post("/api/tipdocexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocexp)))
            .andExpect(status().isBadRequest());

        // Validate the Tipdocexp in the database
        List<Tipdocexp> tipdocexpList = tipdocexpRepository.findAll();
        assertThat(tipdocexpList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdocexpRepository.findAll().size();
        // set the field null
        tipdocexp.setvDescrip(null);

        // Create the Tipdocexp, which fails.

        restTipdocexpMockMvc.perform(post("/api/tipdocexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocexp)))
            .andExpect(status().isBadRequest());

        List<Tipdocexp> tipdocexpList = tipdocexpRepository.findAll();
        assertThat(tipdocexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdocexpRepository.findAll().size();
        // set the field null
        tipdocexp.setnUsuareg(null);

        // Create the Tipdocexp, which fails.

        restTipdocexpMockMvc.perform(post("/api/tipdocexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocexp)))
            .andExpect(status().isBadRequest());

        List<Tipdocexp> tipdocexpList = tipdocexpRepository.findAll();
        assertThat(tipdocexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdocexpRepository.findAll().size();
        // set the field null
        tipdocexp.settFecreg(null);

        // Create the Tipdocexp, which fails.

        restTipdocexpMockMvc.perform(post("/api/tipdocexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocexp)))
            .andExpect(status().isBadRequest());

        List<Tipdocexp> tipdocexpList = tipdocexpRepository.findAll();
        assertThat(tipdocexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdocexpRepository.findAll().size();
        // set the field null
        tipdocexp.setnFlgactivo(null);

        // Create the Tipdocexp, which fails.

        restTipdocexpMockMvc.perform(post("/api/tipdocexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocexp)))
            .andExpect(status().isBadRequest());

        List<Tipdocexp> tipdocexpList = tipdocexpRepository.findAll();
        assertThat(tipdocexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipdocexpRepository.findAll().size();
        // set the field null
        tipdocexp.setnSedereg(null);

        // Create the Tipdocexp, which fails.

        restTipdocexpMockMvc.perform(post("/api/tipdocexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocexp)))
            .andExpect(status().isBadRequest());

        List<Tipdocexp> tipdocexpList = tipdocexpRepository.findAll();
        assertThat(tipdocexpList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipdocexps() throws Exception {
        // Initialize the database
        tipdocexpRepository.saveAndFlush(tipdocexp);

        // Get all the tipdocexpList
        restTipdocexpMockMvc.perform(get("/api/tipdocexps?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipdocexp.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getTipdocexp() throws Exception {
        // Initialize the database
        tipdocexpRepository.saveAndFlush(tipdocexp);

        // Get the tipdocexp
        restTipdocexpMockMvc.perform(get("/api/tipdocexps/{id}", tipdocexp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipdocexp.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingTipdocexp() throws Exception {
        // Get the tipdocexp
        restTipdocexpMockMvc.perform(get("/api/tipdocexps/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipdocexp() throws Exception {
        // Initialize the database
        tipdocexpRepository.saveAndFlush(tipdocexp);
        tipdocexpSearchRepository.save(tipdocexp);
        int databaseSizeBeforeUpdate = tipdocexpRepository.findAll().size();

        // Update the tipdocexp
        Tipdocexp updatedTipdocexp = tipdocexpRepository.findOne(tipdocexp.getId());
        updatedTipdocexp
            .vDescrip(UPDATED_V_DESCRIP)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restTipdocexpMockMvc.perform(put("/api/tipdocexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipdocexp)))
            .andExpect(status().isOk());

        // Validate the Tipdocexp in the database
        List<Tipdocexp> tipdocexpList = tipdocexpRepository.findAll();
        assertThat(tipdocexpList).hasSize(databaseSizeBeforeUpdate);
        Tipdocexp testTipdocexp = tipdocexpList.get(tipdocexpList.size() - 1);
        assertThat(testTipdocexp.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testTipdocexp.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testTipdocexp.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testTipdocexp.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testTipdocexp.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testTipdocexp.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testTipdocexp.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testTipdocexp.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Tipdocexp in Elasticsearch
        Tipdocexp tipdocexpEs = tipdocexpSearchRepository.findOne(testTipdocexp.getId());
        assertThat(tipdocexpEs).isEqualToComparingFieldByField(testTipdocexp);
    }

    @Test
    @Transactional
    public void updateNonExistingTipdocexp() throws Exception {
        int databaseSizeBeforeUpdate = tipdocexpRepository.findAll().size();

        // Create the Tipdocexp

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipdocexpMockMvc.perform(put("/api/tipdocexps")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipdocexp)))
            .andExpect(status().isCreated());

        // Validate the Tipdocexp in the database
        List<Tipdocexp> tipdocexpList = tipdocexpRepository.findAll();
        assertThat(tipdocexpList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipdocexp() throws Exception {
        // Initialize the database
        tipdocexpRepository.saveAndFlush(tipdocexp);
        tipdocexpSearchRepository.save(tipdocexp);
        int databaseSizeBeforeDelete = tipdocexpRepository.findAll().size();

        // Get the tipdocexp
        restTipdocexpMockMvc.perform(delete("/api/tipdocexps/{id}", tipdocexp.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tipdocexpExistsInEs = tipdocexpSearchRepository.exists(tipdocexp.getId());
        assertThat(tipdocexpExistsInEs).isFalse();

        // Validate the database is empty
        List<Tipdocexp> tipdocexpList = tipdocexpRepository.findAll();
        assertThat(tipdocexpList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTipdocexp() throws Exception {
        // Initialize the database
        tipdocexpRepository.saveAndFlush(tipdocexp);
        tipdocexpSearchRepository.save(tipdocexp);

        // Search the tipdocexp
        restTipdocexpMockMvc.perform(get("/api/_search/tipdocexps?query=id:" + tipdocexp.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipdocexp.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tipdocexp.class);
        Tipdocexp tipdocexp1 = new Tipdocexp();
        tipdocexp1.setId(1L);
        Tipdocexp tipdocexp2 = new Tipdocexp();
        tipdocexp2.setId(tipdocexp1.getId());
        assertThat(tipdocexp1).isEqualTo(tipdocexp2);
        tipdocexp2.setId(2L);
        assertThat(tipdocexp1).isNotEqualTo(tipdocexp2);
        tipdocexp1.setId(null);
        assertThat(tipdocexp1).isNotEqualTo(tipdocexp2);
    }
}
