package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Tipenvnot;
import pe.gob.trabajo.repository.TipenvnotRepository;
import pe.gob.trabajo.repository.search.TipenvnotSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the TipenvnotResource REST controller.
 *
 * @see TipenvnotResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class TipenvnotResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private TipenvnotRepository tipenvnotRepository;

    @Autowired
    private TipenvnotSearchRepository tipenvnotSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restTipenvnotMockMvc;

    private Tipenvnot tipenvnot;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final TipenvnotResource tipenvnotResource = new TipenvnotResource(tipenvnotRepository, tipenvnotSearchRepository);
        this.restTipenvnotMockMvc = MockMvcBuilders.standaloneSetup(tipenvnotResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Tipenvnot createEntity(EntityManager em) {
        Tipenvnot tipenvnot = new Tipenvnot()
            .vDescrip(DEFAULT_V_DESCRIP)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return tipenvnot;
    }

    @Before
    public void initTest() {
        tipenvnotSearchRepository.deleteAll();
        tipenvnot = createEntity(em);
    }

    @Test
    @Transactional
    public void createTipenvnot() throws Exception {
        int databaseSizeBeforeCreate = tipenvnotRepository.findAll().size();

        // Create the Tipenvnot
        restTipenvnotMockMvc.perform(post("/api/tipenvnots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipenvnot)))
            .andExpect(status().isCreated());

        // Validate the Tipenvnot in the database
        List<Tipenvnot> tipenvnotList = tipenvnotRepository.findAll();
        assertThat(tipenvnotList).hasSize(databaseSizeBeforeCreate + 1);
        Tipenvnot testTipenvnot = tipenvnotList.get(tipenvnotList.size() - 1);
        assertThat(testTipenvnot.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testTipenvnot.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testTipenvnot.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testTipenvnot.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testTipenvnot.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testTipenvnot.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testTipenvnot.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testTipenvnot.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Tipenvnot in Elasticsearch
        Tipenvnot tipenvnotEs = tipenvnotSearchRepository.findOne(testTipenvnot.getId());
        assertThat(tipenvnotEs).isEqualToComparingFieldByField(testTipenvnot);
    }

    @Test
    @Transactional
    public void createTipenvnotWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = tipenvnotRepository.findAll().size();

        // Create the Tipenvnot with an existing ID
        tipenvnot.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restTipenvnotMockMvc.perform(post("/api/tipenvnots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipenvnot)))
            .andExpect(status().isBadRequest());

        // Validate the Tipenvnot in the database
        List<Tipenvnot> tipenvnotList = tipenvnotRepository.findAll();
        assertThat(tipenvnotList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipenvnotRepository.findAll().size();
        // set the field null
        tipenvnot.setvDescrip(null);

        // Create the Tipenvnot, which fails.

        restTipenvnotMockMvc.perform(post("/api/tipenvnots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipenvnot)))
            .andExpect(status().isBadRequest());

        List<Tipenvnot> tipenvnotList = tipenvnotRepository.findAll();
        assertThat(tipenvnotList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipenvnotRepository.findAll().size();
        // set the field null
        tipenvnot.setnUsuareg(null);

        // Create the Tipenvnot, which fails.

        restTipenvnotMockMvc.perform(post("/api/tipenvnots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipenvnot)))
            .andExpect(status().isBadRequest());

        List<Tipenvnot> tipenvnotList = tipenvnotRepository.findAll();
        assertThat(tipenvnotList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipenvnotRepository.findAll().size();
        // set the field null
        tipenvnot.settFecreg(null);

        // Create the Tipenvnot, which fails.

        restTipenvnotMockMvc.perform(post("/api/tipenvnots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipenvnot)))
            .andExpect(status().isBadRequest());

        List<Tipenvnot> tipenvnotList = tipenvnotRepository.findAll();
        assertThat(tipenvnotList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipenvnotRepository.findAll().size();
        // set the field null
        tipenvnot.setnFlgactivo(null);

        // Create the Tipenvnot, which fails.

        restTipenvnotMockMvc.perform(post("/api/tipenvnots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipenvnot)))
            .andExpect(status().isBadRequest());

        List<Tipenvnot> tipenvnotList = tipenvnotRepository.findAll();
        assertThat(tipenvnotList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = tipenvnotRepository.findAll().size();
        // set the field null
        tipenvnot.setnSedereg(null);

        // Create the Tipenvnot, which fails.

        restTipenvnotMockMvc.perform(post("/api/tipenvnots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipenvnot)))
            .andExpect(status().isBadRequest());

        List<Tipenvnot> tipenvnotList = tipenvnotRepository.findAll();
        assertThat(tipenvnotList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllTipenvnots() throws Exception {
        // Initialize the database
        tipenvnotRepository.saveAndFlush(tipenvnot);

        // Get all the tipenvnotList
        restTipenvnotMockMvc.perform(get("/api/tipenvnots?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipenvnot.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getTipenvnot() throws Exception {
        // Initialize the database
        tipenvnotRepository.saveAndFlush(tipenvnot);

        // Get the tipenvnot
        restTipenvnotMockMvc.perform(get("/api/tipenvnots/{id}", tipenvnot.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(tipenvnot.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingTipenvnot() throws Exception {
        // Get the tipenvnot
        restTipenvnotMockMvc.perform(get("/api/tipenvnots/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateTipenvnot() throws Exception {
        // Initialize the database
        tipenvnotRepository.saveAndFlush(tipenvnot);
        tipenvnotSearchRepository.save(tipenvnot);
        int databaseSizeBeforeUpdate = tipenvnotRepository.findAll().size();

        // Update the tipenvnot
        Tipenvnot updatedTipenvnot = tipenvnotRepository.findOne(tipenvnot.getId());
        updatedTipenvnot
            .vDescrip(UPDATED_V_DESCRIP)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restTipenvnotMockMvc.perform(put("/api/tipenvnots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedTipenvnot)))
            .andExpect(status().isOk());

        // Validate the Tipenvnot in the database
        List<Tipenvnot> tipenvnotList = tipenvnotRepository.findAll();
        assertThat(tipenvnotList).hasSize(databaseSizeBeforeUpdate);
        Tipenvnot testTipenvnot = tipenvnotList.get(tipenvnotList.size() - 1);
        assertThat(testTipenvnot.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testTipenvnot.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testTipenvnot.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testTipenvnot.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testTipenvnot.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testTipenvnot.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testTipenvnot.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testTipenvnot.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Tipenvnot in Elasticsearch
        Tipenvnot tipenvnotEs = tipenvnotSearchRepository.findOne(testTipenvnot.getId());
        assertThat(tipenvnotEs).isEqualToComparingFieldByField(testTipenvnot);
    }

    @Test
    @Transactional
    public void updateNonExistingTipenvnot() throws Exception {
        int databaseSizeBeforeUpdate = tipenvnotRepository.findAll().size();

        // Create the Tipenvnot

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restTipenvnotMockMvc.perform(put("/api/tipenvnots")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(tipenvnot)))
            .andExpect(status().isCreated());

        // Validate the Tipenvnot in the database
        List<Tipenvnot> tipenvnotList = tipenvnotRepository.findAll();
        assertThat(tipenvnotList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteTipenvnot() throws Exception {
        // Initialize the database
        tipenvnotRepository.saveAndFlush(tipenvnot);
        tipenvnotSearchRepository.save(tipenvnot);
        int databaseSizeBeforeDelete = tipenvnotRepository.findAll().size();

        // Get the tipenvnot
        restTipenvnotMockMvc.perform(delete("/api/tipenvnots/{id}", tipenvnot.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean tipenvnotExistsInEs = tipenvnotSearchRepository.exists(tipenvnot.getId());
        assertThat(tipenvnotExistsInEs).isFalse();

        // Validate the database is empty
        List<Tipenvnot> tipenvnotList = tipenvnotRepository.findAll();
        assertThat(tipenvnotList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchTipenvnot() throws Exception {
        // Initialize the database
        tipenvnotRepository.saveAndFlush(tipenvnot);
        tipenvnotSearchRepository.save(tipenvnot);

        // Search the tipenvnot
        restTipenvnotMockMvc.perform(get("/api/_search/tipenvnots?query=id:" + tipenvnot.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(tipenvnot.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Tipenvnot.class);
        Tipenvnot tipenvnot1 = new Tipenvnot();
        tipenvnot1.setId(1L);
        Tipenvnot tipenvnot2 = new Tipenvnot();
        tipenvnot2.setId(tipenvnot1.getId());
        assertThat(tipenvnot1).isEqualTo(tipenvnot2);
        tipenvnot2.setId(2L);
        assertThat(tipenvnot1).isNotEqualTo(tipenvnot2);
        tipenvnot1.setId(null);
        assertThat(tipenvnot1).isNotEqualTo(tipenvnot2);
    }
}
