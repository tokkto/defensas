package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Resulconci;
import pe.gob.trabajo.repository.ResulconciRepository;
import pe.gob.trabajo.repository.search.ResulconciSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ResulconciResource REST controller.
 *
 * @see ResulconciResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class ResulconciResourceIntTest {

    private static final String DEFAULT_V_DESCRIP = "AAAAAAAAAA";
    private static final String UPDATED_V_DESCRIP = "BBBBBBBBBB";

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private ResulconciRepository resulconciRepository;

    @Autowired
    private ResulconciSearchRepository resulconciSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restResulconciMockMvc;

    private Resulconci resulconci;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ResulconciResource resulconciResource = new ResulconciResource(resulconciRepository, resulconciSearchRepository);
        this.restResulconciMockMvc = MockMvcBuilders.standaloneSetup(resulconciResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Resulconci createEntity(EntityManager em) {
        Resulconci resulconci = new Resulconci()
            .vDescrip(DEFAULT_V_DESCRIP)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return resulconci;
    }

    @Before
    public void initTest() {
        resulconciSearchRepository.deleteAll();
        resulconci = createEntity(em);
    }

    @Test
    @Transactional
    public void createResulconci() throws Exception {
        int databaseSizeBeforeCreate = resulconciRepository.findAll().size();

        // Create the Resulconci
        restResulconciMockMvc.perform(post("/api/resulconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resulconci)))
            .andExpect(status().isCreated());

        // Validate the Resulconci in the database
        List<Resulconci> resulconciList = resulconciRepository.findAll();
        assertThat(resulconciList).hasSize(databaseSizeBeforeCreate + 1);
        Resulconci testResulconci = resulconciList.get(resulconciList.size() - 1);
        assertThat(testResulconci.getvDescrip()).isEqualTo(DEFAULT_V_DESCRIP);
        assertThat(testResulconci.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testResulconci.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testResulconci.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testResulconci.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testResulconci.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testResulconci.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testResulconci.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Resulconci in Elasticsearch
        Resulconci resulconciEs = resulconciSearchRepository.findOne(testResulconci.getId());
        assertThat(resulconciEs).isEqualToComparingFieldByField(testResulconci);
    }

    @Test
    @Transactional
    public void createResulconciWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = resulconciRepository.findAll().size();

        // Create the Resulconci with an existing ID
        resulconci.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResulconciMockMvc.perform(post("/api/resulconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resulconci)))
            .andExpect(status().isBadRequest());

        // Validate the Resulconci in the database
        List<Resulconci> resulconciList = resulconciRepository.findAll();
        assertThat(resulconciList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkvDescripIsRequired() throws Exception {
        int databaseSizeBeforeTest = resulconciRepository.findAll().size();
        // set the field null
        resulconci.setvDescrip(null);

        // Create the Resulconci, which fails.

        restResulconciMockMvc.perform(post("/api/resulconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resulconci)))
            .andExpect(status().isBadRequest());

        List<Resulconci> resulconciList = resulconciRepository.findAll();
        assertThat(resulconciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = resulconciRepository.findAll().size();
        // set the field null
        resulconci.setnUsuareg(null);

        // Create the Resulconci, which fails.

        restResulconciMockMvc.perform(post("/api/resulconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resulconci)))
            .andExpect(status().isBadRequest());

        List<Resulconci> resulconciList = resulconciRepository.findAll();
        assertThat(resulconciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = resulconciRepository.findAll().size();
        // set the field null
        resulconci.settFecreg(null);

        // Create the Resulconci, which fails.

        restResulconciMockMvc.perform(post("/api/resulconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resulconci)))
            .andExpect(status().isBadRequest());

        List<Resulconci> resulconciList = resulconciRepository.findAll();
        assertThat(resulconciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = resulconciRepository.findAll().size();
        // set the field null
        resulconci.setnFlgactivo(null);

        // Create the Resulconci, which fails.

        restResulconciMockMvc.perform(post("/api/resulconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resulconci)))
            .andExpect(status().isBadRequest());

        List<Resulconci> resulconciList = resulconciRepository.findAll();
        assertThat(resulconciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = resulconciRepository.findAll().size();
        // set the field null
        resulconci.setnSedereg(null);

        // Create the Resulconci, which fails.

        restResulconciMockMvc.perform(post("/api/resulconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resulconci)))
            .andExpect(status().isBadRequest());

        List<Resulconci> resulconciList = resulconciRepository.findAll();
        assertThat(resulconciList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllResulconcis() throws Exception {
        // Initialize the database
        resulconciRepository.saveAndFlush(resulconci);

        // Get all the resulconciList
        restResulconciMockMvc.perform(get("/api/resulconcis?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resulconci.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getResulconci() throws Exception {
        // Initialize the database
        resulconciRepository.saveAndFlush(resulconci);

        // Get the resulconci
        restResulconciMockMvc.perform(get("/api/resulconcis/{id}", resulconci.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(resulconci.getId().intValue()))
            .andExpect(jsonPath("$.vDescrip").value(DEFAULT_V_DESCRIP.toString()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingResulconci() throws Exception {
        // Get the resulconci
        restResulconciMockMvc.perform(get("/api/resulconcis/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResulconci() throws Exception {
        // Initialize the database
        resulconciRepository.saveAndFlush(resulconci);
        resulconciSearchRepository.save(resulconci);
        int databaseSizeBeforeUpdate = resulconciRepository.findAll().size();

        // Update the resulconci
        Resulconci updatedResulconci = resulconciRepository.findOne(resulconci.getId());
        updatedResulconci
            .vDescrip(UPDATED_V_DESCRIP)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restResulconciMockMvc.perform(put("/api/resulconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedResulconci)))
            .andExpect(status().isOk());

        // Validate the Resulconci in the database
        List<Resulconci> resulconciList = resulconciRepository.findAll();
        assertThat(resulconciList).hasSize(databaseSizeBeforeUpdate);
        Resulconci testResulconci = resulconciList.get(resulconciList.size() - 1);
        assertThat(testResulconci.getvDescrip()).isEqualTo(UPDATED_V_DESCRIP);
        assertThat(testResulconci.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testResulconci.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testResulconci.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testResulconci.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testResulconci.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testResulconci.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testResulconci.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Resulconci in Elasticsearch
        Resulconci resulconciEs = resulconciSearchRepository.findOne(testResulconci.getId());
        assertThat(resulconciEs).isEqualToComparingFieldByField(testResulconci);
    }

    @Test
    @Transactional
    public void updateNonExistingResulconci() throws Exception {
        int databaseSizeBeforeUpdate = resulconciRepository.findAll().size();

        // Create the Resulconci

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restResulconciMockMvc.perform(put("/api/resulconcis")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resulconci)))
            .andExpect(status().isCreated());

        // Validate the Resulconci in the database
        List<Resulconci> resulconciList = resulconciRepository.findAll();
        assertThat(resulconciList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteResulconci() throws Exception {
        // Initialize the database
        resulconciRepository.saveAndFlush(resulconci);
        resulconciSearchRepository.save(resulconci);
        int databaseSizeBeforeDelete = resulconciRepository.findAll().size();

        // Get the resulconci
        restResulconciMockMvc.perform(delete("/api/resulconcis/{id}", resulconci.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean resulconciExistsInEs = resulconciSearchRepository.exists(resulconci.getId());
        assertThat(resulconciExistsInEs).isFalse();

        // Validate the database is empty
        List<Resulconci> resulconciList = resulconciRepository.findAll();
        assertThat(resulconciList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchResulconci() throws Exception {
        // Initialize the database
        resulconciRepository.saveAndFlush(resulconci);
        resulconciSearchRepository.save(resulconci);

        // Search the resulconci
        restResulconciMockMvc.perform(get("/api/_search/resulconcis?query=id:" + resulconci.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resulconci.getId().intValue())))
            .andExpect(jsonPath("$.[*].vDescrip").value(hasItem(DEFAULT_V_DESCRIP.toString())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Resulconci.class);
        Resulconci resulconci1 = new Resulconci();
        resulconci1.setId(1L);
        Resulconci resulconci2 = new Resulconci();
        resulconci2.setId(resulconci1.getId());
        assertThat(resulconci1).isEqualTo(resulconci2);
        resulconci2.setId(2L);
        assertThat(resulconci1).isNotEqualTo(resulconci2);
        resulconci1.setId(null);
        assertThat(resulconci1).isNotEqualTo(resulconci2);
    }
}
