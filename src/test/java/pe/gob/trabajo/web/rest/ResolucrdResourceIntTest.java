package pe.gob.trabajo.web.rest;

import pe.gob.trabajo.DefensaApp;

import pe.gob.trabajo.domain.Resolucrd;
import pe.gob.trabajo.repository.ResolucrdRepository;
import pe.gob.trabajo.repository.search.ResolucrdSearchRepository;
import pe.gob.trabajo.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static pe.gob.trabajo.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ResolucrdResource REST controller.
 *
 * @see ResolucrdResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DefensaApp.class)
public class ResolucrdResourceIntTest {

    private static final String DEFAULT_V_NUMRESOSD = "AAAAAAAAAA";
    private static final String UPDATED_V_NUMRESOSD = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECRESOSD = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECRESOSD = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_NOMEMPLEA = "AAAAAAAAAA";
    private static final String UPDATED_V_NOMEMPLEA = "BBBBBBBBBB";

    private static final String DEFAULT_V_NOMTRABAJ = "AAAAAAAAAA";
    private static final String UPDATED_V_NOMTRABAJ = "BBBBBBBBBB";

    private static final String DEFAULT_V_DIRECCION = "AAAAAAAAAA";
    private static final String UPDATED_V_DIRECCION = "BBBBBBBBBB";

    private static final String DEFAULT_V_TELEFONO = "AAAAAAAAAA";
    private static final String UPDATED_V_TELEFONO = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECCONCIL = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECCONCIL = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_HORCONCIL = "AAAAAAAAAA";
    private static final String UPDATED_V_HORCONCIL = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_D_FECHANOTI = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_D_FECHANOTI = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_V_NUMNOTIFI = "AAAAAAAAAA";
    private static final String UPDATED_V_NUMNOTIFI = "BBBBBBBBBB";

    private static final Float DEFAULT_F_MONMULTA = 1F;
    private static final Float UPDATED_F_MONMULTA = 2F;

    private static final Integer DEFAULT_N_USUAREG = 1;
    private static final Integer UPDATED_N_USUAREG = 2;

    private static final Instant DEFAULT_T_FECREG = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECREG = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_N_FLGACTIVO = false;
    private static final Boolean UPDATED_N_FLGACTIVO = true;

    private static final Integer DEFAULT_N_SEDEREG = 1;
    private static final Integer UPDATED_N_SEDEREG = 2;

    private static final Integer DEFAULT_N_USUAUPD = 1;
    private static final Integer UPDATED_N_USUAUPD = 2;

    private static final Instant DEFAULT_T_FECUPD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_T_FECUPD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_N_SEDEUPD = 1;
    private static final Integer UPDATED_N_SEDEUPD = 2;

    @Autowired
    private ResolucrdRepository resolucrdRepository;

    @Autowired
    private ResolucrdSearchRepository resolucrdSearchRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restResolucrdMockMvc;

    private Resolucrd resolucrd;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ResolucrdResource resolucrdResource = new ResolucrdResource(resolucrdRepository, resolucrdSearchRepository);
        this.restResolucrdMockMvc = MockMvcBuilders.standaloneSetup(resolucrdResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Resolucrd createEntity(EntityManager em) {
        Resolucrd resolucrd = new Resolucrd()
            .vNumresosd(DEFAULT_V_NUMRESOSD)
            .dFecresosd(DEFAULT_D_FECRESOSD)
            .vNomemplea(DEFAULT_V_NOMEMPLEA)
            .vNomtrabaj(DEFAULT_V_NOMTRABAJ)
            .vDireccion(DEFAULT_V_DIRECCION)
            .vTelefono(DEFAULT_V_TELEFONO)
            .dFecconcil(DEFAULT_D_FECCONCIL)
            .vHorconcil(DEFAULT_V_HORCONCIL)
            .dFechanoti(DEFAULT_D_FECHANOTI)
            .vNumnotifi(DEFAULT_V_NUMNOTIFI)
            .fMonmulta(DEFAULT_F_MONMULTA)
            .nUsuareg(DEFAULT_N_USUAREG)
            .tFecreg(DEFAULT_T_FECREG)
            .nFlgactivo(DEFAULT_N_FLGACTIVO)
            .nSedereg(DEFAULT_N_SEDEREG)
            .nUsuaupd(DEFAULT_N_USUAUPD)
            .tFecupd(DEFAULT_T_FECUPD)
            .nSedeupd(DEFAULT_N_SEDEUPD);
        return resolucrd;
    }

    @Before
    public void initTest() {
        resolucrdSearchRepository.deleteAll();
        resolucrd = createEntity(em);
    }

    @Test
    @Transactional
    public void createResolucrd() throws Exception {
        int databaseSizeBeforeCreate = resolucrdRepository.findAll().size();

        // Create the Resolucrd
        restResolucrdMockMvc.perform(post("/api/resolucrds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolucrd)))
            .andExpect(status().isCreated());

        // Validate the Resolucrd in the database
        List<Resolucrd> resolucrdList = resolucrdRepository.findAll();
        assertThat(resolucrdList).hasSize(databaseSizeBeforeCreate + 1);
        Resolucrd testResolucrd = resolucrdList.get(resolucrdList.size() - 1);
        assertThat(testResolucrd.getvNumresosd()).isEqualTo(DEFAULT_V_NUMRESOSD);
        assertThat(testResolucrd.getdFecresosd()).isEqualTo(DEFAULT_D_FECRESOSD);
        assertThat(testResolucrd.getvNomemplea()).isEqualTo(DEFAULT_V_NOMEMPLEA);
        assertThat(testResolucrd.getvNomtrabaj()).isEqualTo(DEFAULT_V_NOMTRABAJ);
        assertThat(testResolucrd.getvDireccion()).isEqualTo(DEFAULT_V_DIRECCION);
        assertThat(testResolucrd.getvTelefono()).isEqualTo(DEFAULT_V_TELEFONO);
        assertThat(testResolucrd.getdFecconcil()).isEqualTo(DEFAULT_D_FECCONCIL);
        assertThat(testResolucrd.getvHorconcil()).isEqualTo(DEFAULT_V_HORCONCIL);
        assertThat(testResolucrd.getdFechanoti()).isEqualTo(DEFAULT_D_FECHANOTI);
        assertThat(testResolucrd.getvNumnotifi()).isEqualTo(DEFAULT_V_NUMNOTIFI);
        assertThat(testResolucrd.getfMonmulta()).isEqualTo(DEFAULT_F_MONMULTA);
        assertThat(testResolucrd.getnUsuareg()).isEqualTo(DEFAULT_N_USUAREG);
        assertThat(testResolucrd.gettFecreg()).isEqualTo(DEFAULT_T_FECREG);
        assertThat(testResolucrd.isnFlgactivo()).isEqualTo(DEFAULT_N_FLGACTIVO);
        assertThat(testResolucrd.getnSedereg()).isEqualTo(DEFAULT_N_SEDEREG);
        assertThat(testResolucrd.getnUsuaupd()).isEqualTo(DEFAULT_N_USUAUPD);
        assertThat(testResolucrd.gettFecupd()).isEqualTo(DEFAULT_T_FECUPD);
        assertThat(testResolucrd.getnSedeupd()).isEqualTo(DEFAULT_N_SEDEUPD);

        // Validate the Resolucrd in Elasticsearch
        Resolucrd resolucrdEs = resolucrdSearchRepository.findOne(testResolucrd.getId());
        assertThat(resolucrdEs).isEqualToComparingFieldByField(testResolucrd);
    }

    @Test
    @Transactional
    public void createResolucrdWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = resolucrdRepository.findAll().size();

        // Create the Resolucrd with an existing ID
        resolucrd.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResolucrdMockMvc.perform(post("/api/resolucrds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolucrd)))
            .andExpect(status().isBadRequest());

        // Validate the Resolucrd in the database
        List<Resolucrd> resolucrdList = resolucrdRepository.findAll();
        assertThat(resolucrdList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checknUsuaregIsRequired() throws Exception {
        int databaseSizeBeforeTest = resolucrdRepository.findAll().size();
        // set the field null
        resolucrd.setnUsuareg(null);

        // Create the Resolucrd, which fails.

        restResolucrdMockMvc.perform(post("/api/resolucrds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolucrd)))
            .andExpect(status().isBadRequest());

        List<Resolucrd> resolucrdList = resolucrdRepository.findAll();
        assertThat(resolucrdList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checktFecregIsRequired() throws Exception {
        int databaseSizeBeforeTest = resolucrdRepository.findAll().size();
        // set the field null
        resolucrd.settFecreg(null);

        // Create the Resolucrd, which fails.

        restResolucrdMockMvc.perform(post("/api/resolucrds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolucrd)))
            .andExpect(status().isBadRequest());

        List<Resolucrd> resolucrdList = resolucrdRepository.findAll();
        assertThat(resolucrdList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknFlgactivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = resolucrdRepository.findAll().size();
        // set the field null
        resolucrd.setnFlgactivo(null);

        // Create the Resolucrd, which fails.

        restResolucrdMockMvc.perform(post("/api/resolucrds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolucrd)))
            .andExpect(status().isBadRequest());

        List<Resolucrd> resolucrdList = resolucrdRepository.findAll();
        assertThat(resolucrdList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checknSederegIsRequired() throws Exception {
        int databaseSizeBeforeTest = resolucrdRepository.findAll().size();
        // set the field null
        resolucrd.setnSedereg(null);

        // Create the Resolucrd, which fails.

        restResolucrdMockMvc.perform(post("/api/resolucrds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolucrd)))
            .andExpect(status().isBadRequest());

        List<Resolucrd> resolucrdList = resolucrdRepository.findAll();
        assertThat(resolucrdList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllResolucrds() throws Exception {
        // Initialize the database
        resolucrdRepository.saveAndFlush(resolucrd);

        // Get all the resolucrdList
        restResolucrdMockMvc.perform(get("/api/resolucrds?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resolucrd.getId().intValue())))
            .andExpect(jsonPath("$.[*].vNumresosd").value(hasItem(DEFAULT_V_NUMRESOSD.toString())))
            .andExpect(jsonPath("$.[*].dFecresosd").value(hasItem(DEFAULT_D_FECRESOSD.toString())))
            .andExpect(jsonPath("$.[*].vNomemplea").value(hasItem(DEFAULT_V_NOMEMPLEA.toString())))
            .andExpect(jsonPath("$.[*].vNomtrabaj").value(hasItem(DEFAULT_V_NOMTRABAJ.toString())))
            .andExpect(jsonPath("$.[*].vDireccion").value(hasItem(DEFAULT_V_DIRECCION.toString())))
            .andExpect(jsonPath("$.[*].vTelefono").value(hasItem(DEFAULT_V_TELEFONO.toString())))
            .andExpect(jsonPath("$.[*].dFecconcil").value(hasItem(DEFAULT_D_FECCONCIL.toString())))
            .andExpect(jsonPath("$.[*].vHorconcil").value(hasItem(DEFAULT_V_HORCONCIL.toString())))
            .andExpect(jsonPath("$.[*].dFechanoti").value(hasItem(DEFAULT_D_FECHANOTI.toString())))
            .andExpect(jsonPath("$.[*].vNumnotifi").value(hasItem(DEFAULT_V_NUMNOTIFI.toString())))
            .andExpect(jsonPath("$.[*].fMonmulta").value(hasItem(DEFAULT_F_MONMULTA.doubleValue())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void getResolucrd() throws Exception {
        // Initialize the database
        resolucrdRepository.saveAndFlush(resolucrd);

        // Get the resolucrd
        restResolucrdMockMvc.perform(get("/api/resolucrds/{id}", resolucrd.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(resolucrd.getId().intValue()))
            .andExpect(jsonPath("$.vNumresosd").value(DEFAULT_V_NUMRESOSD.toString()))
            .andExpect(jsonPath("$.dFecresosd").value(DEFAULT_D_FECRESOSD.toString()))
            .andExpect(jsonPath("$.vNomemplea").value(DEFAULT_V_NOMEMPLEA.toString()))
            .andExpect(jsonPath("$.vNomtrabaj").value(DEFAULT_V_NOMTRABAJ.toString()))
            .andExpect(jsonPath("$.vDireccion").value(DEFAULT_V_DIRECCION.toString()))
            .andExpect(jsonPath("$.vTelefono").value(DEFAULT_V_TELEFONO.toString()))
            .andExpect(jsonPath("$.dFecconcil").value(DEFAULT_D_FECCONCIL.toString()))
            .andExpect(jsonPath("$.vHorconcil").value(DEFAULT_V_HORCONCIL.toString()))
            .andExpect(jsonPath("$.dFechanoti").value(DEFAULT_D_FECHANOTI.toString()))
            .andExpect(jsonPath("$.vNumnotifi").value(DEFAULT_V_NUMNOTIFI.toString()))
            .andExpect(jsonPath("$.fMonmulta").value(DEFAULT_F_MONMULTA.doubleValue()))
            .andExpect(jsonPath("$.nUsuareg").value(DEFAULT_N_USUAREG))
            .andExpect(jsonPath("$.tFecreg").value(DEFAULT_T_FECREG.toString()))
            .andExpect(jsonPath("$.nFlgactivo").value(DEFAULT_N_FLGACTIVO.booleanValue()))
            .andExpect(jsonPath("$.nSedereg").value(DEFAULT_N_SEDEREG))
            .andExpect(jsonPath("$.nUsuaupd").value(DEFAULT_N_USUAUPD))
            .andExpect(jsonPath("$.tFecupd").value(DEFAULT_T_FECUPD.toString()))
            .andExpect(jsonPath("$.nSedeupd").value(DEFAULT_N_SEDEUPD));
    }

    @Test
    @Transactional
    public void getNonExistingResolucrd() throws Exception {
        // Get the resolucrd
        restResolucrdMockMvc.perform(get("/api/resolucrds/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResolucrd() throws Exception {
        // Initialize the database
        resolucrdRepository.saveAndFlush(resolucrd);
        resolucrdSearchRepository.save(resolucrd);
        int databaseSizeBeforeUpdate = resolucrdRepository.findAll().size();

        // Update the resolucrd
        Resolucrd updatedResolucrd = resolucrdRepository.findOne(resolucrd.getId());
        updatedResolucrd
            .vNumresosd(UPDATED_V_NUMRESOSD)
            .dFecresosd(UPDATED_D_FECRESOSD)
            .vNomemplea(UPDATED_V_NOMEMPLEA)
            .vNomtrabaj(UPDATED_V_NOMTRABAJ)
            .vDireccion(UPDATED_V_DIRECCION)
            .vTelefono(UPDATED_V_TELEFONO)
            .dFecconcil(UPDATED_D_FECCONCIL)
            .vHorconcil(UPDATED_V_HORCONCIL)
            .dFechanoti(UPDATED_D_FECHANOTI)
            .vNumnotifi(UPDATED_V_NUMNOTIFI)
            .fMonmulta(UPDATED_F_MONMULTA)
            .nUsuareg(UPDATED_N_USUAREG)
            .tFecreg(UPDATED_T_FECREG)
            .nFlgactivo(UPDATED_N_FLGACTIVO)
            .nSedereg(UPDATED_N_SEDEREG)
            .nUsuaupd(UPDATED_N_USUAUPD)
            .tFecupd(UPDATED_T_FECUPD)
            .nSedeupd(UPDATED_N_SEDEUPD);

        restResolucrdMockMvc.perform(put("/api/resolucrds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedResolucrd)))
            .andExpect(status().isOk());

        // Validate the Resolucrd in the database
        List<Resolucrd> resolucrdList = resolucrdRepository.findAll();
        assertThat(resolucrdList).hasSize(databaseSizeBeforeUpdate);
        Resolucrd testResolucrd = resolucrdList.get(resolucrdList.size() - 1);
        assertThat(testResolucrd.getvNumresosd()).isEqualTo(UPDATED_V_NUMRESOSD);
        assertThat(testResolucrd.getdFecresosd()).isEqualTo(UPDATED_D_FECRESOSD);
        assertThat(testResolucrd.getvNomemplea()).isEqualTo(UPDATED_V_NOMEMPLEA);
        assertThat(testResolucrd.getvNomtrabaj()).isEqualTo(UPDATED_V_NOMTRABAJ);
        assertThat(testResolucrd.getvDireccion()).isEqualTo(UPDATED_V_DIRECCION);
        assertThat(testResolucrd.getvTelefono()).isEqualTo(UPDATED_V_TELEFONO);
        assertThat(testResolucrd.getdFecconcil()).isEqualTo(UPDATED_D_FECCONCIL);
        assertThat(testResolucrd.getvHorconcil()).isEqualTo(UPDATED_V_HORCONCIL);
        assertThat(testResolucrd.getdFechanoti()).isEqualTo(UPDATED_D_FECHANOTI);
        assertThat(testResolucrd.getvNumnotifi()).isEqualTo(UPDATED_V_NUMNOTIFI);
        assertThat(testResolucrd.getfMonmulta()).isEqualTo(UPDATED_F_MONMULTA);
        assertThat(testResolucrd.getnUsuareg()).isEqualTo(UPDATED_N_USUAREG);
        assertThat(testResolucrd.gettFecreg()).isEqualTo(UPDATED_T_FECREG);
        assertThat(testResolucrd.isnFlgactivo()).isEqualTo(UPDATED_N_FLGACTIVO);
        assertThat(testResolucrd.getnSedereg()).isEqualTo(UPDATED_N_SEDEREG);
        assertThat(testResolucrd.getnUsuaupd()).isEqualTo(UPDATED_N_USUAUPD);
        assertThat(testResolucrd.gettFecupd()).isEqualTo(UPDATED_T_FECUPD);
        assertThat(testResolucrd.getnSedeupd()).isEqualTo(UPDATED_N_SEDEUPD);

        // Validate the Resolucrd in Elasticsearch
        Resolucrd resolucrdEs = resolucrdSearchRepository.findOne(testResolucrd.getId());
        assertThat(resolucrdEs).isEqualToComparingFieldByField(testResolucrd);
    }

    @Test
    @Transactional
    public void updateNonExistingResolucrd() throws Exception {
        int databaseSizeBeforeUpdate = resolucrdRepository.findAll().size();

        // Create the Resolucrd

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restResolucrdMockMvc.perform(put("/api/resolucrds")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(resolucrd)))
            .andExpect(status().isCreated());

        // Validate the Resolucrd in the database
        List<Resolucrd> resolucrdList = resolucrdRepository.findAll();
        assertThat(resolucrdList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteResolucrd() throws Exception {
        // Initialize the database
        resolucrdRepository.saveAndFlush(resolucrd);
        resolucrdSearchRepository.save(resolucrd);
        int databaseSizeBeforeDelete = resolucrdRepository.findAll().size();

        // Get the resolucrd
        restResolucrdMockMvc.perform(delete("/api/resolucrds/{id}", resolucrd.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate Elasticsearch is empty
        boolean resolucrdExistsInEs = resolucrdSearchRepository.exists(resolucrd.getId());
        assertThat(resolucrdExistsInEs).isFalse();

        // Validate the database is empty
        List<Resolucrd> resolucrdList = resolucrdRepository.findAll();
        assertThat(resolucrdList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void searchResolucrd() throws Exception {
        // Initialize the database
        resolucrdRepository.saveAndFlush(resolucrd);
        resolucrdSearchRepository.save(resolucrd);

        // Search the resolucrd
        restResolucrdMockMvc.perform(get("/api/_search/resolucrds?query=id:" + resolucrd.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(resolucrd.getId().intValue())))
            .andExpect(jsonPath("$.[*].vNumresosd").value(hasItem(DEFAULT_V_NUMRESOSD.toString())))
            .andExpect(jsonPath("$.[*].dFecresosd").value(hasItem(DEFAULT_D_FECRESOSD.toString())))
            .andExpect(jsonPath("$.[*].vNomemplea").value(hasItem(DEFAULT_V_NOMEMPLEA.toString())))
            .andExpect(jsonPath("$.[*].vNomtrabaj").value(hasItem(DEFAULT_V_NOMTRABAJ.toString())))
            .andExpect(jsonPath("$.[*].vDireccion").value(hasItem(DEFAULT_V_DIRECCION.toString())))
            .andExpect(jsonPath("$.[*].vTelefono").value(hasItem(DEFAULT_V_TELEFONO.toString())))
            .andExpect(jsonPath("$.[*].dFecconcil").value(hasItem(DEFAULT_D_FECCONCIL.toString())))
            .andExpect(jsonPath("$.[*].vHorconcil").value(hasItem(DEFAULT_V_HORCONCIL.toString())))
            .andExpect(jsonPath("$.[*].dFechanoti").value(hasItem(DEFAULT_D_FECHANOTI.toString())))
            .andExpect(jsonPath("$.[*].vNumnotifi").value(hasItem(DEFAULT_V_NUMNOTIFI.toString())))
            .andExpect(jsonPath("$.[*].fMonmulta").value(hasItem(DEFAULT_F_MONMULTA.doubleValue())))
            .andExpect(jsonPath("$.[*].nUsuareg").value(hasItem(DEFAULT_N_USUAREG)))
            .andExpect(jsonPath("$.[*].tFecreg").value(hasItem(DEFAULT_T_FECREG.toString())))
            .andExpect(jsonPath("$.[*].nFlgactivo").value(hasItem(DEFAULT_N_FLGACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].nSedereg").value(hasItem(DEFAULT_N_SEDEREG)))
            .andExpect(jsonPath("$.[*].nUsuaupd").value(hasItem(DEFAULT_N_USUAUPD)))
            .andExpect(jsonPath("$.[*].tFecupd").value(hasItem(DEFAULT_T_FECUPD.toString())))
            .andExpect(jsonPath("$.[*].nSedeupd").value(hasItem(DEFAULT_N_SEDEUPD)));
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Resolucrd.class);
        Resolucrd resolucrd1 = new Resolucrd();
        resolucrd1.setId(1L);
        Resolucrd resolucrd2 = new Resolucrd();
        resolucrd2.setId(resolucrd1.getId());
        assertThat(resolucrd1).isEqualTo(resolucrd2);
        resolucrd2.setId(2L);
        assertThat(resolucrd1).isNotEqualTo(resolucrd2);
        resolucrd1.setId(null);
        assertThat(resolucrd1).isNotEqualTo(resolucrd2);
    }
}
